import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';

showAnimDialog(BuildContext context, Widget widget,{duration}) {
  AlertDialog alert = AlertDialog(
    elevation: 5,
    backgroundColor: Colors.white,
    scrollable: true,
    title: widget,
  );

  showAnimatedDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return alert;
    },
    animationType: DialogTransitionType.scale,
    curve: Curves.fastOutSlowIn,
    duration:  Duration(milliseconds: duration ?? 300),
  );
}