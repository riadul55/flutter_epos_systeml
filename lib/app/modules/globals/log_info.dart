
// Blue text
void logInfo(Object msg) {
  print('\x1B[34m$msg\x1B[0m');
}

// Green text
void logSuccess(Object msg) {
  print('\x1B[32m$msg\x1B[0m');
}

// Yellow text
void logWarning(Object msg) {
  print('\x1B[33m$msg\x1B[0m');
}
// cyan text
void logData(Object msg,{asciCode}) {
  print('\x1B${asciCode ?? '[36m'}$msg\x1B[0m');
}

// Red text
void logError(Object msg) {
  print('\x1B[31m$msg\x1B[0m');
}

// static const String red = '\u001b[31m';
// static const String light_red_with_bg = '\u001b[41m';
// static const String green = '\u001b[32m';
// static const String light_green_with_bg = '\u001b[42m';
// static const String blue = '\u001b[34m';
// static const String cyan = '\u001b[36m';
// static const String light_cyan_with_bg = '\u001b[46m';
// static const String magenta = '\u001b[35m';
// static const String yellow = '\u001b[33m';
// static const String grey = '\u001b[90m';