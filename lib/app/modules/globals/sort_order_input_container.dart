import 'package:flutter/material.dart';

import '../../core/utilities/constants.dart';
import 'rounded_button.dart';

class SortOrderInputContainer extends StatefulWidget {
  const SortOrderInputContainer({Key? key}) : super(key: key);

  @override
  _SortOrderInputContainerState createState() =>
      _SortOrderInputContainerState();
}

class _SortOrderInputContainerState extends State<SortOrderInputContainer> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Constants.defaultPadding / 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Display Sort Order",
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).colorScheme.primaryContainer,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                  ),
                  onPressed: () {
                    if (currentIndex > 0) {
                      setState(() {
                        currentIndex--;
                      });
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.all(Constants.defaultPadding * 0.2),
                    child: Icon(Icons.remove),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding * 2),
                child: Text(
                  "$currentIndex",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).colorScheme.primaryContainer,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      currentIndex++;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.all(Constants.defaultPadding * 0.2),
                    child: Icon(Icons.add),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
