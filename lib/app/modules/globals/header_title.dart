import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HeaderTitle extends StatelessWidget {
  final String title;
  final double? width;
  final EdgeInsets? padding;
  final double? borderWidth;
  final Color? bgColor;
  final Color? borderColor;
  final double? fontSize;

  HeaderTitle({
    Key? key,
    required this.title,
    this.width,
    this.padding,
    this.borderWidth,
    this.bgColor,
    this.borderColor,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? Get.width,
      decoration: BoxDecoration(
        color: bgColor,
        border: Border(
          bottom: BorderSide(
            width: borderWidth ?? 1,
            color: borderColor ?? Theme.of(context).colorScheme.primaryVariant,
          ),
        ),
      ),
      child: Padding(
        padding: padding ?? EdgeInsets.all(Constants.defaultPadding / 2),
        child: Text(
          title,
          style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: fontSize ?? Constants.sText,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
