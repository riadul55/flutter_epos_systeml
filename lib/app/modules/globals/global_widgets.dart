import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

divider(BuildContext context, {color, height}) => Divider(
      color: color ?? Theme.of(context).colorScheme.secondaryVariant,
      height: height ?? 1,
    );

leftRightBorderBox(BuildContext context) => BoxDecoration(
      border: Border(
        right: BorderSide(
          color: Theme.of(context).colorScheme.secondaryVariant,
          width: 1,
        ),
        left: BorderSide(
          color: Theme.of(context).colorScheme.secondaryVariant,
          width: 1,
        ),
      ),
    );

bottomBorderBox(BuildContext context) => BoxDecoration(
        border: Border(
            bottom: BorderSide(
      color: Theme.of(context).colorScheme.primaryVariant,
      width: 1,
    )));

showToast({
  required title,
  required message,
  icon,
  position,
  textColor,
  bgColor,
  width = 350.0,
}) =>
    Get.snackbar(
      title,
      message,
      icon: icon,
      snackPosition: position,
      backgroundColor: bgColor,
      colorText: textColor,
      margin: EdgeInsets.all(Constants.defaultPadding),
      maxWidth: width,
    );
