import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RoundedDropdownField extends StatelessWidget {
  final String hintText;
  final double? borderRadius;
  final double? width;
  final double? marginVertical;
  final String value;
  final List<String> list;
  final ValueChanged<String?> onChanged;
  final bool? isRequired;

  RoundedDropdownField({
    Key? key,
    required this.hintText,
    this.borderRadius,
    this.width,
    this.marginVertical,
    required this.value,
    required this.list,
    required this.onChanged,
    this.isRequired,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: marginVertical ?? Constants.defaultPadding/2),
      width: width ?? size.width,
      padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
      decoration: BoxDecoration(
        border: Border.all(
          color: isRequired !=null && isRequired == true ? Colors.red : Theme.of(context).colorScheme.primaryVariant,
        ),
        color: Theme.of(context).colorScheme.background,
        borderRadius: BorderRadius.circular(borderRadius ?? 0.0),
      ),
      child: DropdownButtonFormField(
        decoration: InputDecoration(
            hoverColor: Colors.orange,
            hintText: hintText,
            border: InputBorder.none,
          isDense: true,
        ),
        value: value,
        style: TextStyle(color: Theme.of(context).colorScheme.onBackground, fontSize: Constants.sText),
        icon: Icon(Icons.keyboard_arrow_down),
        items: list.map((e) => DropdownMenuItem(
                  value: e,
                  child: Text(e),
                ))
            .toList(),
        onChanged: onChanged,
      ),
    );
  }
}
