import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

class CustomInputGrid extends StatelessWidget {
  final Function(String) onTap;
  CustomInputGrid({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: LayoutBuilder(builder: (ctx, constraints) {
        print(constraints.maxHeight);
        return ResponsiveGridRow(
          children: [
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "<",
            "0",
            "."
          ].map((e) {
            return ResponsiveGridCol(
              xs: 4,
              md: 4,
              sm: 4,
              xl: 4,
              lg: 4,
              child: InkWell(
                onTap: () {
                  onTap(e);
                },
                child: Container(
                  height: constraints.maxHeight / 4,
                  alignment: Alignment(0, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black45, width: 0.2),
                  ),
                  child: e=="<"?Icon(Icons.backspace_outlined,color: Colors.black,):Text(
                    e,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: Constants.lText,
                    ),
                  ),
                ),
              ),
            );
          }).toList(),
        );
      }),
    );
  }
}
