import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData? icon;
  final double? borderRadius;
  final double? paddingVertical;
  final double? paddingHorizontal;
  final double? marginVertical;
  final double? fontSize;
  final Color? containerColor;
  final Color? borderColor;
  final double? width;
  final double? contentPadding;
  final ValueChanged<String>? onChanged;
  final Function()? onTap;
  final TextEditingController? controller;
  final bool? isEnabled;
  final bool isReadOnly;
  final int? maxLines;
  final TextInputType? inputType;
  final Color? hintColor;

  const RoundedInputField({
    Key? key,
    required this.hintText,
    this.icon,
    this.borderRadius,
    this.paddingVertical,
    this.paddingHorizontal,
    this.marginVertical,
    this.fontSize,
    this.containerColor,
    this.borderColor,
    this.width,
    this.contentPadding,
    this.onChanged,
    this.onTap,
    this.controller,
    this.isEnabled,
    this.isReadOnly = false,
    this.maxLines,
    this.inputType,
    this.hintColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: marginVertical ?? Constants.defaultPadding / 2),
      width: width ?? size.width,
      padding: EdgeInsets.symmetric(
        vertical: paddingVertical ?? Constants.defaultPadding / 4,
        horizontal: paddingHorizontal ?? Constants.defaultPadding / 2,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor ?? Theme.of(context).colorScheme.primaryVariant,
        ),
        color: containerColor ?? Theme.of(context).colorScheme.background,
        borderRadius: BorderRadius.circular(borderRadius ?? 0.0),
      ),
      child: TextField(
        controller: controller,
        onChanged: onChanged,
        onTap: onTap,
        maxLines: maxLines,
        enabled: isEnabled,
        showCursor: true,
        readOnly: isReadOnly,
        keyboardType: inputType,
        style: TextStyle(
          color: Theme.of(context).colorScheme.onBackground,
          fontSize: fontSize ?? Constants.mText,
        ),
        cursorColor: Theme.of(context).colorScheme.primary,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: contentPadding ?? 0.0),
            hoverColor: Colors.orange,
            icon: icon != null
                ? Icon(
                    icon,
                    color: Theme.of(context).colorScheme.primaryVariant,
                  )
                : null,
            hintText: hintText,
            hintStyle: TextStyle(color: hintColor ?? Colors.grey),
            border: InputBorder.none),
      ),
    );
  }
}
