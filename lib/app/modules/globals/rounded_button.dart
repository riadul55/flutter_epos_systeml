import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RoundedButton extends StatelessWidget {
  final Function() press;
  final String text;
  final double? width;
  final double? radius;
  final double? margin;
  final double? padding;
  final double? textSize;
  final Color? color;
  final Color? textColor;

  const RoundedButton({
    Key? key,
    required this.press,
    required this.text,
    this.color,
    this.textColor,
    this.textSize,
    this.width,
    this.radius,
    this.margin,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: margin ?? Constants.defaultPadding / 2),
      width: width ?? Get.width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: color ?? Theme.of(context).colorScheme.primary,
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(radius ?? Constants.defaultPadding + 10),
          ),
        ),
        onPressed: press,
        child: Container(
          padding: EdgeInsets.all(padding ?? Constants.defaultPadding * 0.8),
          child: Text(
            text,
            style: TextStyle(
                color: textColor ?? Colors.white,
                fontSize: textSize ?? Constants.sText,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
