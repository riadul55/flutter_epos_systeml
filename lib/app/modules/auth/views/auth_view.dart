import 'package:epos_system/app/modules/auth/views/screens/config_screen.dart';
import 'package:epos_system/app/modules/auth/views/screens/login_screen.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/auth_controller.dart';
import 'screens/register_screen.dart';

class AuthView extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: PageView(
          controller: controller.pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: controller.onPageChanged,
          children: [
            ConfigScreen(),
            LoginScreen(),
            RegisterScreen(),
          ],
        ),
      ),
    );
  }
}