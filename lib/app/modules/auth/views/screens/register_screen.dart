import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_dropdown_field.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/routes/app_pages.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Card(
          elevation: Constants.defaultElevation,
          child: Container(
            width: Constants.formWidth,
            padding: EdgeInsets.all(Constants.defaultPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    "Register Account",
                    style: TextStyle(
                      fontSize: Constants.lText,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(height: Constants.defaultPadding),
                Text(
                  "User Name",
                  style: TextStyle(
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                RoundedInputField(
                  paddingVertical: 0,
                  hintText: "username",
                  onChanged: (val) {},
                ),
                Text(
                  "Role",
                  style: TextStyle(
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                RoundedDropdownField(
                  hintText: "select role",
                  value: "Provider",
                  list: ["Provider", "Admin"],
                  onChanged: (val) {},
                ),
                Text(
                  "Local Password",
                  style: TextStyle(
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                RoundedInputField(
                  paddingVertical: 0,
                  hintText: "password",
                  onChanged: (val) {},
                ),
                Text(
                  "Cloud Password",
                  style: TextStyle(
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                RoundedInputField(
                  paddingVertical: 0,
                  hintText: "password",
                  onChanged: (val) {},
                ),
                RoundedButton(
                  padding: Constants.defaultPadding / 2,
                  radius: 0,
                  text: "Submit",
                  press: () {
                    Get.offAllNamed(Routes.HOME);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
