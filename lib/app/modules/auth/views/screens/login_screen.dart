import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/auth/controllers/auth_controller.dart';
import 'package:epos_system/app/modules/auth/controllers/login_controller.dart';
import 'package:epos_system/app/modules/globals/custom_input_grid.dart';
import 'package:epos_system/app/core/utilities/helpers.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  LoginController get controller => Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: Get.width * 0.8,
          height: Get.height * 0.7,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        color: Colors.grey.shade300,
                        padding: EdgeInsets.all(Constants.defaultPadding),
                        child: Center(
                          child: TextField(
                            controller: controller.passInputController,
                            enableInteractiveSelection: false,
                            enabled: false,
                            readOnly: true,
                            focusNode: FocusNode(),
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Enter Password',
                              hintStyle: TextStyle(fontSize: Constants.mText),
                            ),
                          ),
                        ),
                      ),
                      CustomInputGrid(onTap: (e) {
                        if (e == "<") {
                          removeLastCharacter(controller.passInputController);
                        } else {
                          controller.passInputController.text += e;
                        }
                      }),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(Constants.defaultPadding),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              bottom: Constants.defaultPadding * 3),
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Image.asset(Constants.APP_LOGO,
                                fit: BoxFit.contain),
                          ),
                        ),
                        RoundedButton(
                          text: "Login",
                          padding: Constants.defaultPadding-5,
                          radius: 0,
                          textSize: Constants.mText,
                          press: () {
                            controller.submit();
                          },
                        ),

                        TextButton(
                            onPressed: () {
                          controller.authController.pageController.jumpToPage(2);
                          },
                            child: Text("Registration")),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
