import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/auth/controllers/config_controller.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfigScreen extends StatelessWidget {
  ConfigScreen({Key? key}) : super(key: key);

  ConfigController get controller => Get.put(ConfigController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Card(
              elevation: Constants.defaultElevation,
              child: Container(
                width: Constants.formWidth,
                margin: EdgeInsets.all(Constants.defaultPadding),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Epos Mode",
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: Constants.mText,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        Expanded(
                          child: Row(
                            children: controller.eposModeList
                                .asMap()
                                .map((key, value) => MapEntry(
                                    key,
                                    Expanded(
                                      flex: 1,
                                      child: RadioListTile(
                                        title: Text(
                                          value,
                                          style: TextStyle(
                                              fontSize: Constants.sText),
                                        ),
                                        groupValue: key,
                                        value: controller.eposModeIndex.value,
                                        onChanged: (val) {
                                          controller.eposModeIndex(key);
                                        },
                                      ),
                                    )))
                                .values
                                .toList(),
                          ),
                        ),
                      ],
                    ),
                    divider(context),
                    controller.eposModeIndex.value == 0
                        ? Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text("URL Mode",
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onBackground,
                                          fontSize: Constants.mText,
                                          fontWeight: FontWeight.bold,
                                        )),
                                  ),
                                  Expanded(
                                    child: Row(
                                      children: controller.eposUrlList
                                          .asMap()
                                          .map((key, value) => MapEntry(
                                              key,
                                              Expanded(
                                                flex: 1,
                                                child: RadioListTile(
                                                  title: Text(
                                                    value,
                                                    style: TextStyle(
                                                        fontSize:
                                                            Constants.sText),
                                                  ),
                                                  groupValue: key,
                                                  value: controller
                                                      .eposUrlIndex.value,
                                                  onChanged: (val) {
                                                    controller
                                                        .eposUrlIndex(key);
                                                  },
                                                ),
                                              )))
                                          .values
                                          .toList(),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Text("URL",
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onBackground,
                                          fontSize: Constants.mText,
                                          fontWeight: FontWeight.bold,
                                        )),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: RoundedInputField(
                                      controller:
                                          controller.urlInputTextController,
                                      paddingVertical: 0,
                                      hintText: "https://...",
                                      borderRadius: 5,
                                      borderColor: controller
                                              .isInputRequired.value
                                          ? Theme.of(context).colorScheme.error
                                          : null,
                                      onChanged: (value) {
                                        controller.isInputRequired(false);
                                      },
                                    ),
                                  ),
                                ],
                              )
                            ],
                          )
                        : SizedBox.shrink(),
                    divider(context),
                    RoundedButton(
                      padding: Constants.defaultPadding / 2,
                      radius: 0.0,
                      text: "Submit",
                      press: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        controller.submit();
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
