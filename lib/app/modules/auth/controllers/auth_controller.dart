import 'dart:async';
import 'dart:isolate';

import 'package:epos_system/app/core/utilities/my_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AuthController extends GetxController {
  var storage = Get.put(MyStorage());
  late PageController pageController;

  @override
  void onInit() async {
    pageController = PageController(initialPage: 0);
    if (Get.width < 800) {
      print("landscape");
      await SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
      ]);
    }
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    var value = storage.appConfig.val;
    print("first time ===> $value");
    if (value.isNotEmpty) {
      pageController.jumpToPage(1);
    }
  }

  @override
  void onClose() {
  }

  void onPageChanged(int page) {
  }
}
