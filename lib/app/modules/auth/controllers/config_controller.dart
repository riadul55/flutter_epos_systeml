import 'package:epos_system/app/core/utilities/my_storage.dart';
import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:epos_system/main/controllers/main_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'auth_controller.dart';

class ConfigController extends GetxController {
  final storage = Get.find<MyStorage>();

  List<String> eposModeList = ["Online", "Offline"];
  List<String> eposUrlList = ["Test", "Prod"];
  var eposModeIndex = 0.obs;
  var eposUrlIndex = 0.obs;

  MainController mainController = Get.find();
  AuthController authController = Get.find();

  /// input controllers
  var urlInputTextController = TextEditingController();
  var isInputRequired = false.obs;

  @override
  void onInit() async {
    super.onInit();
  }

  void submit() {
    // AppDb db = getIt();
    // db.into(db.testTable).insert(Test(id: 2, title: "title dfd af", content: "content dfad fafa sfdas"));
    // IsolateWorker(event: "progress", onListen: (value) {
    //   print(value);
    // }).init();

    if (validate()) {
      bool isOnline = eposModeList[eposModeIndex.value] == "Online";
      storage.appConfig.val = <String, String>{
        StorageKey.CONFIG_APP_MODE : eposModeList[eposModeIndex.value],
        StorageKey.CONFIG_URL_MODE : isOnline ? eposUrlList[eposUrlIndex.value] : "",
        StorageKey.CONFIG_BASE_URL : isOnline ? urlInputTextController.text : "",
      };
      authController.pageController.jumpToPage(1);
    }
  }

  bool validate() {
    if (eposModeList[eposModeIndex.value] == "Online") {
      if (urlInputTextController.text.isEmpty) {
        isInputRequired(true);
        return false;
      }
    }
    return true;
  }
}
