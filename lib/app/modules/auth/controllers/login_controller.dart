import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'auth_controller.dart';

class LoginController extends GetxController {

  /// input controllers
  var passInputController = TextEditingController();

  AuthController authController = Get.find();

  @override
  void onInit() async {
    super.onInit();
  }

  void submit() {
    if (validate()) {
      authController.pageController.jumpToPage(2);
    }
  }

  bool validate() {
    if (passInputController.text.isEmpty) {
      showToast(
          title: "Required!",
          message: "Password is empty",
          bgColor: Colors.red,
          textColor: Colors.white);
      return false;
    } else if (passInputController.text != "0000") {
      showToast(
          title: "Required!",
          message: "Password doesn't matched",
          bgColor: Colors.red,
          textColor: Colors.white);
      return false;
    }
    return true;
  }
}
