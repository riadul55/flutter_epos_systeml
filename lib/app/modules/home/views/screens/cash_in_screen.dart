import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/custom_input_grid.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/home/views/components/cash_drawer_bottom_bar.dart';
import 'package:flutter/material.dart';

class CashInScreen extends StatelessWidget {
  CashInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          alignment: Alignment.topCenter,
          child: Container(
            width: Constants.formWidth,
            child: Card(
              elevation: Constants.defaultElevation,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: Constants.defaultPadding / 2),
                    decoration: bottomBorderBox(context),
                    child: Row(
                      children: [
                        Expanded(
                            child: HeaderTitle(
                          title: "Cash In",
                          borderColor: Colors.transparent,
                          fontSize: Constants.xlText,
                        )),
                        Container(
                          padding: EdgeInsets.all(Constants.defaultPadding / 4),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                          child: Text(
                            "04/01/2022",
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.onSecondary,
                              fontSize: Constants.mText,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: bottomBorderBox(context),
                    padding: EdgeInsets.symmetric(
                      horizontal: Constants.defaultPadding,
                      vertical: Constants.defaultPadding / 2,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Balance",
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: Constants.xlText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "1000.0",
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: Constants.xlText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    color: Theme.of(context)
                        .colorScheme
                        .primaryVariant
                        .withOpacity(0.5),
                    padding: EdgeInsets.all(Constants.defaultPadding),
                    child: Center(
                      child: TextField(
                        enableInteractiveSelection: false,
                        enabled: false,
                        readOnly: true,
                        focusNode: FocusNode(),
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Amount',
                          hintStyle: TextStyle(fontSize: Constants.mText),
                        ),
                      ),
                    ),
                  ),
                  CustomInputGrid(onTap: (e) {}),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: CashDrawerBottomBar(
        btnTitle: "Cash In",
      ),
    );
  }
}
