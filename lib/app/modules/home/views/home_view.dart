import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';
import 'components/bottom_nav_home.dart';
import 'components/custom_appbar.dart';
import 'components/drawer_home.dart';
import '../../orders/views/screens/orders_screen.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: controller.scaffoldState,
      appBar: CustomAppBar(),
      drawer: DrawerHome(),
      body: Obx(
        () => controller.isSearchingEnabled.value
            ? OrdersScreen()
            : PageView(
                controller: controller.pageController,
                physics: NeverScrollableScrollPhysics(),
                children:
                    controller.screenTitles.map((e) => OrdersScreen()).toList(),
              ),
      ),
      bottomNavigationBar: BottomNavHome(),
    );
  }
}
