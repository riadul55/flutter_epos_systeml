import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/home/controllers/home_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/home/views/screens/cash_in_screen.dart';
import 'package:epos_system/app/modules/home/views/screens/cash_out_screen.dart';
import 'package:epos_system/app/modules/products/views/screens/main_products_view.dart';
import 'package:epos_system/app/modules/products/views/screens/product_link_view.dart';
import 'package:epos_system/app/modules/products/views/screens/sub_products_view.dart';
import 'package:epos_system/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item_drawer_home.dart';

class DrawerHome extends GetView {
  DrawerHome({Key? key}) : super(key: key);

  @override
  HomeController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
              child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Constants.defaultPadding,
              vertical: Constants.defaultPadding / 2,
            ),
            child: Image.asset(Constants.APP_LOGO),
          )),
          // SizedBox(height: Constants.defaultPadding,),
          ItemDrawerHome(
            title: "Active Orders",
            leadIcon: Icons.shopping_cart_outlined,
            onTap: () {
              controller.showScreen(0);
            },
          ),
          ItemDrawerHome(
            title: "Closed Orders",
            leadIcon: Icons.shopping_cart_outlined,
            onTap: () {
              controller.showScreen(1);
            },
          ),
          ItemDrawerHome(
            title: "Refunded Orders",
            leadIcon: Icons.shopping_cart_outlined,
            onTap: () {
              controller.showScreen(2);
            },
          ),
          ItemDrawerHome(
            title: "Cancelled Orders",
            leadIcon: Icons.shopping_cart_outlined,
            onTap: () {
              controller.showScreen(3);
            },
          ),
          ItemDrawerHome(
            title: "Refresh Data",
            leadIcon: Icons.refresh,
            onTap: () {
              controller.refreshData(context);
              // controller.productRepository.repository.getCategories();
              // controller.productRepository.repository.getProducts();
              // controller.globalRepository.repository.getGlobalParameter();
            },
          ),

          divider(context),
          ItemDrawerHome(
            title: "Category",
            leadIcon: Icons.category,
            onTap: () {
              controller.scaffoldState.currentState?.openEndDrawer();
              Get.toNamed(Routes.CATEGORY);
            },
          ),
          ExpansionTile(
            leading: Icon(
              Icons.volunteer_activism,
              color: Theme.of(context).colorScheme.onSecondary,
            ),
            collapsedIconColor: Theme.of(context).colorScheme.onSecondary,
            iconColor: Theme.of(context).colorScheme.onSecondary,
            title: Text("Products",
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onSecondary,
                  fontSize: Constants.mText,
                )),
            children: [
              ItemDrawerHome(
                title: "Product List",
                leadIcon: Icons.subdirectory_arrow_right,
                onTap: () {
                  controller.scaffoldState.currentState?.openEndDrawer();
                  Get.toNamed(Routes.PRODUCTS);
                },
              ),
              ItemDrawerHome(
                title: "Add Product",
                leadIcon: Icons.subdirectory_arrow_right,
                onTap: () {
                  controller.scaffoldState.currentState?.openEndDrawer();
                  Get.toNamed(Routes.PRODUCTS_UPDATE);
                },
              ),
              ItemDrawerHome(
                title: "Add Sub Product",
                leadIcon: Icons.subdirectory_arrow_right,
                onTap: () {
                  controller.scaffoldState.currentState?.openEndDrawer();
                  Get.to(() => SubProductsView());
                },
              ),
            ],
          ),
          ItemDrawerHome(
            title: "Link",
            leadIcon: Icons.link,
            onTap: () {
              controller.scaffoldState.currentState?.openEndDrawer();
              Get.to(() => ProductLinkView());
            },
          ),
          divider(context),

          ExpansionTile(
            leading: Icon(
              Icons.attach_money,
              color: Theme.of(context).colorScheme.onSecondary,
            ),
            collapsedIconColor: Theme.of(context).colorScheme.onSecondary,
            iconColor: Theme.of(context).colorScheme.onSecondary,
            title: Text("Cash Drawer",
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onSecondary,
                  fontSize: Constants.mText,
                )),
            children: [
              ItemDrawerHome(
                title: "Cash In",
                leadIcon: Icons.subdirectory_arrow_right,
                onTap: () {
                  controller.scaffoldState.currentState?.openEndDrawer();
                  Get.to(() => CashInScreen());
                },
              ),
              ItemDrawerHome(
                title: "Cash Out",
                leadIcon: Icons.subdirectory_arrow_right,
                onTap: () {
                  controller.scaffoldState.currentState?.openEndDrawer();
                  Get.to(() => CashOutScreen());
                },
              ),
            ],
          ),
          divider(context),

          ItemDrawerHome(
            title: "Daily Report",
            leadIcon: Icons.notifications,
            onTap: () {},
          ),
          ItemDrawerHome(
            title: "Shift Close",
            leadIcon: Icons.close,
            onTap: () {},
          ),
          ItemDrawerHome(
            title: "Settings",
            leadIcon: Icons.settings,
            onTap: () {
              controller.scaffoldState.currentState?.openEndDrawer();
              Get.toNamed(Routes.SETTING);
            },
          ),
          ItemDrawerHome(
            title: "Delete Order Data",
            leadIcon: Icons.delete_outline,
            onTap: () {},
          ),
          ItemDrawerHome(
            title: "Log Out",
            leadIcon: Icons.delete_outline,
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
