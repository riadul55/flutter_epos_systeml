import 'package:epos_system/app/modules/home/controllers/home_controller.dart';
import 'package:epos_system/app/routes/app_pages.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item_bottom_nav.dart';

class BottomNavHome extends GetView implements PreferredSizeWidget {
  BottomNavHome({Key? key}) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(Constants.preferredSize);

  @override
  HomeController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: preferredSize.height,
      color: Theme.of(context).colorScheme.secondaryVariant,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              InkWell(
                child: Container(
                  width: preferredSize.height,
                  height: preferredSize.height,
                  color: Theme.of(context).colorScheme.secondary,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.menu),
                      Text(
                        "Menu",
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.onSecondary,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  controller.scaffoldState.currentState?.openDrawer();
                },
              ),
              SizedBox(
                width: Constants.defaultPadding,
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Total Order",
                    style: TextStyle(
                      fontSize: Constants.sText,
                      color: Theme.of(context).colorScheme.background,
                    ),
                  ),
                  Text(
                    "1",
                    style: TextStyle(
                      fontSize: Constants.sText,
                      color: Theme.of(context).colorScheme.background,
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: Constants.defaultPadding,
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Online Order",
                    style: TextStyle(
                      fontSize: Constants.sText,
                      color: Theme.of(context).colorScheme.background,
                    ),
                  ),
                  Text(
                    "0",
                    style: TextStyle(
                      fontSize: Constants.sText,
                      color: Theme.of(context).colorScheme.background,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            width: Get.width * 0.2,
            height: preferredSize.height,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ItemBottomNav(
                  label: "Collection",
                  icon: Icons.volunteer_activism,
                  onTap: () {
                    Get.toNamed(Routes.ORDER_CREATE);
                  },
                ),
                ItemBottomNav(
                  label: "Delivery",
                  icon: Icons.local_shipping,
                  onTap: () {
                    Get.toNamed(Routes.ORDER_CREATE);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
