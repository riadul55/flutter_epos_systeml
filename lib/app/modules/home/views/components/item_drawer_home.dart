import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class ItemDrawerHome extends StatelessWidget {
  final String title;
  final IconData leadIcon;
  final Function() onTap;

  ItemDrawerHome({
    Key? key,
    required this.title,
    required this.leadIcon,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        leadIcon,
        color: Theme.of(context).colorScheme.onSecondary,
      ),
      title: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).colorScheme.onSecondary,
          fontSize: Constants.mText,
        ),
      ),
      onTap: onTap,
    );
  }
}
