
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class ItemBottomNav extends StatelessWidget {
  final IconData icon;
  final String label;
  final Function() onTap;

  ItemBottomNav({
    Key? key,
    required this.icon,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(icon, size: Constants.iconSize, color: Theme.of(context).colorScheme.onSecondary,),
                  Text(
                    label,
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onSecondary,
                      fontSize: Constants.sText,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
