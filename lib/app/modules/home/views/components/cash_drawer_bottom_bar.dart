import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CashDrawerBottomBar extends StatelessWidget {
  final String btnTitle;

  const CashDrawerBottomBar({
    Key? key,
    required this.btnTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.secondaryVariant,
      height: Constants.preferredSize,
      child: Stack(
        children: [
          Material(
            color: Theme.of(context).colorScheme.secondary,
            child: InkWell(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Back",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: Constants.defaultPadding / 5,
              ),
              width: Constants.formWidth,
              child: RoundedButton(
                text: btnTitle,
                radius: 0,
                margin: 0,
                press: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}
