import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/home/controllers/home_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomAppBar extends GetView implements PreferredSizeWidget {
  CustomAppBar({Key? key}) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(Constants.preferredSize);

  @override
  HomeController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: preferredSize.height,
        color: Theme.of(context).colorScheme.secondary,
        child: Obx(() => Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                    child: Row(
                      children: [
                        Icon(
                          Icons.local_shipping,
                          size: Constants.iconSize,
                          color: Theme.of(context).colorScheme.background,
                        ),
                        SizedBox(
                          width: Constants.defaultPadding,
                        ),
                        Text(
                          controller.isSearchingEnabled.value
                              ? "Search Order".toUpperCase()
                              : controller.appBarTitle.value.toUpperCase(),
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.background,
                              fontSize: Constants.mText),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  // child: SingleChildScrollView(
                  //   scrollDirection: Axis.horizontal,
                  //   child: Row(
                  //     children: controller.appBarButtonList
                  //         .asMap()
                  //         .map((key, value) => MapEntry(
                  //             key,
                  //             RoundedButton(
                  //               press: () {
                  //                 controller.appBarButtonSelectedIndex(key);
                  //               },
                  //               text: value.toUpperCase(),
                  //               width: Get.width < Constants.defaultBreakPoint ? 105 : 120,
                  //               padding: 0,
                  //               margin: 0,
                  //               radius: 0,
                  //               color: key ==
                  //                       controller.appBarButtonSelectedIndex.value
                  //                   ? Theme.of(context).colorScheme.primary
                  //                   : Theme.of(context).colorScheme.background,
                  //               textColor: key ==
                  //                   controller.appBarButtonSelectedIndex.value
                  //                   ? Theme.of(context).colorScheme.onPrimary
                  //                   : Theme.of(context).colorScheme.onBackground,
                  //               textSize: Get.width < Constants.defaultBreakPoint
                  //                   ? Constants.xsText
                  //                   : Constants.sText,
                  //             )))
                  //         .values
                  //         .toList(),
                  //   ),
                  // ),
                  child: Container(
                    color: Theme.of(context)
                        .colorScheme
                        .secondaryVariant
                        .withOpacity(0.2),
                    child: TabBar(
                      isScrollable: true,
                      labelColor: Theme.of(context).colorScheme.onPrimary,
                      unselectedLabelColor:
                      Theme.of(context).colorScheme.onSecondary,
                      indicatorColor: Theme.of(context).colorScheme.primary,
                      indicator: BoxDecoration(
                        color: Theme.of(context).colorScheme.primary,
                      ),
                      controller: controller.tabController,
                      tabs: controller.appBarButtonList
                          .asMap().map((key, value) => MapEntry(key, Tab(
                        child: Text(value.toUpperCase(), style: GoogleFonts.roboto(),),
                      ))).values.toList(),
                      onTap: (index) {
                        controller.appBarButtonSelectedIndex(index);
                      },
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: RoundedInputField(
                    // contentPadding: 10,
                    marginVertical: 0,
                    paddingVertical: 0,
                    hintText: "Search Orders...",
                    maxLines: 1,
                    onChanged: (val) {
                      controller.searchInputValue(val);
                    },
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
