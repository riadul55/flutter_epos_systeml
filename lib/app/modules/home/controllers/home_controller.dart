import 'package:epos_system/app/data/remote_db/bindings/global.parameter.repository.binding.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';

import '../../../data/remote_db/bindings/product.repository.binding.dart';

class HomeController extends GetxController with GetSingleTickerProviderStateMixin {
  var searchInputValue = "".obs;
  var isSearchingEnabled = false.obs;

  final GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

  var appBarButtonList = ["all", "online", "collection", "delivery"];
  var appBarButtonSelectedIndex = 0.obs;

  late PageController pageController;

  var screenTitles = [
    "Active Orders",
    "Closed Orders",
    "Refunded Orders",
    "Cancelled Orders",
  ];
  var appBarTitle = "".obs;


  late TabController tabController;

  late ProductRepositoryBinding productRepository;
  late GlobalParameterRepositoryBinding globalRepository;

  @override
  void onInit() {
    productRepository = ProductRepositoryBinding();
    globalRepository = GlobalParameterRepositoryBinding();
    tabController = TabController(length: appBarButtonList.length, vsync: this, initialIndex: 0);
    pageController = PageController(initialPage: 0);
    super.onInit();
    showScreen(0);

    debounce(searchInputValue, (value) {
      if (value.isBlank! && value.toString().isEmpty) {
        isSearchingEnabled(false);
      } else {
        isSearchingEnabled(true);
      }
      print(value);
    });

    tabController.addListener(() {

    });

    // IsolateWorker(event: "progress", onListen: (value) {
    //   print(value);
    // }).init();
  }

  showScreen(index) {
    if (pageController.hasClients) {
      pageController.jumpToPage(index);
    }
    appBarTitle(screenTitles[index]);
    scaffoldState.currentState?.openEndDrawer();
  }

  @override
  void onClose() {}

  Future<void> refreshData(BuildContext  context,) async{
    ProgressDialog pd = ProgressDialog(context: context);
    pd.show(
      max: 100,
      hideValue: true,
      msg: 'Loading data...',
      progressBgColor: Colors.transparent,
    );
    await globalRepository.repository.getGlobalParameter();

    await productRepository.repository.getCategories();

    await productRepository.repository.getProducts().then((value) {
      pd.close();
      Get.defaultDialog(title: "Success",
          content: Text("Data Loading Successful"),
          actions:[TextButton(onPressed: (){Get.back();}, child: Text("OK"))]);
    }).catchError((error, stackTrace){
      print(error);
      pd.close();
    });
  }
}
