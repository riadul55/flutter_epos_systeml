import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/orders/views/components/payment_info.dart';
import 'package:flutter/material.dart';

import '../../../globals/animated_dialog.dart';
import 'customer_info_item.dart';
import 'info_tile.dart';

class CustomerInfo extends StatelessWidget {
  CustomerInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        child: ListView(
          children: [
            HeaderTitle(title: "Customer Information"),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: Constants.defaultPadding / 2),
              child: Column(
                children: [
                  CustomerInfoItem(title: "First Name", value: "N/A"),
                  CustomerInfoItem(title: "Last Name", value: "N/A"),
                  CustomerInfoItem(title: "Phone", value: "N/A"),
                  CustomerInfoItem(title: "Email", value: "N/A"),
                ],
              ),
            ),

            HeaderTitle(title: "Address"),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: Constants.defaultPadding / 2),
              child: Column(
                children: [
                  CustomerInfoItem(title: "House Number", value: "N/A"),
                  CustomerInfoItem(title: "Street Name", value: "N/A"),
                  CustomerInfoItem(title: "Post Code", value: "N/A"),
                  CustomerInfoItem(title: "Town", value: "N/A"),
                ],
              ),
            ),
            SizedBox(height: Constants.defaultPadding),
            InfoTile(
                icon: Icons.access_time,
                title: "Delivery Time",
                subTitle: "Asap",
                press: (){
                  showAnimDialog(context,DeliveryTimeDialog(orderType: "Collection",), );
                },),

            SizedBox(height: Constants.defaultPadding),
            HeaderTitle(title: "Comments"),
          ],
        ),
      ),
    );
  }
}
