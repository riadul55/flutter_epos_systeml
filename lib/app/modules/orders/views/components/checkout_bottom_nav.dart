import 'package:epos_system/app/routes/app_pages.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CheckoutBottomNav extends StatelessWidget {
  CheckoutBottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.secondaryVariant,
      height: Constants.preferredSize,
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Material(
                    color: Theme.of(context).colorScheme.secondary,
                    child: InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.arrow_back,
                              size: Constants.iconSize,
                              color: Theme.of(context).colorScheme.onSecondary,
                            ),
                            Text(
                              "Back",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Material(
                    color: Theme.of(context).colorScheme.secondaryVariant,
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.print,
                              size: Constants.iconSize,
                              color: Theme.of(context).colorScheme.onSecondary,
                            ),
                            Text(
                              "Print",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
          Expanded(
              flex: 5,
              child: Row(
                children: [
                  Expanded(
                      child: Material(
                    color: Theme.of(context).colorScheme.primary,
                    child: InkWell(
                      onTap: () {
                        Get.toNamed(Routes.ORDER_DETAILS);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: Constants.defaultPadding,
                          vertical: Constants.defaultPadding / 2,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(Constants.IC_PAY_NOW),
                            SizedBox(width: Constants.defaultPadding / 2),
                            Text(
                              "Pay Now".toUpperCase(),
                              style: TextStyle(
                                fontSize: Constants.mText,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )),
                  SizedBox(width: Constants.defaultPadding / 5),
                  Expanded(
                      child: Material(
                        color: Theme.of(context).colorScheme.primary,
                        child: InkWell(
                          onTap: () {},
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: Constants.defaultPadding,
                              vertical: Constants.defaultPadding / 2,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(Constants.IC_PAY_LATER),
                                SizedBox(width: Constants.defaultPadding / 2),
                                Text(
                                  "Pay Later".toUpperCase(),
                                  style: TextStyle(
                                    fontSize: Constants.mText,
                                    fontWeight: FontWeight.bold,
                                    color:
                                    Theme.of(context).colorScheme.onSecondary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )),
                ],
              )),
          Expanded(
              flex: 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Material(
                    color: Theme.of(context).colorScheme.secondaryVariant,
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.close,
                              size: Constants.iconSize,
                              color: Colors.red.shade900,
                            ),
                            Text(
                              "Void",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Material(
                    color: Theme.of(context).colorScheme.secondaryVariant,
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.email,
                              size: Constants.iconSize,
                              color: Theme.of(context).colorScheme.onSecondary,
                            ),
                            Text(
                              "Send",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
