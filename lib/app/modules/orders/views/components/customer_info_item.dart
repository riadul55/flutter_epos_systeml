import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class CustomerInfoItem extends StatelessWidget {
  final String title;
  final String value;

  CustomerInfoItem({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: Constants.defaultPadding / 2),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              value,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
