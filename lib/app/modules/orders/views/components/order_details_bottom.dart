import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderDetailsBottom extends StatelessWidget {
  const OrderDetailsBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.secondaryVariant,
      height: Constants.preferredSize,
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Material(
                    color: Theme.of(context).colorScheme.secondary,
                    child: InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.arrow_back,
                              size: Constants.iconSize,
                              color: Theme.of(context).colorScheme.onSecondary,
                            ),
                            Text(
                              "Back",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
          Expanded(
              flex: 5,
              child: Container()),
          Expanded(
              flex: 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Material(
                    color: Theme.of(context).colorScheme.secondaryVariant,
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.redo,
                              size: Constants.iconSize,
                              color: Colors.red.shade900,
                            ),
                            Text(
                              "Refund",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Material(
                    color: Theme.of(context).colorScheme.secondaryVariant,
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding),
                        child: Column(
                          children: [
                            Icon(
                              Icons.print,
                              size: Constants.iconSize,
                              color: Theme.of(context).colorScheme.onSecondary,
                            ),
                            Text(
                              "Print",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                Theme.of(context).colorScheme.onSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
