import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/orders/controllers/checkout_controller.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:get/get.dart';

import '../../../globals/animated_dialog.dart';
import '../../../globals/rounded_button.dart';
import 'customer_info_item.dart';
import 'info_tile.dart';

class PaymentInfo extends StatelessWidget {
  final String orderType;
  PaymentInfo({Key? key, required this.orderType}) : super(key: key);

  CheckoutController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        child: ListView(
          children: [
            HeaderTitle(title: "Payment Method".toUpperCase()),
            // divider(context),
            Obx(() => Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding / 2,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: controller.paymentMethods
                        .asMap()
                        .map((key, value) => MapEntry(
                            key,
                            Expanded(
                              child: Material(
                                color: key != controller.selectedPayMethod.value
                                    ? Theme.of(context)
                                        .colorScheme
                                        .primaryVariant
                                    : Theme.of(context).colorScheme.primary,
                                child: InkWell(
                                  onTap: () {
                                    controller.selectedPayMethod(key);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.all(
                                        Constants.defaultPadding / 3),
                                    child: Text(
                                      value,
                                      style: TextStyle(
                                        fontSize: Constants.mText,
                                        color: key !=
                                                controller
                                                    .selectedPayMethod.value
                                            ? Theme.of(context)
                                                .colorScheme
                                                .onBackground
                                            : Theme.of(context)
                                                .colorScheme
                                                .onPrimary,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )))
                        .values
                        .toList(),
                  ),
                )),
            SizedBox(height: Constants.defaultPadding / 2),
            InfoTile(
              icon: Icons.access_time,
              title: "$orderType Time",
              subTitle: "Asap",
              press: () {
                showAnimDialog(context,DeliveryTimeDialog(orderType: orderType,));
              },
            ),
            divider(context),
            InfoTile(
              icon: Icons.local_shipping,
              title: "$orderType Charge",
              subTitle: "0.0",
              press: () {
                showAnimDialog(context,DeliveryChargeDialog(), );
              },
            ),
            divider(context),
            InfoTile(
              icon: Icons.bookmark,
              title: "Discount",
              subTitle: "Total: ",
              subValue: "-0.0",
              press: () {
                showAnimDialog(context,DiscountDialog());
              },
            ),
            divider(context),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Constants.defaultPadding / 2),
              child: RoundedInputField(
                hintText: "Comments",
                maxLines: 3,
              ),
            ),
            SizedBox(height: Constants.defaultPadding / 2),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Constants.defaultPadding / 2),
                  child: Text(
                    "Customer",
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontWeight: FontWeight.bold,
                      fontSize: Constants.mText,
                    ),
                  ),
                ),
                divider(context),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: Constants.defaultPadding / 2),
                  child: Column(
                    children: [
                      CustomerInfoItem(title: "Name", value: "N/A"),
                      CustomerInfoItem(title: "Post Code", value: "N/A"),
                      CustomerInfoItem(title: "Address", value: "N/A"),
                      CustomerInfoItem(title: "Phone", value: "N/A"),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

}

class DeliveryTimeDialog extends StatelessWidget {
  final String orderType;
  DeliveryTimeDialog({Key? key, required this.orderType}) : super(key: key);
  CheckoutController get controller => Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
        width: Get.width/2,
        child: Column(children: [
          Text("$orderType Time"),
          SizedBox(
            height: Constants.defaultPadding,
          ),
          Row(
            children: [
               Expanded(
                 child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "$orderType Date",
                          style: TextStyle(
                            fontSize: Constants.sText,
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).colorScheme.onBackground,
                          ),
                        ),
                        Obx(()=> GestureDetector(
                            onTap: (){_selectDate(context);},
                            child: RoundedInputField(
                              paddingVertical: 0,
                              marginVertical: 0,
                              contentPadding: 0,
                              hintText: "${controller.selectedDate.value.day}-${controller.selectedDate.value.month}-${controller.selectedDate.value.year} ",
                              isReadOnly: true,
                              isEnabled: false,
                              hintColor: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
               ),
              SizedBox(width: Constants.defaultPadding,),
              Expanded(child:  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "$orderType Time",
                    style: TextStyle(
                      fontSize: Constants.sText,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.onBackground,
                    ),
                  ),
                  Obx(()=> GestureDetector(
                      onTap: (){_selectTime(context);},
                      child: RoundedInputField(
                        paddingVertical: 0,
                        marginVertical: 0,
                        contentPadding: 0,
                        hintText: "${controller.selectedTime.value.hour}:${controller.selectedTime.value.minute}",
                        isReadOnly: true,
                        isEnabled: false,
                        hintColor: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),)
            ],
          ),

          SizedBox(height: Constants.defaultPadding/2,),
          Row(children: [
            Spacer(),
            Spacer(),
            Expanded(
              child: RoundedButton(
                text: "Cancel",
                padding: Constants.defaultPadding / 2,
                radius: 0,color: Colors.grey,
                press: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(width: Constants.defaultPadding,),
            Expanded(
              child: RoundedButton(
                text: "Save",
                padding: Constants.defaultPadding / 2,
                radius: 0,
                press: () {
                },
              ),
            ),
          ],)
        ]));
  }
  _selectTime(BuildContext context) async {
    final TimeOfDay? timeOfDay = await showTimePicker(
      context: context,
      initialTime: controller.selectedTime.value,
      initialEntryMode: TimePickerEntryMode.dial,
      builder: (context, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: Color.fromRGBO(255, 165, 0, 1.0),
              onPrimary: Colors.black,
              // surface: Theme.of(context).colorScheme.background,
              onSurface: Color.fromRGBO(142, 139, 139, 1.0),
            ),
          ),
          child: child!,
        );
      },
    );
    if(timeOfDay != null && timeOfDay != controller.selectedTime.value)
    {
      controller.selectedTime.value = timeOfDay;
      print(controller.selectedTime.value);
    }
  }
  _selectDate(BuildContext context) async {
    final DateTime? currentDate = await showDatePicker(
      context: context,
        initialDate: controller.selectedDate.value,
        firstDate: DateTime(2020),
        lastDate: DateTime(2050),
      builder: (context, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: Color.fromRGBO(255, 165, 0, 1.0),
              onPrimary: Colors.black,
              // surface: Theme.of(context).colorScheme.background,
              onSurface: Color.fromRGBO(142, 139, 139, 1.0),
            ),
          ),
          child: child!,
        );
      },
    );
    if(currentDate != null && currentDate != controller.selectedDate)
    {
      controller.selectedDate.value = currentDate;
      print(controller.selectedDate.value);
    }
  }
}

class DeliveryChargeDialog extends StatelessWidget {
  const DeliveryChargeDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 500,
        child: Column(children: [
          Text("Delivery Charge"),
          SizedBox(
            height: Constants.defaultPadding,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Delivery Charge",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onBackground,
                      ),
                    ),
                    RoundedInputField(
                      paddingVertical: 0,
                      marginVertical: 0,
                      contentPadding: 0,
                      hintText: "0.0",
                      inputType: TextInputType.number,
                    ),
                  ],
                ),
              ),
            ],
          ),

          SizedBox(height: Constants.defaultPadding/2,),
          Row(children: [
            Spacer(),
            Spacer(),
            Expanded(
              child: RoundedButton(
                text: "Cancel",
                padding: Constants.defaultPadding / 2,
                radius: 0,color: Colors.grey,
                press: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(width: Constants.defaultPadding,),
            Expanded(
              child: RoundedButton(
                text: "Save",
                padding: Constants.defaultPadding / 2,
                radius: 0,
                press: () {
                },
              ),
            ),
          ],)
        ]));
  }
}

class DiscountDialog extends StatelessWidget {
  const DiscountDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 500,
        child: Column(children: [
          Text("Discount"),
          SizedBox(
            height: Constants.defaultPadding,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Discount Amount",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onBackground,
                      ),
                    ),
                    RoundedInputField(
                      paddingVertical: 0,
                      marginVertical: 0,
                      contentPadding: 0,
                      hintText: "0.0",
                      inputType: TextInputType.number,
                    ),
                  ],
                ),
              ),
            ],
          ),

          SizedBox(height: Constants.defaultPadding/2,),
          Row(children: [
            Spacer(),
            Spacer(),
            Expanded(
              child: RoundedButton(
                text: "Cancel",
                padding: Constants.defaultPadding / 2,
                radius: 0,color: Colors.grey,
                press: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(width: Constants.defaultPadding,),
            Expanded(
              child: RoundedButton(
                text: "Save",
                padding: Constants.defaultPadding / 2,
                radius: 0,
                press: () {
                },
              ),
            ),
          ],)
        ]));
  }
}


