import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DataTableHeader extends StatelessWidget {
  DataTableHeader({Key? key}) : super(key: key);

  var headerList = [
    "Local",
    "Terminal",
    "Table No",
    "Server",
    "Order Date",
    "Status",
    "Customer",
    "Total",
    "Cash",
    "Card",
    "Payment",
    "Edit",
    "Details",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          //Local
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[0],
            ),
          ),
          //Terminal
          Expanded(
            flex: 2,
            child: getHeaderTextStyle(
              context,
              headerList[1],
            ),
          ),
          //Table no
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[2],
            ),
          ),
          //Server
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[3],
            ),
          ),
          //Order Date
          Expanded(
            flex: 3,
            child: getHeaderTextStyle(
              context,
              headerList[4],
            ),
          ),
          //Status
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[5],
            ),
          ),
          //Customer
          Expanded(
            flex: 3,
            child: getHeaderTextStyle(
              context,
              headerList[6],
            ),
          ),
          //Total
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[7],
            ),
          ),
          //Cash
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[8],
            ),
          ),
          //Card
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[9],
            ),
          ),
          //Payment
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[10],
            ),
          ),
          //Edit
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[11],
            ),
          ),
          //Details
          Expanded(
            flex: 1,
            child: getHeaderTextStyle(
              context,
              headerList[12],
            ),
          ),

        ],
      ),
    );
  }

  getHeaderTextStyle(BuildContext context, String text) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        text,
        style: TextStyle(
          fontSize: Get.width < Constants.defaultBreakPoint ? Constants.xsText : Constants.sText,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).colorScheme.onBackground,
        ),
      ),
    );
  }
}


// TableRow dataTableHeader(context) => TableRow(
//       decoration: BoxDecoration(
//           border: Border(
//         bottom: BorderSide(
//             color: Theme.of(context).colorScheme.secondaryVariant, width: 2),
//       )),
//       children: headerList.map((e) => Container(
//         alignment: Alignment.center,
//         padding: EdgeInsets.fromLTRB(Constants.defaultPadding / 2, 0,
//             Constants.defaultPadding / 2, Constants.defaultPadding / 1.3),
//         child: Text(
//           e,
//           style: TextStyle(
//             fontSize: Constants.sText,
//             fontWeight: FontWeight.bold,
//             color: Theme.of(context).colorScheme.onBackground,
//           ),
//         ),
//       )).toList(),
//     );

