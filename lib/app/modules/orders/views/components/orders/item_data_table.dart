import 'package:epos_system/app/data/worker/isolate_worker.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/home/controllers/orders_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ItemDataTable extends GetView {
  final bool isSelected;
  final Function() onTap;

  ItemDataTable({Key? key, required this.onTap, this.isSelected = false})
      : super(key: key);

  @override
  OrdersController get controller => Get.find();

  var statusButtonList = [
    "Accept",
    "Ready",
    "Sent",
    "Closed",
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: onTap,
          child: Card(
            elevation: 6,
            child: Container(
              color: isSelected
                  ? Theme.of(context).colorScheme.primary.withOpacity(0.2)
                  : Theme.of(context).colorScheme.background,
              child: Row(
                children: [
                  // local
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        '1',
                        style: TextStyle(
                          fontSize: Get.width < Constants.defaultBreakPoint
                              ? Constants.sText
                              : Constants.mText,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),

                  // terminal
                  Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "Collection".toUpperCase(),
                            style: TextStyle(
                                fontSize:
                                    Get.width < Constants.defaultBreakPoint
                                        ? Constants.xsText
                                        : Constants.sText,
                                fontWeight: FontWeight.bold),
                          ),
                          // SizedBox(
                          //   height: Constants.defaultPadding / 2,
                          // ),
                          Text(
                            "EPOS",
                            style: TextStyle(
                              fontSize: Get.width < Constants.defaultBreakPoint
                                  ? Constants.sText
                                  : Constants.mText,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // Table No
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Center(
                        child: Text(
                          '1',
                          style: TextStyle(
                            fontSize: Get.width < Constants.defaultBreakPoint
                                ? Constants.sText
                                : Constants.mText,
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Server
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Center(
                        child: Text(
                          '10001',
                          style: TextStyle(
                            fontSize: Get.width < Constants.defaultBreakPoint
                                ? Constants.sText
                                : Constants.mText,
                          ),
                        ),
                      ),
                    ),
                  ),
                  // order date
                  Expanded(
                    flex: 3,
                    child: Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Today 05:06 PM",
                            style: TextStyle(
                                fontSize:
                                    Get.width < Constants.defaultBreakPoint
                                        ? Constants.sText
                                        : Constants.mText,
                                fontWeight: FontWeight.bold),
                          ),
                          // SizedBox(
                          //   height: Constants.defaultPadding / 2,
                          // ),
                          Text(
                            "ASAP",
                            style: TextStyle(
                              fontSize: Get.width < Constants.defaultBreakPoint
                                  ? Constants.sText
                                  : Constants.mText,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // status
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.topCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.star_rate_outlined,
                            color: Theme.of(context).colorScheme.primary,
                            size: Get.width < Constants.defaultBreakPoint
                                ? Constants.iconSize * 0.8
                                : Constants.iconSize,
                          ),
                          Text(
                            "New".toUpperCase(),
                            style: TextStyle(
                              fontSize: Get.width < Constants.defaultBreakPoint
                                  ? Constants.sText
                                  : Constants.mText,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // customer
                  Expanded(
                    flex: 3,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        // vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            child: Icon(
                              Icons.account_circle_rounded,
                              color: Theme.of(context).colorScheme.primary,
                              size: Get.width < Constants.defaultBreakPoint
                                  ? Constants.iconSize * 0.8
                                  : Constants.iconSize,
                            ),
                          ),
                          SizedBox(width: Constants.defaultPadding / 2),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Not Selected",
                                  style: TextStyle(
                                    fontSize:
                                        Get.width < Constants.defaultBreakPoint
                                            ? Constants.sText
                                            : Constants.mText,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                  "012*****",
                                  style: TextStyle(
                                    fontSize:
                                        Get.width < Constants.defaultBreakPoint
                                            ? Constants.sText
                                            : Constants.mText,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // total
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Center(
                        child: Text(
                          '99.0',
                          style: TextStyle(
                            fontSize: Get.width < Constants.defaultBreakPoint
                                ? Constants.sText
                                : Constants.mText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),

                  // cash
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Center(
                        child: Text(
                          '6.50',
                          style: TextStyle(
                            fontSize: Get.width < Constants.defaultBreakPoint
                                ? Constants.sText
                                : Constants.mText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),

                  // card
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: Constants.defaultPadding / 2,
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Center(
                        child: Text(
                          '6.50',
                          style: TextStyle(
                            fontSize: Get.width < Constants.defaultBreakPoint
                                ? Constants.sText
                                : Constants.mText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),

                  // payment
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: IconButton(
                        icon: Icon(
                          Icons.payment,
                          color: Theme.of(context).colorScheme.primary,
                          size: Get.width < Constants.defaultBreakPoint
                              ? Constants.iconSize * 0.8
                              : Constants.iconSize,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),

                  // edit
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: Theme.of(context).colorScheme.primary,
                          size: Get.width < Constants.defaultBreakPoint
                              ? Constants.iconSize * 0.8
                              : Constants.iconSize,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),

                  // details
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: IconButton(
                        icon: Icon(
                          Icons.track_changes,
                          color: Theme.of(context).colorScheme.primary,
                          size: Get.width < Constants.defaultBreakPoint
                              ? Constants.iconSize * 0.8
                              : Constants.iconSize,
                        ),
                        onPressed: () {Get.toNamed(Routes.ORDER_DETAILS);},
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),

        // this buttons only for online orders
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: statusButtonList.map((e) => Container(
            width: 100,
            margin: EdgeInsets.symmetric(
              horizontal: Constants.defaultPadding / 4,
            ),
            child: RoundedButton(
              padding: 0,
              margin: 0,
              radius: 0,
              text: e.toUpperCase(),
              color: Colors.red,
              // color: Theme.of(context).colorScheme.primaryVariant,
              press: () {
              },
            ),
          )).toList(),
        )
      ],
    );
  }
}

// dataTableItem(context, element) {
//   bool isSelected = false;
//   return TableRow(
//     decoration: BoxDecoration(
//         border: Border(
//           bottom: BorderSide(
//               color: Theme.of(context).colorScheme.secondaryVariant, width: 1),
//         )),
//     children: [
//       // local
//       Center(
//         child: Text(
//           '1',
//           style: TextStyle(
//             fontSize: Constants.mText,
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//
//       // terminal
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Text(
//               "Collection".toUpperCase(),
//               style: TextStyle(
//                   fontSize: Constants.sText, fontWeight: FontWeight.bold),
//             ),
//             SizedBox(
//               height: Constants.defaultPadding / 2,
//             ),
//             Text(
//               "EPOS",
//               style: TextStyle(fontSize: Constants.mText),
//             )
//           ],
//         ),
//       ),
//
//       // order date
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisSize: MainAxisSize.min,
//           children: [
//             Text(
//               "Today 05:06 PM",
//               style: TextStyle(
//                   fontSize: Constants.mText, fontWeight: FontWeight.bold),
//             ),
//             SizedBox(
//               height: Constants.defaultPadding / 2,
//             ),
//             Text(
//               "ASAP",
//               style: TextStyle(fontSize: Constants.mText),
//             ),
//           ],
//         ),
//       ),
//
//       // status
//       Container(
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Icon(
//               Icons.star_rate_outlined,
//               color: Theme.of(context).colorScheme.primary,
//               size: Constants.iconSize,
//             ),
//             Text(
//               "New".toUpperCase(),
//               style: TextStyle(fontSize: Constants.mText),
//             ),
//           ],
//         ),
//       ),
//
//       // customer
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: ListTile(
//           contentPadding: EdgeInsets.zero,
//           horizontalTitleGap: 0,
//           leading: Icon(
//             Icons.account_circle_rounded,
//             color: Theme.of(context).colorScheme.primary,
//             size: Constants.iconSize,
//           ),
//           title: Text(
//             "Not Selected",
//             style: TextStyle(
//               fontSize: Constants.mText,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//           subtitle: Text(
//             "012*****",
//             style: TextStyle(fontSize: Constants.sText),
//           ),
//         ),
//       ),
//
//       // total
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: Center(
//           child: Text(
//             '6.50',
//             style: TextStyle(
//               fontSize: Constants.mText,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ),
//
//       // cash
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: Center(
//           child: Text(
//             '6.50',
//             style: TextStyle(
//               fontSize: Constants.mText,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ),
//
//       // card
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: Center(
//           child: Text(
//             '6.50',
//             style: TextStyle(
//               fontSize: Constants.mText,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ),
//
//       // payment
//       Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.payment,
//             color: Theme.of(context).colorScheme.primary,
//             size: Constants.iconSize,
//           ),
//           onPressed: () {},
//         ),
//       ),
//
//       // edit
//       Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.edit,
//             color: Theme.of(context).colorScheme.primary,
//             size: Constants.iconSize,
//           ),
//           onPressed: () {},
//         ),
//       ),
//
//       // details
//       Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.track_changes,
//             color: Theme.of(context).colorScheme.primary,
//             size: Constants.iconSize,
//           ),
//           onPressed: () {},
//         ),
//       ),
//
//       // user
//       Container(
//         padding: EdgeInsets.symmetric(
//           vertical: Constants.defaultPadding / 2,
//           horizontal: Constants.defaultPadding / 2,
//         ),
//         child: Center(
//           child: Text(
//             'N/A',
//             style: TextStyle(fontSize: Constants.mText),
//           ),
//         ),
//       ),
//     ],
//   );
// }
