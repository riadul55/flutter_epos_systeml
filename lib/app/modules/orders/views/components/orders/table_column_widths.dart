import 'package:flutter/material.dart';

Map<int, TableColumnWidth> getDataTableColumnWidth() => <int, TableColumnWidth>{
      0: IntrinsicColumnWidth(),
      1: IntrinsicColumnWidth(),
      2: FlexColumnWidth(),
      3: IntrinsicColumnWidth(),
      4: IntrinsicColumnWidth(),
      5: IntrinsicColumnWidth(),
      6: IntrinsicColumnWidth(),
      7: IntrinsicColumnWidth(),
      8: IntrinsicColumnWidth(),
      9: IntrinsicColumnWidth(),
      10: IntrinsicColumnWidth(),
      11: IntrinsicColumnWidth(),
    };
