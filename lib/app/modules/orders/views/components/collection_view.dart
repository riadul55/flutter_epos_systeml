import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class CollectionView extends StatelessWidget {
  final int flex;
  final String title;
  final bool isPayNowView;

  CollectionView({
    Key? key,
    required this.flex,
    required this.title,
    this.isPayNowView = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: Column(
            children: [
              header(context),
              divider(context),
            ],
          ),
        ),
        body: Stack(
          children: [
            isPayNowView ? Align(
              alignment: Alignment.center,
              child: Container(
                height: 150,
                width: 150,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      width: 10, color: Colors.green.withOpacity(0.2)),
                ),
                child: Center(
                  child: Transform.rotate(
                    angle: - math.pi / 4,
                    child: Text("Paid".toUpperCase(), style: TextStyle(
                      color: Colors.green.withOpacity(0.4),
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),),
                  ),
                ),
              ),
            ) : Container(),
            Container(
              child: ListView(
                children: List.generate(8, (index) => orderLIstItem(context)),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Constants.defaultPadding / 2,
            vertical: Constants.defaultPadding / 2,
          ),
          child: Wrap(
            children: [
              bottomItems(
                context: context,
                title: "Discount",
                fontSize: Constants.sText,
              ),
              bottomItems(
                context: context,
                title: "Manual Discount",
                value: "-0.0",
                fontSize: Constants.mText,
                titleColor: Colors.green,
                valueColor: Colors.green,
              ),
              divider(context,
                  color: Theme.of(context).colorScheme.primaryVariant),
              bottomItems(
                context: context,
                title: "Sub Total".toUpperCase(),
                value: "46.0",
                fontSize: Constants.mText,
              ),
              bottomItems(
                context: context,
                title: "Delivery Charge".toUpperCase(),
                value: "0.0",
                fontSize: Constants.mText,
              ),
              bottomItems(
                context: context,
                title: "Credit".toUpperCase(),
                value: "0.0",
                fontSize: Constants.mText,
                icon: Icons.edit,
              ),
              bottomItems(
                context: context,
                title: "Credit Note".toUpperCase(),
                fontSize: Constants.mText,
                icon: Icons.edit,
              ),
              bottomItems(
                context: context,
                title: "Tips".toUpperCase(),
                value: "0.0",
                fontSize: Constants.mText,
              ),
              bottomItems(
                context: context,
                title: "Total".toUpperCase(),
                value: "0.0",
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ],
          ),
        ),
      ),
    );
  }

  header(BuildContext context) => Padding(
        padding: EdgeInsets.all(Constants.defaultPadding / 2),
        child: Row(
          children: [
            Text(
              "Order: #2",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold,
              ),
            ),
            Expanded(
                child: Center(
              child: Text(
                "Collection".toUpperCase(),
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: Constants.sText,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )),
            Row(
              children: [
                Icon(
                  Icons.star_border,
                  size: Constants.iconSize * 0.8,
                  color: Theme.of(context).colorScheme.primary,
                ),
                Text(
                  title.toUpperCase(),
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      );

  orderLIstItem(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(
          horizontal: Constants.defaultPadding / 2,
          vertical: Constants.defaultPadding / 4,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).colorScheme.primaryVariant,
              width: 1,
            ),
          ),
        ),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(right: Constants.defaultPadding / 4),
              child: Text(
                "x1",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: Constants.sText,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              ),
            ),
            Expanded(
                child: Row(
              children: [
                Icon(
                  Icons.bookmark_border,
                  size: Constants.iconSize * 0.7,
                  color: Theme.of(context).colorScheme.primary,
                ),
                Text(
                  "Aloo Gosth",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                  ),
                ),
              ],
            )),
            Padding(
              padding: EdgeInsets.only(left: Constants.defaultPadding / 4),
              child: Text(
                "0.0",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: Constants.sText,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              ),
            ),
          ],
        ),
      );

  bottomItems({
    required BuildContext context,
    required String title,
    String? value,
    Color? titleColor,
    Color? valueColor,
    FontWeight? fontWeight,
    double? fontSize,
    IconData? icon,
    Function()? onTap,
  }) =>
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Text(
                title,
                style: TextStyle(
                  color:
                      titleColor ?? Theme.of(context).colorScheme.onBackground,
                  fontWeight: fontWeight ?? FontWeight.normal,
                  fontSize: fontSize ?? Constants.sText,
                ),
              ),
              icon != null
                  ? InkWell(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: Constants.defaultPadding / 2,
                        ),
                        child: Icon(
                          icon,
                          size: Constants.iconSize * 0.8,
                          color: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                    )
                  : SizedBox.shrink(),
            ],
          ),
          value != null
              ? Text(
                  value,
                  style: TextStyle(
                    color: valueColor ??
                        Theme.of(context).colorScheme.onBackground,
                    fontWeight: fontWeight ?? FontWeight.normal,
                    fontSize: fontSize ?? Constants.sText,
                  ),
                )
              : SizedBox.shrink(),
        ],
      );
}
