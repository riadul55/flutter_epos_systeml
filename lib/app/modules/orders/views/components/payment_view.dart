import 'package:epos_system/app/modules/globals/custom_input_grid.dart';
import 'package:epos_system/app/core/utilities/helpers.dart';
import 'package:epos_system/app/modules/orders/controllers/checkout_controller.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';

class PaymentView extends StatelessWidget {
  PaymentView({Key? key}) : super(key: key);

  CheckoutController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    // var height = (Get.height - Constants.preferredSize - 255) / 4.2;
    return Expanded(
      flex: 5,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              color: Theme.of(context).colorScheme.secondaryVariant,
              width: 1,
            ),
            left: BorderSide(
              color: Theme.of(context).colorScheme.secondaryVariant,
              width: 1,
            ),
          ),
        ),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(Constants.defaultPadding / 2),
              child: Row(
                children: [
                  Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Subtotal",
                            style: TextStyle(
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                          RoundedInputField(
                            paddingVertical: 0,
                            marginVertical: 0,
                            contentPadding: 0,
                            hintText: "0.0",
                            isReadOnly: true,
                          ),
                          SizedBox(height: Constants.defaultPadding / 2),
                          Text(
                            "Total",
                            style: TextStyle(
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                          RoundedInputField(
                            paddingVertical: 0,
                            marginVertical: 0,
                            contentPadding: 0,
                            hintText: "0.0",
                            isReadOnly: true,
                          ),
                          SizedBox(height: Constants.defaultPadding / 2),
                          Text(
                            "Amount Due",
                            style: TextStyle(
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                          RoundedInputField(
                            controller: controller.dueInputController,
                            paddingVertical: 0,
                            marginVertical: 0,
                            contentPadding: 0,
                            hintText: "0.0",
                            isReadOnly: true,
                            onTap: () {
                              controller.isReceivedInputActive(true);
                              controller.isTipsInputActive(false);
                              controller.isChangeInputActive(false);
                              controller.receivedInputController.text =
                              "10.0"; // controller.dueInputController.text
                            },
                          ),
                        ],
                      )),
                  SizedBox(width: Constants.defaultPadding / 2),
                  Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Received",
                            style: TextStyle(
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                          Obx(() => RoundedInputField(
                            controller: controller.receivedInputController,
                            paddingVertical: 0,
                            marginVertical: 0,
                            contentPadding: 0,
                            hintText: "0.0",
                            isReadOnly: true,
                            borderColor: controller.isReceivedInputActive.value
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryVariant,
                            onTap: () {
                              FocusScope.of(context).unfocus();
                              controller.isReceivedInputActive(true);
                              controller.isTipsInputActive(false);
                              controller.isChangeInputActive(false);
                            },
                          )),
                          SizedBox(height: Constants.defaultPadding / 2),
                          Text(
                            "Tips",
                            style: TextStyle(
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                          Obx(() => RoundedInputField(
                            controller: controller.tipsInputController,
                            paddingVertical: 0,
                            marginVertical: 0,
                            contentPadding: 0,
                            hintText: "0.0",
                            isReadOnly: true,
                            borderColor: controller.isTipsInputActive.value
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryVariant,
                            onTap: () {
                              controller.isReceivedInputActive(false);
                              controller.isTipsInputActive(true);
                              controller.isChangeInputActive(false);
                            },
                          )),
                          SizedBox(height: Constants.defaultPadding / 2),
                          Text(
                            "Change given",
                            style: TextStyle(
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                          Obx(() => RoundedInputField(
                            controller: controller.changeInputController,
                            paddingVertical: 0,
                            marginVertical: 0,
                            contentPadding: 0,
                            hintText: "0.0",
                            isReadOnly: true,
                            borderColor: controller.isChangeInputActive.value
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryVariant,
                            onTap: () {
                              controller.isReceivedInputActive(false);
                              controller.isTipsInputActive(false);
                              controller.isChangeInputActive(true);
                            },
                          )),
                        ],
                      )),
                ],
              ),
            ),
            CustomInputGrid(onTap: (e) {
              if (e == "<") {
                if (controller.isReceivedInputActive.value) {
                  removeLastCharacter(
                      controller.receivedInputController);
                } else if (controller.isTipsInputActive.value) {
                  removeLastCharacter(controller.tipsInputController);
                } else if (controller.isChangeInputActive.value) {
                  removeLastCharacter(controller.changeInputController);
                }
              } else {
                if (controller.isReceivedInputActive.value) {
                  controller.receivedInputController.text += e;
                } else if (controller.isTipsInputActive.value) {
                  controller.tipsInputController.text += e;
                } else if (controller.isChangeInputActive.value) {
                  var text = controller.changeInputController.text += e;
                  controller.changeInputController.text =
                  "-${text.replaceAll("-", "")}";
                }
              }
            }),
          ],
        ),
      ),
    );
  }
}
