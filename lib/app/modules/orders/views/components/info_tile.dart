import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class InfoTile extends StatelessWidget {
  final IconData icon;
  final String title;
  final String subTitle;
  final String? subValue;
  final Function() press;

  InfoTile({
    Key? key,
    required this.icon,
    required this.title,
    required this.subTitle,
    this.subValue,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: Constants.defaultPadding / 4,
          horizontal: Constants.defaultPadding / 2,
        ),
        child: Container(
          color: Colors.transparent,
          child: Row(
            children: [
              Icon(
                icon,
                size: Constants.iconSize,
                color: Theme.of(context).colorScheme.onBackground,
              ),
              SizedBox(width: Constants.defaultPadding / 2),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(title,
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.onBackground,
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                      )),
                  Row(
                    children: [
                      Text(
                        subTitle.toUpperCase(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.onBackground,
                          fontSize: Constants.sText,
                        ),
                      ),
                      Text(
                        subValue ?? "",
                        style: TextStyle(
                          color: Colors.green,
                          fontSize: Constants.sText,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}
