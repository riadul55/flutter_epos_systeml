import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class OrderStatusView extends StatelessWidget {
  OrderStatusView({Key? key}) : super(key: key);

  var statusList = ["Review", "Processing", "Ready"];

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        decoration: leftRightBorderBox(context),
        child: Padding(
          padding: EdgeInsets.all(Constants.defaultPadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("Order was Successfully Paid", style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),),
              SizedBox(height: Constants.defaultPadding / 2),
              Text("Total 25.00".toUpperCase(), style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),),
              SizedBox(height: Constants.defaultPadding / 2),
              Text("27/12/2021 01:13 PM", style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.normal,
              ),),
              SizedBox(height: Constants.defaultPadding * 2),
              Container(
                child: MasonryGridView.count(
                    crossAxisCount: 2,
                    itemCount: statusList.length,
                    crossAxisSpacing: Constants.defaultPadding / 2,
                    mainAxisSpacing: Constants.defaultPadding / 2,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Material(
                        color: index == 1
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.primaryVariant,
                        child: InkWell(
                          onTap: () {},
                          child: Container(
                            height: 100,
                            child: Center(
                              child: Text(
                                statusList[index].toUpperCase(),
                                style: TextStyle(
                                  fontSize: Constants.xlText,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).colorScheme.onPrimary,
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
              ),

              RoundedButton(
                text: "Closed",
                color: Colors.red.shade900,
                radius: 0,
                textSize: Constants.xlText,
                width: 200,
                press: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
