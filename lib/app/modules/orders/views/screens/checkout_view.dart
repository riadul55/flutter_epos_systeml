
import 'package:epos_system/app/modules/orders/views/components/payment_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../controllers/checkout_controller.dart';
import '../components/checkout_bottom_nav.dart';
import '../components/collection_view.dart';
import '../components/payment_info.dart';

class CheckoutView extends GetView<CheckoutController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              CollectionView(
                flex: 3,
                title: "New",
              ),
              PaymentView(),
              PaymentInfo(orderType: "Collection",),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CheckoutBottomNav(),
    );
  }
}
