
import 'package:epos_system/app/modules/orders/views/components/collection_view.dart';
import 'package:epos_system/app/modules/orders/views/components/customer_info.dart';
import 'package:epos_system/app/modules/orders/views/components/order_details_bottom.dart';
import 'package:epos_system/app/modules/orders/views/components/order_status_view.dart';
import 'package:flutter/material.dart';

import '../../controllers/order_details_controller.dart';
import 'package:get/get.dart';

class OrderDetailsView extends GetView<OrderDetailsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              CollectionView(
                flex: 3,
                title: "Review",
                isPayNowView: true,
              ),
              OrderStatusView(),
              CustomerInfo(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: OrderDetailsBottom(),
    );
  }
}