import 'package:epos_system/app/modules/home/controllers/orders_controller.dart';
import 'package:epos_system/app/modules/orders/views/components/orders/data_table_header.dart';
import 'package:epos_system/app/modules/orders/views/components/orders/item_data_table.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrdersScreen extends GetView {
  OrdersScreen({Key? key}) : super(key: key);

  @override
  OrdersController get controller => Get.put(OrdersController());

  @override
  Widget build(BuildContext context) {
    // FocusScope.of(context).requestFocus(FocusNode());
    return SafeArea(
      child: Obx(() {
        print(controller.homeController.appBarButtonSelectedIndex);
        return Padding(
          padding: EdgeInsets.all(Constants.defaultPadding / 2),
          child: GetBuilder(
            init: controller,
            builder: (_) {
              return Column(
                children: [
                  DataTableHeader(),
                  Expanded(
                      child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: 4,
                    itemBuilder: (context, index) {
                      bool isSelected =
                          controller.selectedIndexList.value.contains(index);
                      return ItemDataTable(
                        isSelected: isSelected,
                        onTap: () {
                          if (isSelected) {
                            controller.selectedIndexList.value.remove(index);
                          } else {
                            controller.selectedIndexList.value.add(index);
                          }
                          controller.update();
                        },
                      );
                    },
                  )),
                ],
              );
            },
          ),
        );
      }),
    );
  }
}
