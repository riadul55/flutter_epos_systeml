import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CheckoutController extends GetxController {

  var subtotalInputController = TextEditingController();
  var totalInputController = TextEditingController();
  var dueInputController = TextEditingController();
  var receivedInputController = TextEditingController();
  var tipsInputController = TextEditingController();
  var changeInputController = TextEditingController();

  var isReceivedInputActive = false.obs;
  var isTipsInputActive = false.obs;
  var isChangeInputActive = false.obs;

  var paymentMethods = ["Cash", "Card"];
  var selectedPayMethod = 0.obs;

  var selectedTime = TimeOfDay.now().obs;
  var selectedDate = DateTime.now().obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
