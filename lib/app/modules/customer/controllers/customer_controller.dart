import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:get/get.dart';

import '../../../data/local_db/app_db.dart';
import '../../../data/local_db/data_modules/bindings/local.customer.repository.binding.dart';

class CustomerController extends GetxController {
  late LocalCustomerRepositoryBinding repositoryBinding;
  var customer=<Customer>[].obs;
  final count = 0.obs;
  @override
  void onInit() {
    repositoryBinding=LocalCustomerRepositoryBinding();
    super.onInit();
    getCustomerList();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getCustomerList()async {
    customer.value= await repositoryBinding.repository.getAllCustomer();

      logSuccess("Total Customer ====> ${customer.length}");
  }
}
