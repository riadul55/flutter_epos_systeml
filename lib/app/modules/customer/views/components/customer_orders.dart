import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:flutter/material.dart';

class CustomerOrders extends StatelessWidget {
  const CustomerOrders({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Column(
        children: [
          HeaderTitle(title: "Customer Orders"),
        ],
      ),
    );
  }
}
