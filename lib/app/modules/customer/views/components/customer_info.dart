import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_dropdown_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'customer_info_input.dart';

class CustomerInfo extends StatelessWidget {
  CustomerInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        decoration: leftRightBorderBox(context),
        child: Column(
          children: [
            HeaderTitle(title: "Customer Information"),
            ListView(
              shrinkWrap: true,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding / 2,
                    vertical: Constants.defaultPadding / 4,
                  ),
                  child: Row(
                    children: [
                      CustomerInfoInput(title: "Phone Number"),
                      SizedBox(width: Constants.defaultPadding),
                      CustomerInfoInput(title: "Email Address"),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding / 2,
                    vertical: Constants.defaultPadding / 4,
                  ),
                  child: Row(
                    children: [
                      CustomerInfoInput(title: "Post Code", onTap: () {}),
                      SizedBox(width: Constants.defaultPadding),
                      Expanded(
                          child: RoundedDropdownField(
                        hintText: "select role",
                        value: "Address",
                        list: ["Address", "Address 1"],
                        onChanged: (val) {},
                      )),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding / 2,
                    vertical: Constants.defaultPadding / 4,
                  ),
                  child: Row(
                    children: [
                      CustomerInfoInput(title: "House No"),
                      SizedBox(width: Constants.defaultPadding),
                      CustomerInfoInput(title: "Street Name"),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding / 2,
                    vertical: Constants.defaultPadding / 4,
                  ),
                  child: Row(
                    children: [
                      CustomerInfoInput(title: "First Name"),
                      SizedBox(width: Constants.defaultPadding),
                      CustomerInfoInput(title: "Last Name"),
                    ],
                  ),
                ),
                // buttons
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding / 2,
                    vertical: Constants.defaultPadding / 4,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: RoundedButton(
                          press: () {},
                          text: "Save".toUpperCase(),
                          radius: 0,
                          padding: 0,
                        ),
                      ),
                      SizedBox(width: Constants.defaultPadding),
                      Expanded(
                        child: RoundedButton(
                          press: () {},
                          text: "Select".toUpperCase(),
                          radius: 0,
                          padding: 0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
