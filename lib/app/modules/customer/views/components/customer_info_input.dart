import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class CustomerInfoInput extends StatelessWidget {
  final String title;
  final Function()? onTap;

  CustomerInfoInput({
    Key? key,
    required this.title,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TextField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(
            horizontal: Constants.defaultPadding / 2,
          ),
          labelText: title,
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.secondaryVariant,
              width: 1,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.secondaryVariant,
              width: 1,
            ),
          ),
          suffixIcon: onTap != null
              ? IconButton(
                  onPressed: onTap,
                  icon: Icon(
                    Icons.search,
                    color: Theme.of(context).colorScheme.primary,
                    size: Constants.iconSize,
                  ),
                )
              : null,
        ),
      ),
    );
  }
}
