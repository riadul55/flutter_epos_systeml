import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../data/local_db/app_db.dart';
import '../../controllers/customer_controller.dart';

class CustomerList extends StatelessWidget {
  CustomerList({Key? key}) : super(key: key);

  CustomerController get controller => Get.find();
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Column(
        children: [
          HeaderTitle(title: "Select a customer", borderWidth: 0),
          _searchCustomer(context),
          Expanded(
            child: Obx(()=> ListView(
                shrinkWrap: true,
                children:
                  controller.customer.value.map((list) =>  CustomerListItem(customer: list),).toList(),

              ),
            ),
          ),
        ],
      ),
    );
  }


  _searchCustomer(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Theme.of(context).colorScheme.primaryVariant,
            width: 1,
          ),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: RoundedInputField(
              hintText: "Customer Name/Phone",
              marginVertical: 0,
              paddingVertical: 0,
              width: 0,
              borderColor: Colors.transparent,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Constants.defaultPadding / 2,
            ),
            child: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.add,
                color: Theme.of(context).colorScheme.primary,
                size: Constants.iconSize,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomerListItem extends StatelessWidget {
  final Customer customer;
  const CustomerListItem({Key? key, required this.customer}) : super(key: key);

  CustomerController get controller => Get.find();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){},
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).colorScheme.primaryVariant,
              width: 1,
            ),
          ),
        ),
        child: ListTile(
          horizontalTitleGap: 0,
          dense: true,
          leading: Icon(
            Icons.account_circle,
            size: Constants.iconSize,
            color: Theme.of(context).colorScheme.primary,
          ),
          title: Text(
            "${customer.firstName!} ${customer.lastName!}",
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.mText,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            customer.phone!,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.sText,
            ),
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.edit,
                    color: Theme.of(context).colorScheme.primary,
                  )),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.check,
                    color: Colors.green.shade900,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
