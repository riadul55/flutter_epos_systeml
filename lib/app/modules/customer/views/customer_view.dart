import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/customer_controller.dart';
import 'components/customer_bottom_nav.dart';
import 'components/customer_info.dart';
import 'components/customer_list.dart';
import 'components/customer_orders.dart';

class CustomerView extends GetView<CustomerController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              CustomerList(),
              CustomerInfo(),
              CustomerOrders(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CustomerBottomNav(),
    );
  }
}
