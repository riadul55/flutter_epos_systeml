import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';

import '../../../core/utilities/my_storage.dart';
import '../../../data/remote_db/bindings/customer.repository.binding.dart';
import '../../../data/remote_db/bindings/global.parameter.repository.binding.dart';

class BusinessSettingController extends GetxController {
  var storage = Get.find<MyStorage>();

  late GlobalParameterRepositoryBinding globalRepository;

  ///Business Setting all value
  ///--------------------------------
  var isBusinessNameVisible = false.obs;
  var isBusinessAddressVisible = false.obs;
  var isBusinessPhoneVisible = false.obs;
  var isBusinessEmailVisible = false.obs;
  var isBusinessVatVisible = false.obs;
  var isBusinessStatusVisible = false.obs;
  var isBusinessOpeningTime = false.obs;
  var businessNameController = TextEditingController();
  var businessAddressController = TextEditingController();
  var businessPostCodeController = TextEditingController();
  var businessPhoneController = TextEditingController();
  var businessEmailController = TextEditingController();
  var businessVatController = TextEditingController();
  var businessName = "".obs;
  var businessAddress = "".obs;
  var businessPostCode = "".obs;
  var businessPhone = "".obs;
  var businessEmail = "".obs;
  var businessVat = "".obs;
  var status = "".obs;
  var businessOpeningTime = TimeOfDay.now().obs;
  var selectedTime = "".obs;
  var businessStatus = false.obs;

  ///--------------------------------

  @override
  void onInit() {
    globalRepository = GlobalParameterRepositoryBinding();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<void> getGlobalParameterData() async {
    getBusinessStatus();
    businessName.value = storage.businessName.val;
    businessAddress.value = storage.businessAddress.val;
    businessPostCode.value = storage.businessPostCode.val;
    businessPhone.value = storage.businessPhone.val;
    businessEmail.value = storage.businessEmail.val;
    businessVat.value = storage.businessVat.val;
    selectedTime.value=storage.businessOpeningTime.val;

    businessNameController.text = storage.businessName.val;
    businessAddressController.text = storage.businessAddress.val;
    businessPostCodeController.text = storage.businessPostCode.val;
    businessPhoneController.text = storage.businessPhone.val;
    businessEmailController.text = storage.businessEmail.val;
    businessVatController.text = storage.businessVat.val;
    // status.value = storage.businessStatus.val;
    // if(status.value == "open"){
    //   businessStatus.value=true;
    // }else if(status.value == "close"){
    //   businessStatus.value=false;
    // }
  }

  Future<void> getBusinessStatus() async {
    await globalRepository.repository.getGlobalBusinessStatus().then((value) {
      if (value.globalParameter != null) {
        if (value.globalParameter!.businessStatus != null) {
          status.value = value.globalParameter!.businessStatus!;
          storage.businessStatus.val = status.value;
          if (status.value == "open") {
            businessStatus.value = true;
          } else if (status.value == "close") {
            businessStatus.value = false;
          }
        }
      } else {
        if (storage.businessStatus.val != "") {
          status.value = storage.businessStatus.val;
          if (status.value == "open") {
            businessStatus.value = true;
          } else if (status.value == "close") {
            businessStatus.value = false;
          }
        } else {
          status.value = "N/A";
          businessStatus.value = false;
        }
      }
    });
    status.value = status.value.substring(0, 1).toUpperCase() +
        status.value.substring(1).toLowerCase();
  }

  void saveBusinessName(BuildContext context) {
    Map map = {"business_name": businessNameController.text};
    storage.businessName.val = businessName.value = businessNameController.text;
    updatingBusinessData(context, map);
  }

  void saveBusinessAddress(BuildContext context) {
    Map map = {
      "business_address": businessAddressController.text,
      "business_postcode": businessPostCodeController.text,
    };
    storage.businessAddress.val =
        businessAddress.value = businessAddressController.text;
    storage.businessPostCode.val =
        businessPostCode.value = businessPostCodeController.text;
    updatingBusinessData(context, map);
  }

  void saveBusinessPhone(BuildContext context) {
    Map map = {"business_phone": businessPhoneController.text};
    storage.businessPhone.val =
        businessPhone.value = businessPhoneController.text;
    updatingBusinessData(context, map);
  }

  void saveBusinessEmail(BuildContext context) {
    Map map = {"business_email": businessEmailController.text};
    storage.businessEmail.val =
        businessEmail.value = businessEmailController.text;
    updatingBusinessData(context, map);
  }

  void saveBusinessVat(BuildContext context) {
    Map map = {"vat": businessVatController.text};
    storage.businessVat.val = businessVat.value = businessVatController.text;
    updatingBusinessData(context, map);
  }
  void saveBusinessOpening(BuildContext context) {
    Map map = {"opening_hour": selectedTime.value};
    storage.businessOpeningTime.val = selectedTime.value;
    updatingBusinessData(context, map);
  }

  void saveBusinessStatus(BuildContext context) {
    //Todo: need to update value to the server
    if (businessStatus.value) {
      storage.businessStatus.val = "open";
      status.value = "Open";
    } else {
      storage.businessStatus.val = "close";
      status.value = "Close";
    }
  }

  Future<void> updatingBusinessData(BuildContext context, Map body) async {
    ProgressDialog pd = ProgressDialog(context: context);
    pd.show(
      max: 100,
      hideValue: true,
      msg: 'Updating data...',
      progressBgColor: Colors.transparent,
    );
    await globalRepository.repository.updateGlobalParameter(body).then((value) {
      pd.close();
      Get.defaultDialog(
          title: "Success",
          content: Text("Data Update Successfully"),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ]);
    });
  }
}
