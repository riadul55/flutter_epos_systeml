import 'package:get/get.dart';

class PrinterSettingController extends GetxController {

  var isFontSizeEdit = false.obs;
  var isDeliveryNumEdit = false.obs;
  var isCollectionNumEdit = false.obs;
  var isTableOrderNumEdit = false.obs;
  var isTakeawayNumEdit = false.obs;
  var isPrintStyleEdit = false.obs;
  var isPrintEnableEdit = false.obs;
  var isOrderNoInvoiceEdit = false.obs;
}