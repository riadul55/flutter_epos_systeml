import 'package:epos_system/app/data/remote_db/bindings/customer.repository.binding.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';
import '../../../core/utilities/my_storage.dart';
import '../../../core/utilities/storage_key.dart';
import '../../../data/remote_db/bindings/global.parameter.repository.binding.dart';
import '../views/screens/business_setting.dart';
import '../views/screens/customer_setting.dart';
import '../views/screens/epos_function.dart';
import '../views/screens/history_setting.dart';
import '../views/screens/printer_setting.dart';
import 'business_setting_controller.dart';
import 'customer_setting_controller.dart';
import 'epos_setting_controller.dart';
import 'history_setting_controller.dart';
import 'printer_setting_controller.dart';

class SettingController extends GetxController {
  var storage = Get.put(MyStorage());

  var businessSettingController= Get.put(BusinessSettingController());
  var customerSettingController= Get.put(CustomerSettingController());
  var printerSettingController= Get.put(PrinterSettingController());
  var historySettingController= Get.put(HistorySettingController());
  var eposSettingController= Get.put(EPOSSettingController());

  late PageController pageController;
  var selectedScreenIndex = 0.obs;
  var screenList = [
    BusinessSetting(),
    HistorySetting(),
    PrinterSetting(),
    EposFunction(),
    CustomerSetting(),
  ];

  late CustomerRepositoryBinding customerRepository;
  late GlobalParameterRepositoryBinding globalRepository;

  @override
  void onInit() {
    pageController = PageController(initialPage: selectedScreenIndex.value);
    customerRepository=CustomerRepositoryBinding();
    globalRepository=GlobalParameterRepositoryBinding();
    super.onInit();

    ever(selectedScreenIndex, (callback) {
      pageController.jumpToPage(selectedScreenIndex.value);
    });
  }

  @override
  void onReady() {
    super.onReady();
  }





}
