import 'package:epos_system/app/data/remote_db/bindings/customer.repository.binding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';

class CustomerSettingController extends GetxController {


  late CustomerRepositoryBinding customerRepository;

  @override
  void onInit() {
    customerRepository=CustomerRepositoryBinding();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<void> importCustomer(BuildContext  context) async{
    ProgressDialog pd = ProgressDialog(context: context);
    pd.show(
      max: 100,
      hideValue: true,
      msg: 'Importing customer information...',
      progressBgColor: Colors.transparent,
    );
    await customerRepository.repository.getCustomerList().then((value){
      pd.close();
      Get.defaultDialog(title: "Import Success",
          content: Column(
            children: [
              Text("${value.data.customerInformation!.length} Success"),
              Text("0 Failed"),
            ],
          ),
          actions:[TextButton(onPressed: (){Get.back();}, child: Text("OK"))]);
    });
  }

  void exportCustomer(){
    Get.defaultDialog(title: "Export Success",
        content: Column(
          children: [
            Text("0 Success"),
            Text("0 Failed"),
          ],
        ),
        actions:[TextButton(onPressed: (){Get.back();}, child: Text("OK"))]);
  }
}