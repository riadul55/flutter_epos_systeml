import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/utilities/my_storage.dart';

class EPOSSettingController extends GetxController {
  var storage = Get.find<MyStorage>();

  ///EPOS Setting all value
  ///--------------------------------
  var isTakeOutVisible=false.obs;
  var isTakeOrderVisible=false.obs;
  var isWaiterAppVisible=false.obs;
  var isCollectionVisible=false.obs;
  var isDeliveryVisible=false.obs;


  var takeOutStatus="".obs;
  var takeOrderStatus="".obs;
  var waiterAppStatus="".obs;
  var collectionStatus="".obs;
  var deliveryStatus="".obs;

  var isTakeOutStatusChecked=false.obs;
  var isTakeOrderStatusChecked=false.obs;
  var isWaiterAppStatusChecked=false.obs;
  var isCollectionStatusChecked=false.obs;
  var isDeliveryStatusChecked=false.obs;



  ///--------------------------------


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
  Future<void>  getData() async{
    takeOutStatus.value=(storage.takeOutOrderStatus.val).toString();
    takeOutStatus.value =takeOutStatus.value.substring(0,1).toUpperCase() + takeOutStatus.value.substring(1).toLowerCase();
    if(storage.takeOutOrderStatus.val){
      isTakeOutStatusChecked.value=true;
    }else{
      isTakeOutStatusChecked.value=false;
    }

    takeOrderStatus.value=(storage.takeOrderStatus.val).toString();
    takeOrderStatus.value =takeOrderStatus.value.substring(0,1).toUpperCase() + takeOrderStatus.value.substring(1).toLowerCase();
    if(storage.takeOrderStatus.val){
      isTakeOrderStatusChecked.value=true;
    }else{
      isTakeOrderStatusChecked.value=false;
    }
    waiterAppStatus.value=(storage.waiterAppStatus.val).toString();
    waiterAppStatus.value =waiterAppStatus.value.substring(0,1).toUpperCase() + waiterAppStatus.value.substring(1).toLowerCase();
    if(storage.waiterAppStatus.val){
      isWaiterAppStatusChecked.value=true;
    }else{
      isWaiterAppStatusChecked.value=false;
    }

    collectionStatus.value=(storage.collectionOrderStatus.val).toString();
    collectionStatus.value =collectionStatus.value.substring(0,1).toUpperCase() + collectionStatus.value.substring(1).toLowerCase();
    if(storage.collectionOrderStatus.val){
      isCollectionStatusChecked.value=true;
    }else{
      isCollectionStatusChecked.value=false;
    }

    deliveryStatus.value=(storage.deliveryOrderStatus.val).toString();
    deliveryStatus.value =deliveryStatus.value.substring(0,1).toUpperCase() + deliveryStatus.value.substring(1).toLowerCase();
    if(storage.deliveryOrderStatus.val){
      isDeliveryStatusChecked.value=true;
    }else{
      isDeliveryStatusChecked.value=false;
    }
  }


  void saveTakeOutOrderStatus(){
    storage.takeOutOrderStatus.val = isTakeOutStatusChecked.value;
    showDialog("Success", "Data Saved Successfully");
    getData();
  }

  void saveTakeOrderStatus(){
    storage.takeOrderStatus.val = isTakeOrderStatusChecked.value;
    showDialog("Success", "Data Saved Successfully");
    getData();
  }

  void saveWaiterAppStatus(){
    storage.waiterAppStatus.val = isWaiterAppStatusChecked.value;
    showDialog("Success", "Data Saved Successfully");
    getData();
  }

  void saveCollectionOrderStatus(){
    storage.collectionOrderStatus.val = isCollectionStatusChecked.value;
    showDialog("Success", "Data Saved Successfully");
    getData();
  }

  void saveDeliveryOrderStatus(){
    storage.deliveryOrderStatus.val = isDeliveryStatusChecked.value;
    showDialog("Success", "Data Saved Successfully");
    getData();
  }

  void showDialog(String title, String body){
    Get.defaultDialog(title: title,
        content: Text(body),
        actions:[TextButton(onPressed: (){Get.back();}, child: Text("OK"))]);
  }
}