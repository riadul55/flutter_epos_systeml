import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/setting/controllers/setting_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'navigation_item.dart';

class SettingsNavigation extends StatelessWidget {
  SettingsNavigation({Key? key}) : super(key: key);

  SettingController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        alignment: Alignment.topLeft,
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderTitle(title: "Setting".toUpperCase()),
              NavigationItem(
                icon: Icons.settings,
                title: "Business Setting".toUpperCase(),
                onTap: () {
                  controller.selectedScreenIndex(0);
                },
              ),
              NavigationItem(
                icon: Icons.history_outlined,
                title: "History Setting".toUpperCase(),
                onTap: () {
                  controller.selectedScreenIndex(1);
                },
              ),
              NavigationItem(
                icon: Icons.print,
                title: "Printer Setting".toUpperCase(),
                onTap: () {
                  controller.selectedScreenIndex(2);
                },
              ),
              NavigationItem(
                icon: Icons.view_module,
                title: "Epos Function".toUpperCase(),
                onTap: () {
                  controller.selectedScreenIndex(3);
                },
              ),
              NavigationItem(
                icon: Icons.people,
                title: "Customer Setting".toUpperCase(),
                onTap: () {
                  controller.selectedScreenIndex(4);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
