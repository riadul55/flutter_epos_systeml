import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class CounterWidget extends StatefulWidget {
  const CounterWidget({Key? key}) : super(key: key);

  @override
  _CounterWidgetState createState() => _CounterWidgetState();
}

class _CounterWidgetState extends State<CounterWidget> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          IconButton(
            icon: Icon(
              Icons.remove,
              size: Constants.iconSize,
              color: Theme.of(context).colorScheme.primary,
            ),
            onPressed: () {
              setState(() {
                if (count > 0) {
                  count--;
                }
              });
            },
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: Constants.defaultPadding,
            ),
            child: Text(
              "$count",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.add,
              size: Constants.iconSize,
              color: Theme.of(context).colorScheme.primary,
            ),
            onPressed: () {
              setState(() {
                count++;
              });
            },
          ),
        ],
      ),
    );
  }
}
