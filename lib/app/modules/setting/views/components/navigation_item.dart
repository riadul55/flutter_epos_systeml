import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NavigationItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final Function()? onTap;

  NavigationItem({
    Key? key,
    required this.title,
    required this.icon,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: ListTile(
        tileColor: Theme.of(context).colorScheme.primary,
        horizontalTitleGap: 0,
        leading: Icon(
          icon,
          size: Constants.iconSize,
          color: Theme.of(context).colorScheme.onPrimary,
        ),
        title: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
