import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ItemSettings extends StatelessWidget {
  final String title;
  final String? value;
  final bool isVisible;
  final Widget visibleChild;
  final Function() onTapEdit;

  ItemSettings({
    Key? key,
    required this.title,
    this.value,
    this.isVisible = false,
    required this.visibleChild,
    required this.onTapEdit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: bottomBorderBox(context),
      child: Column(
        children: [
          ListTile(
            title: Text(
              title,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  value ?? "",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.mText,
                  ),
                ),
                SizedBox(width: Constants.defaultPadding),
                IconButton(
                  icon: Icon(
                    !isVisible ? Icons.keyboard_arrow_down: Icons.keyboard_arrow_up,
                    size: Constants.iconSize,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  onPressed: onTapEdit,
                ),
              ],
            ),
          ),
          Visibility(
            visible: isVisible,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: Constants.defaultPadding / 1.2,
              ),
              child: visibleChild,
            ),
          ),
        ],
      ),
    );
  }
}
