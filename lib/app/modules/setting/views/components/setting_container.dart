import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:flutter/material.dart';

class SettingContainer extends StatelessWidget {
  final String title;
  final Widget child;

  SettingContainer({
    Key? key,
    required this.title,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HeaderTitle(title: title),
          Expanded(
            child: Container(
              width: Constants.formWidth,
              padding: EdgeInsets.only(
                top: Constants.defaultPadding / 2,
                right: Constants.defaultPadding / 2,
                left: Constants.defaultPadding / 2,
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: Card(
                elevation: Constants.defaultElevation,
                child: child,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
