import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/setting_controller.dart';
import 'components/setting_bottom_nav.dart';
import 'components/settings_navigation.dart';

class SettingView extends GetView<SettingController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Row(
          children: [
            SettingsNavigation(),
            Expanded(
              flex: 8,
              child: PageView(
                controller: controller.pageController,
                physics: NeverScrollableScrollPhysics(),
                children: controller.screenList,
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: SettingBottomNav(),
    );
  }
}
