import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_dropdown_field.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/setting/controllers/printer_setting_controller.dart';
import 'package:epos_system/app/modules/setting/controllers/setting_controller.dart';
import 'package:epos_system/app/modules/setting/views/components/counter_widget.dart';
import 'package:epos_system/app/modules/setting/views/components/item_settings.dart';
import 'package:epos_system/app/modules/setting/views/components/setting_container.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PrinterSetting extends StatelessWidget {
  PrinterSetting({Key? key}) : super(key: key);

  // PrinterSettingController controller = Get.put(PrinterSettingController());
  PrinterSettingController  get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return SettingContainer(
      title: "Printer Setting",
      child: ListView(
        children: [
          Obx(() => ItemSettings(
            title: "Print Font Size",
            value: "16",
            isVisible: controller.isFontSizeEdit.value,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isFontSizeEdit(!controller.isFontSizeEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Number of Print on Delivery",
            value: "16",
            isVisible: controller.isDeliveryNumEdit.value,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isDeliveryNumEdit(!controller.isDeliveryNumEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Number of Print on Collection",
            value: "16",
            isVisible: controller.isCollectionNumEdit.value,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isCollectionNumEdit(!controller.isCollectionNumEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Number of Print on Table Order",
            value: "16",
            isVisible: controller.isTableOrderNumEdit.value,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isTableOrderNumEdit(!controller.isTableOrderNumEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Number of Print on Takeaway order",
            value: "16",
            isVisible: controller.isTakeawayNumEdit.value,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isTakeawayNumEdit(!controller.isTakeawayNumEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Select Print Style",
            value: "Style 1",
            isVisible: controller.isPrintStyleEdit.value,
            visibleChild: Row(
              children: [
                Container(
                  width: 200,
                  child: RoundedDropdownField(
                    hintText: "",
                    value: "",
                    list: [],
                    onChanged: (value) {},
                  ),
                ),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isPrintStyleEdit(!controller.isPrintStyleEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Auto-print Enable",
            value: "true",
            isVisible: controller.isPrintEnableEdit.value,
            visibleChild: Row(
              children: [
                Checkbox(value: true, onChanged: (value) {}),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isPrintEnableEdit(!controller.isPrintEnableEdit.value),
          )),
          Obx(() => ItemSettings(
            title: "Show order No. on Invoice",
            value: "true",
            isVisible: controller.isOrderNoInvoiceEdit.value,
            visibleChild: Row(
              children: [
                Checkbox(value: true, onChanged: (value) {}),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () => controller.isOrderNoInvoiceEdit(!controller.isOrderNoInvoiceEdit.value),
          )),
        ],
      ),
    );
  }
}
