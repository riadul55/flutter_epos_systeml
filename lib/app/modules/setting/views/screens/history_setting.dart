import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/setting/views/components/counter_widget.dart';
import 'package:epos_system/app/modules/setting/views/components/item_settings.dart';
import 'package:epos_system/app/modules/setting/views/components/setting_container.dart';
import 'package:flutter/material.dart';

class HistorySetting extends StatelessWidget {
  const HistorySetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SettingContainer(
      title: "History Setting",
      child: ListView(
        children: [
          ItemSettings(
            title: "Data Retainsion Period",
            value: "5",
            isVisible: true,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () {},
          ),
          ItemSettings(
            title: "Customer Order Data count",
            value: "5",
            isVisible: false,
            visibleChild: Row(
              children: [
                CounterWidget(),
                Spacer(),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: Constants.mText,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            onTapEdit: () {},
          ),
        ],
      ),
    );
  }
}
