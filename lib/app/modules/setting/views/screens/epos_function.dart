import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/setting/views/components/item_settings.dart';
import 'package:epos_system/app/modules/setting/views/components/setting_container.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controllers/epos_setting_controller.dart';

class EposFunction extends StatelessWidget {
  const EposFunction({Key? key}) : super(key: key);

  EPOSSettingController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: controller.getData(),
      builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
        return SettingContainer(
          title: "Epos Function",
          child: Obx(()=> ListView(
              children: [
                ItemSettings(
                  title: "Take out",
                  value: controller.takeOutStatus.value,
                  isVisible: controller.isTakeOutVisible.value,
                  visibleChild: Row(
                    children: [
                      Checkbox(
                          value: controller.isTakeOutStatusChecked.value,
                          onChanged: (value)
                          {
                            controller.isTakeOutStatusChecked.value = value!;
                          }),
                      Spacer(),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {controller.saveTakeOutOrderStatus();},
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isTakeOutVisible.value =
                        !controller.isTakeOutVisible.value;
                  },
                ),
                 ItemSettings(
                    title: "Take Order",
                    value: controller.takeOrderStatus.value,
                    isVisible: controller.isTakeOrderVisible.value,
                    visibleChild: Row(
                      children: [
                        Checkbox(
                            value: controller.isTakeOrderStatusChecked.value,
                            onChanged: (value)
                            {
                              controller.isTakeOrderStatusChecked.value = value!;
                            }),
                        Spacer(),
                        TextButton(
                          child: Text(
                            "Save",
                            style: TextStyle(
                              fontSize: Constants.mText,
                            ),
                          ),
                          onPressed: () {controller.saveTakeOrderStatus();},
                        ),
                      ],
                    ),
                    onTapEdit: () {
                      controller.isTakeOrderVisible.value =
                          !controller.isTakeOrderVisible.value;
                    },
                  ),

                ItemSettings(
                  title: "Waiter app",
                  value: controller.waiterAppStatus.value,
                  isVisible: controller.isWaiterAppVisible.value,
                  visibleChild: Row(
                    children: [
                      Checkbox(
                          value: controller.isWaiterAppStatusChecked.value,
                          onChanged: (value)
                          {
                            controller.isWaiterAppStatusChecked.value = value!;
                          }),
                      Spacer(),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {controller.saveWaiterAppStatus();},
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isWaiterAppVisible.value =
                        !controller.isWaiterAppVisible.value;
                  },
                ),
                ItemSettings(
                  title: "Collection",
                  value: controller.collectionStatus.value,
                  isVisible: controller.isCollectionVisible.value,
                  visibleChild: Row(
                    children: [
                      Checkbox(
                          value: controller.isCollectionStatusChecked.value,
                          onChanged: (value)
                          {
                            controller.isCollectionStatusChecked.value = value!;
                          }),
                      Spacer(),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {controller.saveCollectionOrderStatus();},
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isCollectionVisible.value =
                        !controller.isCollectionVisible.value;
                  },
                ),
                ItemSettings(
                  title: "Delivery",
                  value: controller.deliveryStatus.value,
                  isVisible: controller.isDeliveryVisible.value,
                  visibleChild: Row(
                    children: [
                      Checkbox(
                          value: controller.isDeliveryStatusChecked.value,
                          onChanged: (value)
                          {
                            controller.isDeliveryStatusChecked.value = value!;
                          }),
                      Spacer(),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {controller.saveDeliveryOrderStatus();},
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isDeliveryVisible.value =
                        !controller.isDeliveryVisible.value;
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
