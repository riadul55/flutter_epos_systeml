import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/setting/controllers/business_setting_controller.dart';
import 'package:epos_system/app/modules/setting/controllers/setting_controller.dart';
import 'package:epos_system/app/modules/setting/views/components/item_settings.dart';
import 'package:epos_system/app/modules/setting/views/components/setting_container.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../customer/views/components/customer_info_input.dart';
import '../../../globals/rounded_dropdown_field.dart';

class BusinessSetting extends StatelessWidget {
  const BusinessSetting({Key? key}) : super(key: key);

  BusinessSettingController get controller => Get.find();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: controller.getGlobalParameterData(),
      builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
        return SettingContainer(
          title: "Business Setting",
          child: ListView(
            shrinkWrap: true,
            children: [
              Obx(
                () => ItemSettings(
                  title: "Business Name",
                  value: controller.businessName.value,
                  isVisible: controller.isBusinessNameVisible.value,
                  visibleChild: Row(
                    children: [
                      Expanded(
                        child: RoundedInputField(
                          hintText: "Write here...",
                          controller: controller.businessNameController,
                          paddingVertical: 0,
                          contentPadding: 0,
                          maxLines: 1,
                        ),
                      ),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {
                          controller.saveBusinessName(context);
                        },
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessNameVisible.value =
                        !controller.isBusinessNameVisible.value;
                  },
                ),
              ),
              Obx(
                    () => ItemSettings(
                  title: "Phone No",
                  value: controller.businessPhone.value,
                  isVisible: controller.isBusinessPhoneVisible.value,
                  visibleChild: Row(
                    children: [
                      Expanded(
                        child: RoundedInputField(
                          hintText: "Write Phone No...",
                          paddingVertical: 0,
                          contentPadding: 0,
                          maxLines: 1,
                          controller: controller.businessPhoneController,
                        ),
                      ),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {
                          controller.saveBusinessPhone(context);
                        },
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessPhoneVisible.value =
                    !controller.isBusinessPhoneVisible.value;
                  },
                ),
              ),
              Obx(
                    () => ItemSettings(
                  title: "Email",
                  value: controller.businessEmail.value,
                  isVisible: controller.isBusinessEmailVisible.value,
                  visibleChild: Row(
                    children: [
                      Expanded(
                        child: RoundedInputField(
                          hintText: "Write here...",
                          paddingVertical: 0,
                          contentPadding: 0,
                          maxLines: 1,
                          controller: controller.businessEmailController,
                        ),
                      ),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {
                          controller.saveBusinessEmail(context);
                        },
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessEmailVisible.value =
                    !controller.isBusinessEmailVisible.value;
                  },
                ),
              ),
              Obx(
                () => ItemSettings(
                  title: "Business Location",
                  value: "${controller.businessAddress.value}"
                      " ${controller.businessPostCode.value}",
                  isVisible: controller.isBusinessAddressVisible.value,
                  visibleChild: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: RoundedInputField(
                              hintText: "Write Address...",
                              paddingVertical: 0,
                              contentPadding: 0,
                              maxLines: 1,
                              controller: controller.businessAddressController,
                            ),
                          ),
                          SizedBox(
                            width: Constants.defaultPadding / 4,
                          ),
                          Expanded(
                            child: RoundedInputField(
                              hintText: "Write Post Code...",
                              paddingVertical: 0,
                              contentPadding: 0,
                              maxLines: 1,
                              controller: controller.businessPostCodeController,
                            ),
                          ),
                          TextButton(
                            child: Text(
                              "Save",
                              style: TextStyle(
                                fontSize: Constants.mText,
                              ),
                            ),
                            onPressed: () {
                              controller.saveBusinessAddress(context);
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessAddressVisible.value =
                        !controller.isBusinessAddressVisible.value;
                  },
                ),
              ),
              Obx(
                () => ItemSettings(
                  title: "Business Status",
                  value: controller.status.value,
                  isVisible: controller.isBusinessStatusVisible.value,
                  visibleChild: Row(
                    children: [
                      Obx(
                        () => Checkbox(
                            value: controller.businessStatus.value,
                            onChanged: (value) {
                              controller.businessStatus.value = value!;
                            }),
                      ),
                      Spacer(),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {
                          controller.saveBusinessStatus(context);
                        },
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessStatusVisible.value =
                        !controller.isBusinessStatusVisible.value;
                  },
                ),
              ),
              Obx(
                    () => ItemSettings(
                  title: "Vat No.",
                  value: controller.businessVat.value,
                  isVisible: controller.isBusinessVatVisible.value,
                  visibleChild: Row(
                    children: [
                      Expanded(
                        child: RoundedInputField(
                          hintText: "Write here...",
                          paddingVertical: 0,
                          contentPadding: 0,
                          maxLines: 1,
                          controller: controller.businessVatController,
                        ),
                      ),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {
                          controller.saveBusinessVat(context);
                        },
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessVatVisible.value =
                    !controller.isBusinessVatVisible.value;
                  },
                ),
              ),
              Obx(
                    () => ItemSettings(
                  title: "Opening Time",
                  value: controller.selectedTime.value,
                  isVisible: controller.isBusinessOpeningTime.value,
                  visibleChild: Row(
                    children: [
                      Icon(Icons.access_time,color: Theme.of(context).colorScheme.primary,size: 34,),
                      SizedBox(width: 20,),
                      SizedBox(
                        width: 90,
                        child: GestureDetector(
                          onTap:(){ _selectTime(context);},
                          child: RoundedInputField(
                            hintText: controller.selectedTime.value,
                            paddingVertical: 0,
                            contentPadding: 0,
                            maxLines: 1,
                            isReadOnly: true,
                            isEnabled: false,
                            hintColor: Colors.black,
                          ),
                        ),
                      ),
                      Spacer(),
                      TextButton(
                        child: Text(
                          "Save",
                          style: TextStyle(
                            fontSize: Constants.mText,
                          ),
                        ),
                        onPressed: () {
                          controller.saveBusinessOpening(context);
                        },
                      ),
                    ],
                  ),
                  onTapEdit: () {
                    controller.isBusinessOpeningTime(!controller.isBusinessOpeningTime.value);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );


  }
  _selectTime(BuildContext context) async {
    final TimeOfDay? timeOfDay = await showTimePicker(
      context: context,
      initialTime: controller.businessOpeningTime.value,
      initialEntryMode: TimePickerEntryMode.dial,
      builder: (context, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: Color.fromRGBO(255, 165, 0, 1.0),
              onPrimary: Colors.black,
              // surface: Theme.of(context).colorScheme.background,
              onSurface: Color.fromRGBO(142, 139, 139, 1.0),
            ),
          ),
          child: child!,
        );
      },
    );
    if(timeOfDay != null)
    {
      controller.businessOpeningTime.value = timeOfDay;
      controller.selectedTime.value = "${controller.businessOpeningTime.value.hour}"
          ":${controller.businessOpeningTime.value.minute}:00";
      print(controller.selectedTime.value);
    }
  }
}
