import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/setting/views/components/setting_container.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../globals/rounded_button.dart';
import '../../controllers/customer_setting_controller.dart';

class CustomerSetting extends StatelessWidget {
  const CustomerSetting({Key? key}) : super(key: key);

  CustomerSettingController get controller => Get.find();
  @override
  Widget build(BuildContext context) {
    return SettingContainer(
      title: "Customer Setting",
      child: Padding(
        padding:  EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
        child: ListView(
          children: [
            RoundedButton(
              press: () {
                controller.importCustomer(context);
              },
              text: "Import Customer Information",radius: 10,padding: Constants.defaultPadding/2,),
            RoundedButton(press: () {controller.exportCustomer();},
              text: "Export Customer Information",radius: 10,padding: Constants.defaultPadding/2),

          ],
        ),
      ),
    );
  }
}
