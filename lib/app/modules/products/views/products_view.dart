import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/products_controller.dart';
import 'components/category/category_list.dart';
import 'components/products/product_list.dart';
import 'components/products/products_bottom_bar.dart';

class ProductsView extends GetView<ProductsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              CategoryList(
                title: "All Products",
                enableEdit: false,
                categories: [],
                onUpdate: (ConfigProductProperty item) {},
                onDelete: (ConfigProductProperty item) {},
              ),
              ProductList(
                flex: 8,
                products: [],
                isComponent: true,
                isOffer: true,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: ProductsBottomBar(),
    );
  }
}
