import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../../../../../data/local_db/app_db.dart';
import '../../../../../data/local_db/data_modules/bindings/product.repository.binding.dart';
import '../../../controllers/order_create_controller.dart';
import '../../../controllers/sub_cat_controller.dart';
import 'item_sub_category.dart';
import 'product_by_category.dart';

class SubCatProducts extends StatefulWidget {
  final MapEntry item;

  SubCatProducts({Key? key, required this.item}) : super(key: key);

  @override
  _SubCatProductsState createState() => _SubCatProductsState();
}

class _SubCatProductsState extends State<SubCatProducts> with AutomaticKeepAliveClientMixin{

  SubCatController get controller => Get.put(SubCatController(), tag: widget.item.key);
  OrderCreateController get collectionCtrl => Get.find();

  late ProductRepositoryBinding productRepo;

  @override
  void initState() {
    productRepo = ProductRepositoryBinding();
    super.initState();
    logData(widget.item);
  }

  @override
  Widget build(BuildContext context) {
    List<ConfigProductProperty> properties =
        (widget.item.value as List<ConfigProductProperty>);

    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
                top: Constants.defaultPadding / 2,
                left: Constants.defaultPadding / 2),
            child: MasonryGridView.count(
                crossAxisCount: 9,
                itemCount: properties.length,
                crossAxisSpacing: 7,
                mainAxisSpacing: Constants.defaultPadding / 2,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                // staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                itemBuilder: (context, index) {
                  return Obx(() {
                    return ItemSubCategory(
                      property: properties[index],
                      isSelected: controller.selectedCategoryIndex.value == index,
                      onTap: () {
                        controller.selectedCategoryIndex(index);
                      },
                    );
                  });
                }),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(
                top: Constants.defaultPadding / 2,
                left: Constants.defaultPadding / 4,
                right: Constants.defaultPadding / 4,
              ),
              child: Obx(() => ProductByCategory(
                category: properties[controller.selectedCategoryIndex.value].value,
              )),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => false;
}
