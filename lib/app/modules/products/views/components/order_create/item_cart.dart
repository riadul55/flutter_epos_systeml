import 'package:epos_system/app/modules/products/controllers/cart_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ItemCart extends GetView {
  final int index;

  ItemCart({Key? key, required this.index}) : super(key: key);

  @override
  CartController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: Constants.defaultElevation,
      child: Column(
        children: [
          InkWell(
            onTap: () {
              if (index == controller.selectedCartIndex.value) {
                controller.selectedCartIndex(-1);
              } else {
                controller.selectedCartIndex(index);
              }
              controller.update();
            },
            child: Column(
              children: [
                Container(
                  child: Padding(
                    padding: EdgeInsets.all(Constants.defaultPadding / 2),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              "x1",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).colorScheme.onBackground,
                              ),
                            ),
                            SizedBox(
                              width: Constants.defaultPadding,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Aloo Chat",
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: Constants.sText,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onBackground,
                                    ),
                                  ),
                                  // Container(),
                                  // Text(
                                  //   "Aloo Chat",
                                  //   softWrap: true,
                                  //   overflow: TextOverflow.ellipsis,
                                  //   style: TextStyle(
                                  //     fontSize: Constants.sText,
                                  //     color: Theme.of(context)
                                  //         .colorScheme
                                  //         .onBackground,
                                  //   ),
                                  // )
                                ],
                              ),
                            ),
                            Text(
                              "4.95",
                              style: TextStyle(
                                fontSize: Constants.sText,
                                color:
                                    Theme.of(context).colorScheme.onBackground,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          "Group : Com -> subcom",
                          style: TextStyle(
                            fontSize: Constants.xsText,
                            fontWeight: FontWeight.normal,
                            color:
                            Theme.of(context).colorScheme.onBackground,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

          // expandable
          Visibility(
            visible: index == controller.selectedCartIndex.value,
            child: Column(
              children: [
                Container(
                  color: Theme.of(context)
                      .colorScheme
                      .secondaryVariant
                      .withOpacity(0.3),
                  child: Table(
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    columnWidths: {
                      0: IntrinsicColumnWidth(),
                      1: IntrinsicColumnWidth(),
                      2: IntrinsicColumnWidth(),
                      3: FlexColumnWidth(),
                      4: IntrinsicColumnWidth(),
                      5: IntrinsicColumnWidth(),
                      6: IntrinsicColumnWidth(),
                      7: IntrinsicColumnWidth(),
                    },
                    children: [
                      TableRow(children: [
                        IconButton(
                          icon: Icon(
                            Icons.note_add_outlined,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.attach_money,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.add_shopping_cart_outlined,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {},
                        ),
                        Container(),
                        IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.remove,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {},
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: Constants.defaultPadding / 5),
                          child: Text(
                            "1",
                            style: TextStyle(
                              fontSize: Constants.mText,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.add,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {},
                        ),
                      ]),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  color: Theme.of(context).colorScheme.primary.withOpacity(0.2),
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding,
                    vertical: Constants.defaultPadding / 3,
                  ),
                  child: Text(
                    "add note",
                    style: TextStyle(
                      fontSize: Constants.xsText,
                      color: Theme.of(context).colorScheme.onBackground,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
