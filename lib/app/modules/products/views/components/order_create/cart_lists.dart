import 'package:epos_system/app/modules/products/controllers/cart_controller.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/item_cart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartList extends GetView {
  CartList({Key? key}) : super(key: key);

  @override
  CartController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: GetBuilder(
          init: CartController(),
          builder: (_) {
            return ListView(
              physics: BouncingScrollPhysics(),
              children: List.generate(5, (index) => ItemCart(index: index,)),
            );
          },
        ),
      ),
    );
  }
}
