import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/products/controllers/order_create_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../data/local_db/app_db.dart';
import 'sub_cat_products.dart';

class MainProductView extends StatelessWidget {
  MainProductView({Key? key}) : super(key: key);

  OrderCreateController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 8,
      child: FutureBuilder(
          future: controller.getAllCategories(),
          builder: (context, AsyncSnapshot<List<ConfigProductProperty>> snapshot) {
            if (snapshot.hasData) {
              var categories = controller.getCategoriesGroupByKeyName(snapshot.data ?? []);
              return Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 8,
                        child: Container(
                          color: Theme.of(context)
                              .colorScheme
                              .secondaryVariant
                              .withOpacity(0.2),
                          child: TabBar(
                            isScrollable: true,
                            labelColor: Theme.of(context).colorScheme.onPrimary,
                            unselectedLabelColor:
                            Theme.of(context).colorScheme.onBackground,
                            indicatorColor: Theme.of(context).colorScheme.primary,
                            indicator: BoxDecoration(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                            controller: controller.mainCategoryController,
                            tabs: categories.keys.map((e) => Tab(
                              child: Text(e.toUpperCase(),style: GoogleFonts.roboto(textStyle: TextStyle(fontWeight: FontWeight.bold)),),
                            )).toList(),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: RoundedInputField(
                          paddingVertical: 0,
                          marginVertical: 0,
                          hintText: "Search here..",
                          onChanged: (val) {},
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Constants.defaultPadding / 2),
                  divider(context),
                  Expanded(
                    child: TabBarView(
                      controller: controller.mainCategoryController,
                      physics: NeverScrollableScrollPhysics(),
                      children: categories.entries.map((e) {
                        return SubCatProducts(item: e);
                      }).toList(),
                    ),
                  ),
                ],
              );
            } else {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          }),
    );
  }
}
