import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class CartBottom extends StatelessWidget {
  const CartBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        divider(context),
        Padding(
          padding: EdgeInsets.only(
            left: Constants.defaultPadding / 2,
            right: Constants.defaultPadding / 2,
            bottom: Constants.defaultPadding / 5,
            top: Constants.defaultPadding / 5,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Total", style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold,
              ),),
              Text("77.00", style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold,
              ),),
            ],
          ),
        ),
      ],
    );
  }
}
