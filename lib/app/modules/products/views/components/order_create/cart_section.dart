import 'package:epos_system/app/modules/products/controllers/cart_controller.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/cart_body.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/cart_bottom.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartSection extends StatelessWidget {
  CartSection({Key? key}) : super(key: key);

  CartController get controller => Get.put(CartController());

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              width: 1,
              color: Theme.of(context).colorScheme.secondaryVariant,
            ),
          ),
        ),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: CartBody(),
          bottomNavigationBar: CartBottom(),
        ),
      ),
    );
  }
}
