import 'package:epos_system/app/core/utilities/my_storage.dart';
import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:epos_system/app/modules/products/controllers/order_create_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../../../../../data/local_db/models/product_result.dart';
import '../../../../globals/log_info.dart';
import '../../../../globals/rounded_button.dart';
import '../../../../globals/rounded_input_field.dart';

class ProductByCategory extends StatefulWidget {
  final String category;

  ProductByCategory({Key? key, required this.category}) : super(key: key);

  @override
  _ProductByCategoryState createState() => _ProductByCategoryState();
}

class _ProductByCategoryState extends State<ProductByCategory> {
  OrderCreateController get controller => Get.find();

  @override
  void initState() {
    // controller.productRepo.repository.getProductsByCategory(widget.category);
  }

  @override
  Widget build(BuildContext context) {
    print(widget.category);
    return GetBuilder(
        init: controller,
        builder: (_) {
          return SingleChildScrollView(
            child: FutureBuilder(
              future: controller.productRepo.repository.getProductsByCategory(widget.category),
              builder: (ctx, AsyncSnapshot<List<ProductResult>> snap) {
                if (snap.hasData) {
                  List<ProductResult> data = snap.data ?? [];
                  return MasonryGridView.count(
                      crossAxisCount: controller.storage.mainProductItemCrossCount.val,
                      itemCount: data.length,
                      crossAxisSpacing: Constants.defaultPadding * 0.2,
                      mainAxisSpacing: Constants.defaultPadding * 0.4,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      // staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                      itemBuilder: (context, index) {
                        bool isSelected = controller
                            .selectedMainProductIndexList.value['${widget.category}']
                            ?.toList()
                            .contains(index) ?? false;
                        return GestureDetector(
                          onLongPress: (){showEditProductDialog(context);},
                          child: Card(
                            elevation: 6,
                            child: Material(
                              color: isSelected
                                  ? Color.fromRGBO(220, 20, 60, 1)
                                  : Theme.of(context).colorScheme.primary,
                              child: InkWell(
                                onTap: () {
                                  if (data[index].shortName == "Aloo Chop") {
                                    controller.isShowSubItemScreen(true);
                                    controller.update();
                                  } else {
                                    var indexList = controller.selectedMainProductIndexList.value['${widget.category}'] ?? [];
                                    if (isSelected) {
                                      indexList.remove(index);
                                    } else {
                                      indexList.add(index);
                                    }
                                    controller.selectedMainProductIndexList['${widget.category}'] = indexList;
                                    controller.update();
                                  }
                                },
                                child: Container(
                                  height: controller.storage.mainProductItemHeight.val,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                  ),
                                  child: Column(
                                    children: [
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                            horizontal: Constants.defaultPadding / 2,
                                            vertical: Constants.defaultPadding / 10,
                                          ),
                                          color: Color.fromRGBO(42, 174, 47, 1.0),
                                          child: Text(
                                            "${data[index].price?.price ?? "0.0"}",
                                            style: TextStyle(
                                              color: Color.fromRGBO(18, 72, 19, 1.0),
                                              fontWeight: FontWeight.bold,
                                              fontSize: Constants.mText,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Text(
                                          "${data[index].shortName}",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onBackground,
                                            fontWeight: FontWeight.bold,
                                            fontSize: Constants.sText,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      });
                } else {
                  return Container();
                }
              },
            ),
          );
        });
  }

  @override
  bool get wantKeepAlive => true;
  showEditProductDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      elevation: 5,
      backgroundColor: Colors.white,
      scrollable: true,
      title: ItemEditProductDialog(),
    );

    showAnimatedDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
      animationType: DialogTransitionType.scale,
      curve: Curves.fastOutSlowIn,
      duration: const Duration(milliseconds: 600),
    );

    /*showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return alert;
      },
    );*/
  }
}

class ItemEditProductDialog extends StatelessWidget {
  const ItemEditProductDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return Container(width: 500,child: Column(children: [
      Text("Update"),
      SizedBox(height: Constants.defaultPadding,),
      Row(
        children: [
          Text(
            "Product Name",
            style: TextStyle(
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.onBackground,
            ),
          ),
          Spacer(),
          SizedBox(
            width: size.width/4,
            child: RoundedInputField(
              paddingVertical: 0,
              marginVertical: 0,
              contentPadding: 0,
              hintText: "Austrailan Semillion Chardonnary",
            ),
          ),
        ],
      ),
      SizedBox(height: Constants.defaultPadding/2,),
      Row(
        children: [
          Text(
            "Product Price",
            style: TextStyle(
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.onBackground,
            ),
          ),
          Spacer(),
          SizedBox(
            width: size.width/4,
            child: RoundedInputField(
              paddingVertical: 0,
              marginVertical: 0,
              contentPadding: 0,
              inputType: TextInputType.number,
              hintText: "5.95",
            ),
          ),
        ],
      ),
      Row(
        children: [
          Text(
            "Product Description",
            style: TextStyle(
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.onBackground,
            ),
          ),
          Spacer(),
          SizedBox(
            width: size.width/4,
            child: RoundedInputField(
              hintText: "Description",
              maxLines: 3,
            ),
          ),
        ],
      ),
      Row(
        children: [
          Text(
            "Available",
            style: TextStyle(
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.onBackground,
            ),
          ),
          Spacer(),
          SizedBox(
            width: size.width/3.85,
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Checkbox(
                value: true,
                onChanged: (bool? value) {  },),
            )
          ),
        ],
      ),

      SizedBox(height: Constants.defaultPadding/2,),
      Row(children: [
        Expanded(
          child: RoundedButton(
            text: "Cancel",
            padding: Constants.defaultPadding / 2,
            radius: 0,color: Colors.grey,
            press: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SizedBox(width: Constants.defaultPadding,),
        Expanded(
          child: RoundedButton(
            text: "Update",
            padding: Constants.defaultPadding / 2,
            radius: 0,
            press: () {
            },
          ),
        ),
      ],)


    ],),);
  }
}