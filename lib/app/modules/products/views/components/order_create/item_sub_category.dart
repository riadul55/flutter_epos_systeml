import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

import '../../../../../data/local_db/app_db.dart';

class ItemSubCategory extends StatelessWidget {
  final ConfigProductProperty property;
  final bool isSelected;
  final Function() onTap;

  ItemSubCategory({Key? key, required this.property, required this.onTap, this.isSelected = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 85,
        decoration: BoxDecoration(
          color: isSelected
              ? Color.fromRGBO(220, 20, 60, 1)
              : Color.fromRGBO(57, 166, 163, 1),
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).colorScheme.secondaryVariant,
              blurRadius: 1,
              spreadRadius: 0,
              offset: Offset(0, 1),
            )
          ],
        ),
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding/4),
            child: Text(
              property.displayName,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
