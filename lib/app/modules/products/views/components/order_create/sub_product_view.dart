import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/products/controllers/order_create_controller.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class SubProductView extends GetView {
  SubProductView({Key? key}) : super(key: key);

  @override
  OrderCreateController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Expanded(
          flex: 8,
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      right: BorderSide(
                        width: 1,
                        color: Theme.of(context).colorScheme.secondaryVariant,
                      ),
                    ),
                  ),
                  child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    body: Container(
                      child: Column(
                        children: [
                          header(context, title: "Buter Chicken", price: "6.5"),
                          divider(context),
                          Expanded(
                              child: Container(
                            child: ListView(
                              physics: BouncingScrollPhysics(),
                              children: List.generate(
                                  15,
                                  (index) => Container(
                                        padding: EdgeInsets.symmetric(
                                          vertical:
                                              Constants.defaultPadding / 2,
                                          horizontal:
                                              Constants.defaultPadding / 2,
                                        ),
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondaryVariant,
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                        child: Row(
                                          children: [
                                            Text(
                                              "1",
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .onBackground,
                                                fontSize: Constants.sText,
                                              ),
                                            ),
                                            SizedBox(
                                              width:
                                                  Constants.defaultPadding / 2,
                                            ),
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Chicken",
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .onBackground,
                                                    fontSize: Constants.sText,
                                                  ),
                                                ),
                                                Container(),
                                                // Text("Chicken", style: TextStyle(
                                                //   color: Theme.of(context).colorScheme.onBackground,
                                                //   fontSize: Constants.xsText,
                                                // ),),
                                              ],
                                            )),
                                            SizedBox(
                                              width:
                                                  Constants.defaultPadding / 2,
                                            ),
                                            Text(
                                              "2.00",
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .onBackground,
                                                fontSize: Constants.sText,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                            ),
                          ))
                        ],
                      ),
                    ),
                    bottomNavigationBar: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        divider(context),
                        Padding(
                          padding: EdgeInsets.only(
                            left: Constants.defaultPadding / 2,
                            right: Constants.defaultPadding / 2,
                            bottom: Constants.defaultPadding / 5,
                            top: Constants.defaultPadding / 5,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Total",
                                style: TextStyle(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onBackground,
                                  fontSize: Constants.sText,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                "77.00",
                                style: TextStyle(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onBackground,
                                  fontSize: Constants.sText,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      header(context, title: "Protein"),
                      divider(context),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(Constants.defaultPadding / 2),
                          child: SingleChildScrollView(
                            child: MasonryGridView.count(
                                crossAxisCount: controller
                                    .storage.subProductItemCrossCount.val,
                                itemCount: 10,
                                crossAxisSpacing:
                                    Constants.defaultPadding * 0.2,
                                mainAxisSpacing: Constants.defaultPadding * 0.4,
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                // staggeredTileBuilder: (index) =>
                                //     StaggeredTile.fit(1),
                                itemBuilder: (context, index) {
                                  bool isSelected = controller
                                      .selectedSubProductIndexList.value
                                      .contains(index);
                                  return Card(
                                    elevation: 6,
                                    child: Material(
                                      color: isSelected
                                          ? Color.fromRGBO(220, 20, 60, 1)
                                          : Theme.of(context)
                                              .colorScheme
                                              .primary,
                                      child: InkWell(
                                        onTap: () {
                                          var indexList = controller
                                              .selectedSubProductIndexList
                                              .value;
                                          if (isSelected) {
                                            indexList.remove(index);
                                          } else {
                                            indexList.add(index);
                                          }
                                          controller
                                              .selectedSubProductIndexList(
                                                  indexList);
                                          controller.update();
                                        },
                                        child: Container(
                                          height: controller.storage.subProductItemHeight.val,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(2)),
                                          ),
                                          child: Column(
                                            children: [
                                              Align(
                                                alignment: Alignment.topRight,
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                    horizontal: Constants
                                                            .defaultPadding /
                                                        2,
                                                    vertical: Constants
                                                            .defaultPadding /
                                                        10,
                                                  ),
                                                  color: Color.fromRGBO(
                                                      42, 174, 47, 1.0),
                                                  child: Text(
                                                    "5.95",
                                                    style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          18, 72, 19, 1.0),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: Constants.mText,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Center(
                                                child: Text(
                                                  "Chicken",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .onBackground,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: Constants.mText,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ),
                      Container(
                        height: Constants.defaultPadding * 2,
                        child: Row(
                          children: [
                            Expanded(
                                child: Material(
                              color: Theme.of(context).colorScheme.primary,
                              child: InkWell(
                                onTap: () {
                                  controller.isShowSubItemScreen(false);
                                  controller.update();
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: Constants.defaultPadding,
                                    vertical: Constants.defaultPadding / 2,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.arrow_back,
                                        size: Constants.iconSize * 0.7,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .background,
                                      ),
                                      SizedBox(
                                          width: Constants.defaultPadding / 2),
                                      Text(
                                        "Back".toUpperCase(),
                                        style: TextStyle(
                                          fontSize: Constants.mText,
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onSecondary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )),
                            SizedBox(width: Constants.defaultPadding / 5),
                            Expanded(
                                child: Material(
                              color: Theme.of(context).colorScheme.primary,
                              child: InkWell(
                                onTap: () {},
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: Constants.defaultPadding,
                                    vertical: Constants.defaultPadding / 2,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add,
                                        size: Constants.iconSize * 0.7,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .background,
                                      ),
                                      SizedBox(
                                          width: Constants.defaultPadding / 2),
                                      Text(
                                        "Add".toUpperCase(),
                                        style: TextStyle(
                                          fontSize: Constants.mText,
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onSecondary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      )
                    ],
                  )),
            ],
          ),
        );
      },
    );
  }

  header(BuildContext context, {required String title, String? price}) =>
      Padding(
        padding: EdgeInsets.all(Constants.defaultPadding / 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              price ?? "",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );
}
