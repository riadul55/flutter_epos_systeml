import 'package:epos_system/app/routes/app_pages.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductsBottomNav extends StatelessWidget {
  const ProductsBottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.secondaryVariant,
      height: Constants.preferredSize,
      child: Row(
        children: [
          Material(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: InkWell(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Back",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Material(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.check,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Save",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Spacer(),
          Material(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.add,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Dynamic",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Material(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.delete_forever,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Clear",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Material(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.close,
                      size: Constants.iconSize,
                      color: Colors.red.shade900,
                    ),
                    Text(
                      "Void",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Material(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: InkWell(
              onTap: () {
                Get.toNamed(Routes.CHECKOUT);
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.shopping_cart,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Checkout",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
