import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/app/modules/products/controllers/cart_controller.dart';
import 'package:epos_system/app/routes/app_pages.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'cart_lists.dart';

class CartBody extends GetView {
  CartBody({Key? key}) : super(key: key);

  @override
  CartController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          cartHeader(context),
          divider(context),
          selectedCustomer(context),
          divider(context),
          CartList(),
        ],
      ),
    );
  }

  Widget cartHeader(BuildContext context) => Padding(
        padding: EdgeInsets.all(Constants.defaultPadding / 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Order : #1",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "Collection".toUpperCase(),
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );

  Widget selectedCustomer(BuildContext context) => InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: Constants.defaultPadding / 2,
            vertical: Constants.defaultPadding / 5,
          ),
          child: Row(
            children: [
              Icon(
                Icons.account_circle,
                color: Theme.of(context).colorScheme.primary,
                size: Constants.iconSize * 1.3,
              ),
              SizedBox(width: Constants.defaultPadding / 2),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Customer",
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontWeight: FontWeight.bold,
                      fontSize: Constants.sText,
                    ),
                  ),
                  Text(
                    "Select Customer",
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontSize: Constants.xsText,
                    ),
                  ),
                ],
              )),
            ],
          ),
        ),
        onTap: () {
          Get.toNamed(Routes.CUSTOMER);
        },
      );
}
