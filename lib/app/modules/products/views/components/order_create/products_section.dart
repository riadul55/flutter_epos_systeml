import 'package:epos_system/app/modules/products/controllers/order_create_controller.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/main_product_view.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/sub_product_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductsSection extends StatelessWidget {
  ProductsSection({Key? key}) : super(key: key);

  OrderCreateController get controller => Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (controller.isShowSubItemScreen.value) {
        return SubProductView();
      } else {
        return MainProductView();
      }
    });
  }
}
