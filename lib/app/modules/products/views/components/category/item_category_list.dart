import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/products/controllers/category_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ItemCategoryList extends StatelessWidget {
  final MapEntry data;
  final bool enableEdit;
  final Function(ConfigProductProperty item) onUpdate;
  final Function(ConfigProductProperty item) onDelete;

  ItemCategoryList({
    Key? key,
    required this.data,
    this.enableEdit = true,
    required this.onUpdate,
    required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(data.value);
    
    return Container(
      child: Column(
        children: [
          HeaderTitle(
            title: "${data.key}",
            bgColor: Theme.of(context).colorScheme.primaryVariant,
          ),
          Column(
            children: (data.value as List<ConfigProductProperty>).map((item) {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Theme.of(context).colorScheme.primaryVariant,
                      width: 1,
                    ),
                  ),
                ),
                child: ListTile(
                  horizontalTitleGap: 0,
                  dense: true,
                  leading: Icon(
                    Icons.category,
                    size: Constants.iconSize,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  title: Text(
                    item.displayName,
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontSize: Constants.mText,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  // subtitle: Text(
                  //   item.description,
                  //   style: TextStyle(
                  //     color: Theme.of(context).colorScheme.onBackground,
                  //     fontSize: Constants.sText,
                  //   ),
                  // ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      enableEdit
                          ? Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                onUpdate(item);
                                },
                              icon: Icon(
                                Icons.edit,
                                color:
                                Theme.of(context).colorScheme.primary,
                              )),
                          IconButton(
                              onPressed: () {
                                onDelete(item);
                              },
                              icon: Icon(
                                Icons.delete,
                                color:
                                Theme.of(context).colorScheme.primary,
                              )),
                        ],
                      )
                          : SizedBox.shrink(),
                      Container(
                        padding: EdgeInsets.all(Constants.defaultPadding / 4),
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.primaryVariant,
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          "${item.sortOrder < 10 ? "0${item.sortOrder}" : item.sortOrder}",
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onPrimary,
                            fontSize: Constants.sText,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
