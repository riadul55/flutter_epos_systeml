import 'package:collection/collection.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/products/views/components/category/item_category_list.dart';
import 'package:flutter/material.dart';

class CategoryList extends StatelessWidget {
  final String title;
  final bool enableEdit;
  final List<ConfigProductProperty>? categories;
  final Function(String value)? onSearch;
  final Function(ConfigProductProperty item) onUpdate;
  final Function(ConfigProductProperty item) onDelete;

  const CategoryList({
    Key? key,
    required this.title,
    this.enableEdit = true,
    this.categories,
    required this.onUpdate,
    required this.onDelete,
    this.onSearch,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> newGroupList =
        categories != null && categories!.isNotEmpty
            ? groupBy(categories!, (ConfigProductProperty obj) => obj.keyName)
            : {};
    print(newGroupList.keys.toString());
    return Expanded(
        flex: 3,
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              right: BorderSide(
                color: Theme.of(context).colorScheme.primaryVariant,
                width: 1,
              ),
            ),
          ),
          child: Column(
            children: [
              HeaderTitle(title: title),
              _searchCategory(context),
              Expanded(
                child: newGroupList.isNotEmpty
                    ? ListView.builder(
                        itemCount: newGroupList.keys.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return ItemCategoryList(
                              data: newGroupList.entries.elementAt(index),
                              enableEdit: enableEdit,
                            onUpdate: onUpdate,
                            onDelete: onDelete,
                          );
                        },
                      )
                    : Container(
                        child: Center(
                          child: Text("Empty!"),
                        ),
                      ),
              ),
            ],
          ),
        ));
  }

  _searchCategory(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Theme.of(context).colorScheme.primaryVariant,
            width: 1,
          ),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: RoundedInputField(
              hintText: "Category",
              marginVertical: 0,
              paddingVertical: 0,
              width: 0,
              borderColor: Colors.transparent,
              onChanged: onSearch,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Constants.defaultPadding / 2,
            ),
            child: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
                color: Theme.of(context).colorScheme.primary,
                size: Constants.iconSize,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
