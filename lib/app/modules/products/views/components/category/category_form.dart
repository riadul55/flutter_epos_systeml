import 'dart:io';

import 'package:collection/collection.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_dropdown_field.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/products/controllers/category_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class CategoryForm extends StatelessWidget {
  CategoryForm({Key? key}) : super(key: key);

  CategoryController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 5,
      child: Obx(() => Container(
            alignment: Alignment.topLeft,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  HeaderTitle(title: "Add/Update Category"),
                  Padding(
                    padding: EdgeInsets.all(Constants.defaultPadding),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                              "Select Property Name",
                              style: TextStyle(
                                color:
                                    Theme.of(context).colorScheme.onBackground,
                                fontSize: Constants.mText,
                              ),
                            )),
                            Expanded(
                              flex: 2,
                              child: RoundedDropdownField(
                                hintText: "Select",
                                value: controller.selectedCategory.keyName,
                                list: controller.getPropertyNames,
                                onChanged: (value) {
                                  controller.selectedCategoryIndex(controller.getCategoryIndex(value));
                                },
                              ),
                            ),
                          ],
                        ),
                        controller.propertyNameInputExist
                            ? _inputFieldItems(
                          context,
                          "New Property Name",
                          controller.propertyNameInput,
                          controller.isPropertyNameRequired,
                        )
                            : Container(),
                        _inputFieldItems(
                          context,
                          "Sort Order",
                          controller.sortOrderInput,
                          controller.isSortOrderRequired,
                          type: TextInputType.number,
                        ),
                        _inputFieldItems(
                          context,
                          "Menu Category Name",
                          controller.menuCatNameInput,
                          controller.isMenuCatNameRequired,
                        ),
                        _inputFieldItems(
                          context,
                          "Description",
                          controller.descriptionInput,
                          controller.isDescriptionRequired,
                        ),
                        // Row(
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   children: [
                        //     Expanded(
                        //       child: Text(
                        //         "File",
                        //         style: TextStyle(
                        //           color: Theme.of(context)
                        //               .colorScheme
                        //               .onBackground,
                        //           fontSize: Constants.mText,
                        //         ),
                        //       ),
                        //     ),
                        //     Expanded(
                        //       flex: 2,
                        //       child: InkWell(
                        //         onTap: () {
                        //           controller.showImagePicker();
                        //         },
                        //         child: Container(
                        //           padding:
                        //               EdgeInsets.all(Constants.defaultPadding),
                        //           decoration: BoxDecoration(
                        //             border: Border.all(
                        //               color: Theme.of(context)
                        //                   .colorScheme
                        //                   .primaryVariant,
                        //             ),
                        //           ),
                        //           child: controller.filePath.value.isNotEmpty
                        //               ? Image.file(
                        //                   File(controller.filePath.value),
                        //                 )
                        //               : Icon(
                        //                   Icons.camera_alt,
                        //                   size: Constants.iconSize * 3,
                        //                   color: Theme.of(context)
                        //                       .colorScheme
                        //                       .primaryVariant,
                        //                 ),
                        //         ),
                        //       ),
                        //     )
                        //   ],
                        // ),
                        Row(
                          children: [
                            Expanded(
                              child: RoundedButton(
                                text: "Clear",
                                radius: 0,
                                padding: 0,
                                press: () {
                                  controller.onSubmitClear();
                                },
                              ),
                            ),
                            SizedBox(width: Constants.defaultPadding),
                            Expanded(
                              child: RoundedButton(
                                text: controller.isUpdatingEnable.value ? "Update" : "Add New",
                                radius: 0,
                                padding: 0,
                                press: () {
                                  if (controller.isUpdatingEnable.value) {
                                    controller.onSubmitUpdate();
                                  } else {
                                    controller.onSubmitAdd();
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  _inputFieldItems(
    BuildContext context,
    String title,
    TextEditingController controller,
    RxBool isRequired,
  {type}
  ) {
    return Row(
      children: [
        Expanded(
            child: Text(
          title,
          style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: Constants.mText,
          ),
        )),
        Expanded(
          flex: 2,
          child: RoundedInputField(
            controller: controller,
            paddingVertical: 0,
            hintText: "",
            borderColor: isRequired.value ? Colors.red : null,
            inputType: type,
            onChanged: (value) {
              isRequired(false);
            },
          ),
        ),
      ],
    );
  }
}
