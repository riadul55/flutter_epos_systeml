import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductInputContainer extends StatelessWidget {
  final String title;
  final int? maxLine;
  final TextEditingController? controller;
  final bool isRequired;
  final Function(String)? onChange;
  final TextInputType? inputType;

  const ProductInputContainer({
    Key? key,
    required this.title,
    this.maxLine,
    this.controller,
    this.isRequired = false,
    this.onChange,
    this.inputType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Constants.defaultPadding / 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
            ),
          ),
          RoundedInputField(
            controller: controller,
            marginVertical: 0,
            paddingVertical: 0,
            maxLines: maxLine ?? 1,
            hintText: "",
            borderColor: isRequired ? Colors.red : null,
            onChanged: onChange,
            inputType: inputType,
          ),
        ],
      ),
    );
  }
}
