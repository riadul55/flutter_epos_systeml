import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/globals/rounded_dropdown_field.dart';
import 'package:epos_system/app/modules/globals/rounded_input_field.dart';
import 'package:epos_system/app/modules/products/controllers/main_products_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../data/local_db/app_db.dart';
import '../../../../globals/sort_order_input_container.dart';
import 'product_dropdown_container.dart';
import 'product_input_container.dart';
import 'product_input_file_container.dart';

class ProductForm extends StatelessWidget {
  final String title;

  ProductForm({Key? key, required this.title}) : super(key: key);

  MainProductsController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              width: 1,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderTitle(title: title),
              Obx(() => Card(
                    child: Column(
                      children: [
                        ProductInputContainer(
                          title: "Display Sort Order",
                          inputType: TextInputType.number,
                          controller: controller.displaySortOrder,
                          isRequired:
                              controller.isDisplaySortOrderRequired.value,
                          onChange: (value) =>
                              controller.isDisplaySortOrderRequired(false),
                        ),
                        ProductInputContainer(
                          title: "Item Name",
                          controller: controller.itemNameInput,
                          isRequired: controller.isItemNameInputRequired.value,
                          onChange: (value) => controller.isItemNameInputRequired(false),
                        ),
                        ProductInputContainer(
                          title: "Description",
                          maxLine: 3,
                          controller: controller.descriptionInput,
                          isRequired:
                              controller.isDescriptionInputRequired.value,
                          onChange: (value) => controller.isDescriptionInputRequired(false),
                        ),
                        ProductInputContainer(
                          title: "URL",
                          controller: controller.urlInput,
                          isRequired: controller.isUrlInputRequired.value,
                          onChange: (value) => controller.isUrlInputRequired(false),
                        ),
                        // ProductInputFileContainer(
                        //     child: null,
                        //     onTap: () {
                        //       // image picker
                        //       controller.showImagePicker();
                        //     }),
                        ProductDropDownContainer(
                          title: "Menu Category",
                          value: controller.menuCategoryValue.value,
                          list: controller.menuCategoryList.value,
                          isRequired: controller.isMenuCategoryRequired.value,
                          onChanged: (value) {
                            controller.isMenuCategoryRequired(false);
                            controller.menuCategoryValue(value);
                          }
                        ),
                        ListTile(
                          title: Text(
                            "Sell on its own?",
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.onBackground,
                              fontSize: Constants.sText,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: Checkbox(
                            value: controller.sellOnItsOwn.value,
                            onChanged: (isChecked) {
                              controller.sellOnItsOwn(isChecked);
                            },
                          ),
                        ),
                        ProductDropDownContainer(
                          title: "Available on",
                          value: controller.availableOptionValue.value,
                          list: controller.availableOptionList,
                          onChanged: (value) =>
                              controller.availableOptionValue(value),
                        ),
                        ProductDropDownContainer(
                          title: "Review Enabled",
                          value: controller.reviewOptionValue.value,
                          list: controller.reviewOptionList,
                          onChanged: (value) =>
                              controller.reviewOptionValue(value),
                        ),
                        ProductInputContainer(
                          title: "Price",
                          inputType: TextInputType.number,
                          controller: controller.priceInput,
                          isRequired: controller.isPriceInputRequired.value,
                          onChange: (value) =>
                              controller.isPriceInputRequired(false),
                        ),
                        ProductInputContainer(
                          title: "Vat (%)",
                          inputType: TextInputType.number,
                          controller: controller.vatInput,
                          isRequired: controller.isVatInputRequired.value,
                          onChange: (value) =>
                              controller.isVatInputRequired(false),
                        ),
                        ProductInputContainer(
                          title: "Invoice Print order",
                          inputType: TextInputType.number,
                          controller: controller.invoicePrintOrder,
                          isRequired:
                              controller.isInvoicePrintOrderRequired.value,
                          onChange: (value) =>
                              controller.isInvoicePrintOrderRequired(false),
                        ),
                        ProductInputContainer(
                          title: "Invoice Print Item & Component order",
                          inputType: TextInputType.number,
                          controller: controller.invoicePrintItemOrder,
                          isRequired:
                              controller.isInvoicePrintItemOrderRequired.value,
                          onChange: (value) =>
                              controller.isInvoicePrintItemOrderRequired(false),
                        ),
                        ProductDropDownContainer(
                          title: "Offer",
                          list: controller.offerList,
                          value: controller.offerValue.value,
                          onChanged: (value) => controller.offerValue(value),
                        ),
                        ProductInputContainer(
                          title: "Offer Price",
                          controller: controller.offerPriceInput,
                          inputType: TextInputType.number,
                          isRequired:
                              controller.isOfferPriceInputRequired.value,
                          onChange: (value) =>
                              controller.isOfferPriceInputRequired(false),
                        ),
                        ProductDropDownContainer(
                          title: "Discount?",
                          list: controller.discountList,
                          value: controller.discountValue.value,
                          onChanged: (value) => controller.discountValue(value),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding / 2,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: RoundedButton(
                                  text: "Clear",
                                  padding: Constants.defaultPadding / 2,
                                  radius: 0,
                                  press: () {
                                    controller.onClear();
                                  },
                                ),
                              ),
                              SizedBox(width: Constants.defaultPadding / 2),
                              Expanded(
                                child: RoundedButton(
                                  text: controller.isUpdatingEnable.value
                                      ? "Update"
                                      : "Add New",
                                  padding: Constants.defaultPadding / 2,
                                  radius: 0,
                                  press: () {
                                    if (controller.isUpdatingEnable.value) {
                                      controller.onSubmitUpdate();
                                    } else {
                                      controller.onSubmitAdd();
                                    }
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
