import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:flutter/material.dart';

import 'multi_selction_container.dart';
import 'product_dropdown_container.dart';

class LinkSubItemWithItself extends StatelessWidget {
  const LinkSubItemWithItself({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.symmetric(
          horizontal: Constants.defaultPadding * 2,
          vertical: Constants.defaultPadding,
        ),
        child: SingleChildScrollView(
          child: Card(
            elevation: Constants.defaultElevation,
            child: Column(
              children: [
                HeaderTitle(title: "Link SubItem/Component with It-self"),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding,
                  ),
                  child: Column(
                    children: [
                      // ProductDropDownContainer(title: "SubItem/Component"),
                      MultiSelectionContainer(title: "SubItems/Components"),
                      // ProductDropDownContainer(title: "Select from existing groups"),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Constants.defaultPadding / 2,
                        ),
                        child: RoundedButton(
                          text: "Create Link",
                          padding: Constants.defaultPadding / 2,
                          radius: 0,
                          press: () {},
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
