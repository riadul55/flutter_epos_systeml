import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductsBottomBar extends StatelessWidget {
  const ProductsBottomBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.secondaryVariant,
      height: Constants.preferredSize,
      child: Row(
        children: [
          Material(
            color: Theme.of(context).colorScheme.secondary,
            child: InkWell(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding),
                child: Column(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      size: Constants.iconSize,
                      color: Theme.of(context).colorScheme.onSecondary,
                    ),
                    Text(
                      "Back",
                      style: TextStyle(
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
