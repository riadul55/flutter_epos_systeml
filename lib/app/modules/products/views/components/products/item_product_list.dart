import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ItemProductList extends StatelessWidget {
  final bool isComponent;
  final bool isOffer;
  final bool isFromSub;
  final Product product;

  const ItemProductList({
    Key? key,
    this.isComponent = false,
    this.isOffer = false,
    this.isFromSub = false,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: Constants.defaultElevation,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: Constants.defaultPadding / 2,
        ),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Text(
                "01",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: Constants.mText,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            // Expanded(
            //   flex: 1,
            //   child: Container(
            //     height: Constants.iconSize * 2,
            //     decoration: BoxDecoration(
            //       image: DecorationImage(
            //         image: NetworkImage(
            //             "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTc9ffLuEvr27s5Lv8cVZ1_UFiRO91CmcFU9wwyFl1fKyRMB2GoFUJRi4TzBfygXETZe0&usqp=CAU"),
            //         fit: BoxFit.fitHeight,
            //       ),
            //     ),
            //     // child: Icon(
            //     //   Icons.image_outlined,
            //     //   size: Constants.iconSize * 2,
            //     //   color: Theme.of(context).colorScheme.primaryVariant,
            //     // ),
            //   ),
            // ),
            Expanded(
              flex: 3,
              child: Text(
                product.shortName,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: Constants.sText,
                ),
              ),
            ),
            isComponent
                ? Expanded(
                    flex: 3,
                    child: Column(
                      children: [
                        Text(
                          "Chicken".toUpperCase(),
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: Constants.sText,
                          ),
                        ),
                        Text(
                          "Meat".toUpperCase(),
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: Constants.sText,
                          ),
                        ),
                      ],
                    ),
                  )
                : SizedBox.shrink(),
            Expanded(
              flex: 1,
              child: Text(
                "GBP 12.95".toUpperCase(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: Constants.sText,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Checkbox(value: product.visible, onChanged: (isChecked) {}),
            ),
            Expanded(
              flex: 1,
              child: Text(
                "07".toUpperCase(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: Constants.sText,
                ),
              ),
            ),
            isOffer
                ? Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: Constants.defaultPadding / 5,
                          ),
                          color: Colors.red,
                          child: Text(
                            "No".toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.onPrimary,
                              fontSize: Constants.xsText,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          "GBP 12.95".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: Constants.sText,
                          ),
                        ),
                      ],
                    ),
                  )
                : SizedBox.shrink(),

            (!isOffer && !isComponent) ?
            Expanded(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: [
                  InkWell(
                    child: Container(
                      padding: EdgeInsets.all(Constants.defaultPadding / 4),
                      color: Theme.of(context).colorScheme.primaryVariant,
                      child: Icon(
                        Icons.edit,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    ),
                    onTap: () {
                      if (isFromSub) {
                        // update from add sub item form
                      } else {
                        // update from add item form
                      }
                    },
                  ),
                  InkWell(
                    child: Container(
                      padding: EdgeInsets.all(Constants.defaultPadding / 4),
                      color: Theme.of(context).colorScheme.primaryVariant,
                      child: Icon(
                        Icons.delete,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            ) : SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
