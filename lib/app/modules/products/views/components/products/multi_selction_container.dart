import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class MultiSelectionContainer extends StatelessWidget {
  final String title;

  MultiSelectionContainer({
    Key? key,
    required this.title,
  }) : super(key: key);

  var items = [
    MultiSelectItem("Value1", "Value 1"),
    MultiSelectItem("Value2", "Value 2"),
    MultiSelectItem("Value3", "Value 3"),
    MultiSelectItem("Value4", "Value 4"),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Constants.defaultPadding / 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
            ),
          ),
          MultiSelectDialogField(
            items: items,
            title: Text(title),
            searchable: true,
            searchIcon: Icon(
              Icons.search,
              size: Constants.iconSize,
              color: Theme.of(context).colorScheme.primary,
            ),
            closeSearchIcon: Icon(
              Icons.close,
              size: Constants.iconSize,
              color: Theme.of(context).colorScheme.primary,
            ),
            selectedColor: Theme.of(context).colorScheme.primary,
            decoration: BoxDecoration(
              color:
                  Theme.of(context).colorScheme.primaryVariant.withOpacity(0.2),
              border: Border.all(
                color: Theme.of(context).colorScheme.primaryVariant,
                width: 1,
              ),
            ),
            buttonIcon: Icon(
              Icons.keyboard_arrow_down,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
            buttonText: Text(
              "Select",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
                fontSize: Constants.sText,
              ),
            ),
            onConfirm: (results) {},
            chipDisplay: MultiSelectChipDisplay(
              onTap: (value) {},
            ),
          ),
        ],
      ),
    );
  }
}
