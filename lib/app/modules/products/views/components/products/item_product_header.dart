import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class ItemProductHeader extends StatelessWidget {
  final bool isComponent;
  final bool isOffer;

  const ItemProductHeader({
    Key? key,
    this.isComponent = false,
    this.isOffer = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Constants.defaultPadding / 2),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Theme.of(context).colorScheme.primaryVariant,
            width: 1,
          ),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: _titleStyle(context, "#"),
          ),
          // Expanded(
          //   flex: 1,
          //   child: _titleStyle(context, "Image"),
          // ),
          Expanded(
            flex: 3,
            child: _titleStyle(context, "Title"),
          ),
          isComponent
              ? Expanded(
                  flex: 3,
                  child: _titleStyle(context, "Components"),
                )
              : SizedBox.shrink(),
          Expanded(
            flex: 1,
            child: _titleStyle(context, "Price"),
          ),
          Expanded(
            flex: 1,
            child: _titleStyle(context, "Visible"),
          ),
          Expanded(
            flex: 1,
            child: _titleStyle(context, "Print Order"),
          ),
          isOffer
              ? Expanded(
                  flex: 1,
                  child: _titleStyle(context, "Offer"),
                )
              : SizedBox.shrink(),
          (!isOffer && !isComponent) ?
          Expanded(
            flex: 2,
            child: _titleStyle(context, "Action"),
          ) : SizedBox.shrink(),
        ],
      ),
    );
  }

  _titleStyle(BuildContext context, String title) {
    return Container(
      // decoration: BoxDecoration(
      //   border: Border(
      //     right: BorderSide(
      //       width: 1,
      //       color: Theme.of(context).colorScheme.primaryVariant,
      //     ),
      //   ),
      // ),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Theme.of(context).colorScheme.onBackground,
          fontWeight: FontWeight.bold,
          fontSize: Constants.sText,
        ),
      ),
    );
  }
}
