import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:flutter/material.dart';

import 'product_dropdown_container.dart';
import 'product_input_container.dart';
import 'product_input_file_container.dart';

class SubProductForm extends StatelessWidget {
  final String title;
  const SubProductForm({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              width: 1,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderTitle(title: title),
              Card(
                child: Column(
                  children: [
                    ProductInputContainer(title: "Display Sort Order"),
                    ProductInputContainer(title: "SubItem/Component Name"),
                    ProductInputContainer(title: "Description", maxLine: 3),
                    ProductInputContainer(title: "URL"),
                    // ProductDropDownContainer(title: "Available on"),
                    ProductInputContainer(title: "Price"),
                    ProductInputFileContainer(),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: Constants.defaultPadding / 2,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: RoundedButton(
                              text: "Clear",
                              padding: Constants.defaultPadding / 2,
                              radius: 0,
                              press: () {},
                            ),
                          ),
                          SizedBox(width: Constants.defaultPadding / 2),
                          Expanded(
                            child: RoundedButton(
                              text: "Save",
                              padding: Constants.defaultPadding / 2,
                              radius: 0,
                              press: () {},
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
