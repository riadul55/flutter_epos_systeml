import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductInputFileContainer extends StatelessWidget {
  final Widget? child;
  final Function()? onTap;

  ProductInputFileContainer({
    Key? key,
    this.child,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Constants.defaultPadding / 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "File",
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
            ),
          ),
          InkWell(
            onTap: onTap,
            child: Container(
              width: Get.width,
              padding: EdgeInsets.all(Constants.defaultPadding / 2),
              decoration: BoxDecoration(
                color: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.2),
                border: Border.all(
                  color: Theme.of(context)
                      .colorScheme
                      .primaryVariant
                      .withOpacity(0.2),
                  width: 1,
                ),
              ),
              child: child ??
                  Icon(
                    Icons.camera_alt,
                    size: Constants.iconSize * 2,
                    color: Theme.of(context).colorScheme.primaryVariant,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
