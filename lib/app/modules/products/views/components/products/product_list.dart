import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:flutter/material.dart';

import 'item_product_header.dart';
import 'item_product_list.dart';

class ProductList extends StatelessWidget {
  final int flex;
  final bool isComponent;
  final bool isOffer;
  final bool isFromSub;
  final List<Product> products;

  const ProductList({
    Key? key,
    required this.flex,
    this.isComponent = false,
    this.isOffer = false,
    this.isFromSub = false,
    required this.products,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Container(
        alignment: Alignment.topLeft,
        child: Column(
          children: [
            ItemProductHeader(
              isComponent: isComponent,
              isOffer: isOffer,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: products.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return ItemProductList(
                    isComponent: isComponent,
                    isOffer: isOffer,
                    isFromSub: isFromSub,
                    product: products[index],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
