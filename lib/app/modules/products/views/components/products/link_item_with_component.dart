import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/globals/rounded_button.dart';
import 'package:epos_system/app/modules/products/views/components/products/product_dropdown_container.dart';
import 'package:epos_system/app/modules/products/views/components/products/product_input_container.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import 'multi_selction_container.dart';

class LinkItemWithComponent extends StatelessWidget {
  const LinkItemWithComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.symmetric(
          horizontal: Constants.defaultPadding * 2,
          vertical: Constants.defaultPadding,
        ),
        child: SingleChildScrollView(
          child: Card(
            elevation: Constants.defaultElevation,
            child: Column(
              children: [
                HeaderTitle(title: "Link Item with Component"),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: Constants.defaultPadding,
                  ),
                  child: Column(
                    children: [
                      // ProductDropDownContainer(title: "EPOS Menu Category"),
                      // ProductDropDownContainer(title: "Choose Item"),
                      // ProductDropDownContainer(title: "SubItem/Component Group"),
                      MultiSelectionContainer(title: "Component"),
                      // ProductDropDownContainer(title: "Relation Type"),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Constants.defaultPadding / 2,
                        ),
                        child: RoundedButton(
                          text: "Create Link",
                          padding: Constants.defaultPadding / 2,
                          radius: 0,
                          press: () {},
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
