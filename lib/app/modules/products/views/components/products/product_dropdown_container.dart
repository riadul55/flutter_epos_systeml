import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/modules/globals/rounded_dropdown_field.dart';
import 'package:flutter/material.dart';

class ProductDropDownContainer extends StatelessWidget {
  final String title;
  final String value;
  final List<String> list;
  final Function(String?) onChanged;
  final bool? isRequired;

  const ProductDropDownContainer({
    Key? key,
    required this.title,
    required this.value,
    required this.list,
    required this.onChanged,
    this.isRequired,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Constants.defaultPadding / 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontSize: Constants.sText,
              fontWeight: FontWeight.bold,
            ),
          ),
          RoundedDropdownField(
            marginVertical: 0,
            hintText: "Select",
            value: value,
            list: list,
            onChanged: onChanged,
            isRequired: isRequired,
          ),
        ],
      ),
    );
  }
}
