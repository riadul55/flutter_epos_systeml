import 'package:epos_system/app/modules/products/controllers/order_create_controller.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/cart_section.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/products_bottom_nav.dart';
import 'package:epos_system/app/modules/products/views/components/order_create/products_section.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderCreateScreen extends GetView<OrderCreateController> {
  const OrderCreateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
          child: Container(
            child: Row(
              children: [
                CartSection(),
                ProductsSection(),
              ],
            ),
          )),
      bottomNavigationBar: ProductsBottomNav(),
    );
  }
}
