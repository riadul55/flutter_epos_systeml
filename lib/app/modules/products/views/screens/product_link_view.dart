import 'package:epos_system/app/modules/globals/header_title.dart';
import 'package:epos_system/app/modules/products/views/components/products/link_item_with_component.dart';
import 'package:epos_system/app/modules/products/views/components/products/link_sub_item_with_itself.dart';
import 'package:epos_system/app/modules/products/views/components/products/products_bottom_bar.dart';
import 'package:flutter/material.dart';

class ProductLinkView extends StatelessWidget {
  const ProductLinkView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            HeaderTitle(title: "Product Link up"),
            Expanded(
              child: Row(
                children: [
                  LinkItemWithComponent(),
                  LinkSubItemWithItself(),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ProductsBottomBar(),
    );
  }
}
