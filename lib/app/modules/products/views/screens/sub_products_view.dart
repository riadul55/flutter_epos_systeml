import 'package:epos_system/app/modules/products/views/components/products/sub_product_form.dart';
import 'package:epos_system/app/modules/products/views/components/products/product_list.dart';
import 'package:epos_system/app/modules/products/views/components/products/products_bottom_bar.dart';
import 'package:flutter/material.dart';

class SubProductsView extends StatelessWidget {
  const SubProductsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              SubProductForm(title: "Sub Items/Components"),
              ProductList(flex: 6, products: [], isFromSub: true,),
            ],
          ),
        ),
      ),
      bottomNavigationBar: ProductsBottomBar(),
    );
  }
}
