import 'package:collection/collection.dart';
import 'package:epos_system/app/core/utilities/constants.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/modules/products/controllers/category_controller.dart';
import 'package:epos_system/app/modules/products/views/components/category/category_bottom_bar.dart';
import 'package:epos_system/app/modules/products/views/components/category/category_form.dart';
import 'package:epos_system/app/modules/products/views/components/category/category_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryView extends GetView<CategoryController> {
  const CategoryView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              StreamBuilder(
                  stream: controller.productRepo.repository.watchAllCategory(),
                  builder: (context,
                      AsyncSnapshot<List<ConfigProductProperty>> snapshot) {
                    controller.getPropertyName();
                    return Obx(() => CategoryList(
                          title: "Category",
                          categories: controller.filterData(snapshot.data ?? [],
                              controller.searchInputValue.value),
                          onUpdate: controller.onEditCategory,
                          onDelete: (ConfigProductProperty item) {
                            _showAlertDialog(context, item);
                          },
                          onSearch: (value) =>
                              controller.searchInputValue(value),
                        ));
                  }),
              CategoryForm(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CategoryBottomBar(),
    );
  }

  _showAlertDialog(BuildContext context, ConfigProductProperty item) async {
    return showDialog(context: context, builder: (context) {
        return AlertDialog(
          title: Text("Warning!", style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: Constants.mText,
          ),),
          content: Text("Are you sure?\nYou want to delete ${item.displayName}!", style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: Constants.mText,
          ),),
          actions: [
            TextButton(
              child: Text('No', style: TextStyle(
                color: Theme.of(context).colorScheme.secondaryVariant,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              )),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Yes', style: TextStyle(
                color: Theme.of(context).colorScheme.primary,
                fontSize: Constants.mText,
                fontWeight: FontWeight.bold,
              )),
              onPressed: () {
                controller.productRepo.repository.deleteCategory(item.id);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      });
  }
}
