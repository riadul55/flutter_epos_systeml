import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/modules/products/views/components/products/product_form.dart';
import 'package:epos_system/app/modules/products/views/components/products/product_list.dart';
import 'package:epos_system/app/modules/products/views/components/products/products_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controllers/main_products_controller.dart';

class MainProductsView extends GetView<MainProductsController> {
  MainProductsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              ProductForm(title: "Add New Product item"),
              StreamBuilder(
                stream: controller.productRepo.repository.watchAllProducts(),
                builder: (BuildContext ctx, AsyncSnapshot<List<Product>> snapshot) {
                  if (snapshot.hasData && snapshot.data != null && snapshot.data!.isNotEmpty) {
                    return ProductList(flex: 6, products: snapshot.data ?? [],);
                  } else {
                    return Expanded(flex: 6, child: Container(
                      child: Center(
                        child: Text("Empty!"),
                      ),
                    ),);
                  }
                },
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: ProductsBottomBar(),
    );
  }
}
