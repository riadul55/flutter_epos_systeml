
import 'dart:io';

import 'package:epos_system/app/core/utilities/custom_img_picker.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/data/local_db/db_injection.dart';
import 'package:epos_system/config/config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';

import '../../../core/utilities/my_storage.dart';
import '../../../data/local_db/data_modules/bindings/product.repository.binding.dart';
import '../../../data/local_db/models/product_entry.model.dart';

class MainProductsController extends GetxController implements ImgPickerMixin{
  final storage = Get.find<MyStorage>();
  late ProductRepositoryBinding productRepo;


  var isUpdatingEnable = false.obs;
  var productIdForUpdate = "".obs;

  var displaySortOrder = TextEditingController();
  var isDisplaySortOrderRequired = false.obs;

  var itemNameInput = TextEditingController();
  var isItemNameInputRequired = false.obs;

  var descriptionInput = TextEditingController();
  var isDescriptionInputRequired = false.obs;

  var urlInput = TextEditingController();
  var isUrlInputRequired = false.obs;

  var filePath = "".obs;

  var dbCategories = <ConfigProductProperty>[].obs;
  var menuCategoryList = <String>[].obs;
  var menuCategoryValue = "".obs;
  var isMenuCategoryRequired = false.obs;

  var sellOnItsOwn = false.obs;

  var availableOptionList = <String>["EPOS", "WEB", "BOTH"];
  var availableOptionValue = "EPOS".obs;

  var reviewOptionList = <String>["Yes", "No"];
  var reviewOptionValue = "Yes".obs;

  var priceInput = TextEditingController();
  var isPriceInputRequired = false.obs;

  var vatInput = TextEditingController();
  var isVatInputRequired = false.obs;

  var invoicePrintOrder = TextEditingController();
  var isInvoicePrintOrderRequired = false.obs;

  var invoicePrintItemOrder = TextEditingController();
  var isInvoicePrintItemOrderRequired = false.obs;

  var offerList = <String>["Yes", "No"];
  var offerValue = "Yes".obs;

  var offerPriceInput = TextEditingController();
  var isOfferPriceInputRequired = false.obs;

  var discountList = <String>["Yes", "No"];
  var discountValue = "Yes".obs;

  late CustomImgPicker imgPicker;

  @override
  void onInit() async {
    imgPicker = CustomImgPicker(this);
    productRepo = ProductRepositoryBinding();
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
    await productRepo.repository.getAllCategories().then((List<ConfigProductProperty> categories) {
      dbCategories(categories);
      var list = categories.map((ConfigProductProperty category) => category.displayName).toList();
      menuCategoryList(list);
      if (list.isNotEmpty) menuCategoryValue(list[0]);
    });
  }

  void onClear() {
    clearInputs();
  }

  void onSubmitUpdate() {
  }

  void onSubmitAdd() async {
    final config = ConfigEnvironments.getEnvironments();
    if (validate()) {
      var productUuid = Uuid().v1();
      List<PropertyEntry> propertyList = [];
      propertyList.add(getPropertyEntry(productUuid, "available_on", availableOptionValue.value));
      propertyList.add(getPropertyEntry(productUuid, "sell_on_its_own", sellOnItsOwn.value ? "1" : "0"));
      propertyList.add(getPropertyEntry(productUuid, "available", "1"));
      propertyList.add(getPropertyEntry(productUuid, "print_order", invoicePrintOrder.text));
      propertyList.add(getPropertyEntry(productUuid, "print_item_component_order", invoicePrintItemOrder.text));
      propertyList.add(getPropertyEntry(productUuid, "offer", offerValue.value == "Yes" ? "1" : "0"));
      propertyList.add(getPropertyEntry(productUuid, "offer_price", offerPriceInput.text));

      ConfigProductProperty category = getCategoryByValue(menuCategoryValue.value);
      propertyList.add(getPropertyEntry(productUuid, "categories_epos", category.value));
      propertyList.add(getPropertyEntry(productUuid, "categories_key", category.keyName));
      propertyList.add(getPropertyEntry(productUuid, "categories", category.value));
      propertyList.add(getPropertyEntry(productUuid, "review_enabled", reviewOptionValue.value == "Yes" ? "1" : "0"));
      propertyList.add(getPropertyEntry(productUuid, "sort_order", displaySortOrder.text));

      var productPrice = priceInput.text.isNotEmpty ? PriceEntry(
        id: Uuid().v1(),
        productUuid: productUuid,
        price: priceInput.text,
        vat: vatInput.text,
        currency: "GBP",
      ) : null;

      var productEntry = ProductEntry(
        id: productUuid,
        shortName: itemNameInput.text,
        productType: "ITEM",
        description: descriptionInput.text,
        status: "APPROVED",
        visible: true,
        discountable: discountValue.value == "Yes",
        providerId: "${config.providerID}",
        language: "${config.language}",
        property: propertyList,
        price: productPrice,
        isSubmitted: false,
      );
      productRepo.repository.addProduct(productEntry).then((value) {
        clearInputs();
      });
    }
  }

  ConfigProductProperty getCategoryByValue(String value) {
    return dbCategories.firstWhere((ConfigProductProperty item) => item.displayName == value);
  }

  getPropertyEntry(productId, key, value) => PropertyEntry(
    id: Uuid().v1(),
    productUuid: productId,
    keyName: key,
    keyValue: value,
  );

  clearInputs() {
    displaySortOrder.clear();
    itemNameInput.clear();
    descriptionInput.clear();
    urlInput.clear();
    priceInput.clear();
    vatInput.clear();
    invoicePrintOrder.clear();
    invoicePrintItemOrder.clear();
    offerPriceInput.clear();

    isUpdatingEnable(false);
    productIdForUpdate("");
  }

  bool validate() {
    if (displaySortOrder.text.isEmpty) {
      isDisplaySortOrderRequired(true);
    }
    if (itemNameInput.text.isEmpty) {
      isItemNameInputRequired(true);
    }
    if (descriptionInput.text.isEmpty) {
      isDescriptionInputRequired(true);
    }
    if (urlInput.text.isEmpty) {
      isUrlInputRequired(true);
    }
    if (priceInput.text.isEmpty) {
      isPriceInputRequired(true);
    }
    if (vatInput.text.isEmpty) {
      isVatInputRequired(true);
    }
    if (invoicePrintOrder.text.isEmpty) {
      isInvoicePrintOrderRequired(true);
    }
    if (invoicePrintItemOrder.text.isEmpty) {
      isInvoicePrintItemOrderRequired(true);
    }
    if (offerPriceInput.text.isEmpty) {
      isOfferPriceInputRequired(true);
    }
    if (menuCategoryValue.value.isEmpty) {
      isMenuCategoryRequired(true);
    }
    return isDisplaySortOrderRequired.value ||
        isItemNameInputRequired.value ||
        isDescriptionInputRequired.value ||
        isUrlInputRequired.value ||
        isPriceInputRequired.value ||
        isVatInputRequired.value ||
        isInvoicePrintOrderRequired.value ||
        isInvoicePrintItemOrderRequired.value ||
        isOfferPriceInputRequired.value ||
        isMenuCategoryRequired.value ? false : true;
  }

  void showImagePicker() {
    imgPicker.getImage(ImageSource.gallery);
  }

  @override
  void onImageSelected(CustomImage image) {
    filePath(image.path);
    print(image.path);
  }

  @override
  void onError(String error) {
    print(error);
  }

  // //profile pic upload
  // var selectedImagePath = "".obs;
  // var selectedImageSize = "".obs;
  //
  // //crop code
  // var cropImagePath = ''.obs;
  // var cropImageSize = ''.obs;
  //
  // //compress code
  // var compressImagePath = ''.obs;
  // var compressImageSize = ''.obs;
  //
  // void getImage(ImageSource imageSource) async {
  //   final pickedFile = await ImagePicker().pickImage(source: imageSource);
  //   if (pickedFile != null) {
  //     selectedImagePath.value = pickedFile.path;
  //     selectedImageSize.value =
  //         ((File(selectedImagePath.value)).lengthSync() / 1024 / 1024)
  //             .toStringAsFixed(2) +
  //             " Mb";
  //
  //     //Crop
  //     final cropImageFile = await ImageCropper.cropImage(
  //       sourcePath: selectedImagePath.value,
  //       maxWidth: 512,
  //       maxHeight: 512,
  //       compressFormat: ImageCompressFormat.jpg,
  //       // aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
  //     );
  //     cropImagePath.value = cropImageFile!.path;
  //     cropImageSize.value = ((File(cropImagePath.value)).lengthSync() / 1024 / 1024).toStringAsFixed(2) + " Mb";
  //
  //     // Compress
  //     final dir = await Directory.systemTemp;
  //     final targetPath = dir.absolute.path + "/temp.jpg";
  //     var compressedFile = await FlutterImageCompress.compressAndGetFile(
  //         cropImagePath.value,
  //         targetPath, quality: 90);
  //     compressImagePath.value = compressedFile!.path;
  //     compressImageSize.value =
  //         ((File(compressImagePath.value)).lengthSync() / 1024 / 1024)
  //             .toStringAsFixed(2) +
  //             " Mb";
  //
  //     // uploadImage(compressedFile);
  //   } else {
  //     // showErrorToast(msg: "No image selected");
  //   }
  // }
}