import 'dart:isolate';

import 'package:collection/collection.dart';
import 'package:drift/drift.dart' as drift;
import 'package:epos_system/app/core/utilities/custom_img_picker.dart';
import 'package:epos_system/app/core/utilities/my_storage.dart';
import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/data/local_db/db_injection.dart';
import 'package:epos_system/app/modules/globals/global_widgets.dart';
import 'package:epos_system/config/config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';

import 'package:easy_isolate/easy_isolate.dart' as iso;

import '../../../data/local_db/data_modules/bindings/product.repository.binding.dart';

class CategoryController extends GetxController implements ImgPickerMixin {
  final storage = Get.find<MyStorage>();
  late ProductRepositoryBinding productRepo;

  // final worker = iso.Worker();

  late CustomImgPicker imgPicker;

  var categoryList = <ConfigProductProperty>[].obs;
  var selectedCategoryIndex = 0.obs;

  ConfigProductProperty get selectedCategory => categoryList.isNotEmpty
      ? categoryList.value[selectedCategoryIndex.value]
      : ConfigProductProperty(
          id: "",
          platform: "",
          keyName: "New",
          value: "",
          sortOrder: -1,
          displayName: "",
          description: "",
          contentType: '',
          filename: '',
    isSubmitted: false,
  );

  int getCategoryIndex(value) =>
      categoryList.value.indexWhere((element) => element.keyName == value);

  get getPropertyNames =>
      groupBy(categoryList.value, (ConfigProductProperty item) => item.keyName)
          .keys
          .map((String e) => e)
          .toList();

  var isUpdatingEnable = false.obs;
  var categoryIdForUpdate = "".obs;

  var propertyNameInput = TextEditingController();
  var isPropertyNameRequired = false.obs;

  get propertyNameInputExist => selectedCategoryIndex.value == 0;

  var sortOrderInput = TextEditingController();
  var isSortOrderRequired = false.obs;

  var menuCatNameInput = TextEditingController();
  var isMenuCatNameRequired = false.obs;

  var descriptionInput = TextEditingController();
  var isDescriptionRequired = false.obs;

  var filePath = "".obs;

  var searchInputValue = "".obs;

  @override
  void onInit() async {
    imgPicker = CustomImgPicker(this);
    productRepo = ProductRepositoryBinding();
    // await worker.init(
    //   onMessage,
    //   isolatedProcess,
    //   errorHandler: print,
    // );
    super.onInit();
  }

  // static isolatedProcess(dynamic data, SendPort mainSendPort, iso.SendErrorFunction sendError) async {
  //   print(data);
  //   await Future.delayed(Duration(seconds: 5));
  //   mainSendPort.send("loading finished");
  // }
  //
  // void onMessage(dynamic data, SendPort isolateSendPort) {
  //   print(data);
  // }

  @override
  void onReady() {
    super.onReady();
    getPropertyName();
  }

  void onEditCategory(ConfigProductProperty item) {
    isUpdatingEnable(true);
    categoryIdForUpdate(item.id);
    selectedCategoryIndex(getCategoryIndex(item.keyName));
    propertyNameInput.text = item.keyName;
    sortOrderInput.text = item.sortOrder.toString();
    menuCatNameInput.text = item.displayName;
    descriptionInput.text = item.description;
    filePath(item.filename);
  }

  void onSubmitClear() {
    isUpdatingEnable(false);
    categoryIdForUpdate("");
    selectedCategoryIndex(0);
    propertyNameInput.clear();
    sortOrderInput.clear();
    menuCatNameInput.clear();
    descriptionInput.clear();
    filePath("");
  }

  void onSubmitUpdate() {
    if (isValid()) {
      productRepo.repository.updateCategory(TbConfigProductPropertyCompanion(
        id: drift.Value(categoryIdForUpdate.value),
        platform: drift.Value("epos"),
        keyName: propertyNameInputExist
            ? drift.Value(propertyNameInput.text)
            : drift.Value(selectedCategory.keyName),
        value: drift.Value(menuCatNameInput.text.trim().toLowerCase()),
        sortOrder: drift.Value(int.parse(sortOrderInput.text)),
        displayName: drift.Value(menuCatNameInput.text),
        description: drift.Value(descriptionInput.text),
        filename: drift.Value(""),
        contentType: drift.Value(""),
        isSubmitted: drift.Value(false)
      ));
      onSubmitClear();
    }
  }

  void onSubmitAdd() async {
    var uuid = Uuid();
    if (isValid()) {
      productRepo.repository.addCategory(TbConfigProductPropertyCompanion(
        id: drift.Value(uuid.v1()),
        platform: drift.Value("epos"),
        keyName: propertyNameInputExist
            ? drift.Value(propertyNameInput.text)
            : drift.Value(selectedCategory.keyName),
        value: drift.Value(""),
        sortOrder: drift.Value(int.parse(sortOrderInput.text)),
        displayName: drift.Value(menuCatNameInput.text),
        description: drift.Value(descriptionInput.text),
        filename: drift.Value(""),
        contentType: drift.Value(""),
          isSubmitted: drift.Value(false),
      ));

      // if (storage.appConfig.val[StorageKey.CONFIG_APP_MODE] == "Online") {
      //   worker.sendMessage("submit");
      // }

      getPropertyName();
      onSubmitClear();
    }
  }

  bool isValid() {
    if (propertyNameInputExist) {
      if (propertyNameInput.text.isEmpty) {
        isPropertyNameRequired(true);
        return false;
      } else {
        if (categoryList
            .where((ConfigProductProperty item) =>
                item.keyName == propertyNameInput.text)
            .isNotEmpty) {
          showToast(
              title: "Oops!",
              message: "This property name already exist!!",
              textColor: Colors.white,
              bgColor: Colors.red);
          isPropertyNameRequired(true);
          return false;
        }
      }
    }

    if (sortOrderInput.text.isEmpty) {
      isSortOrderRequired(true);
    }
    if (menuCatNameInput.text.isEmpty) {
      isMenuCatNameRequired(true);
    }
    if (descriptionInput.text.isEmpty) {
      isDescriptionRequired(true);
    }

    if (isSortOrderRequired.value ||
        isMenuCatNameRequired.value ||
        isDescriptionRequired.value
    ) {
      return false;
    } else {
      return true;
    }
  }

  getPropertyName() async {
    List<ConfigProductProperty> newCatList = [];
    newCatList.add(ConfigProductProperty(
        id: "",
        platform: "",
        keyName: "New",
        value: "",
        sortOrder: -1,
        displayName: "",
        description: "",
        contentType: '',
        filename: '',
      isSubmitted: false,
    ));
    List<ConfigProductProperty> categories =
        await productRepo.repository.getAllCategories();
    for (var item in categories) {
      newCatList.add(item);
    }
    categoryList(newCatList);
  }

  List<ConfigProductProperty> filterData(
      List<ConfigProductProperty> datas, String value) {
    if (value.isEmpty) {
      return datas;
    } else {
      List<ConfigProductProperty> newPropertyList = <ConfigProductProperty>[];
      for (ConfigProductProperty element in datas) {
        if (element.keyName.toLowerCase().contains(value.toLowerCase()) ||
            element.value.toLowerCase().contains(value.toLowerCase()) ||
            element.displayName.toLowerCase().contains(value.toLowerCase()) ||
            element.sortOrder
                .toString()
                .toLowerCase()
                .contains(value.toLowerCase())) {
          newPropertyList.add(element);
        }
      }
      return newPropertyList;
    }
  }

  // image picker
  void showImagePicker() {
    imgPicker.getImage(ImageSource.gallery);
  }

  @override
  void onImageSelected(CustomImage image) {
    filePath(image.path);
    print(image.path);
  }

  @override
  void onError(String error) {
    print(error);
  }
}
