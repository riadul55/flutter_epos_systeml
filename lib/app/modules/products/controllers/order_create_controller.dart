import 'package:collection/collection.dart';
import 'package:epos_system/app/core/utilities/my_storage.dart';
import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/local_db/app_db.dart';
import '../../../data/local_db/data_modules/bindings/product.repository.binding.dart';
import '../../../data/remote_db/bindings/product.repository.binding.dart' as Remote;

class OrderCreateController extends GetxController with GetSingleTickerProviderStateMixin {
  final storage = Get.find<MyStorage>();

  late TabController mainCategoryController;
  late ProductRepositoryBinding productRepo;
  late Remote.ProductRepositoryBinding productRepoRemote;
  var tabLength = 0.obs;

  var isShowSubItemScreen = false.obs;
  // var isProductHasSubItem = false.obs;

  var selectedMainProductIndexList = <String, List<int>>{}.obs;
  var selectedSubProductIndexList = <int>[].obs;

  @override
  void onInit() async {
    productRepo = ProductRepositoryBinding();
    productRepoRemote = Remote.ProductRepositoryBinding();
    super.onInit();

    once(tabLength, (callback) {
      mainCategoryController = TabController(
          length: tabLength.value, vsync: this, initialIndex: 0);
    });
  }

  @override
  void onReady() {
    super.onReady();
  }
  @override
  void onClose() {
    mainCategoryController.dispose();
    super.onClose();
  }

  Future<List<ConfigProductProperty>> getAllCategories() async {
    var allCategories = await productRepo.repository.getAllCategories();
    if (allCategories.isNotEmpty) {
      return allCategories;
    } else {
      await productRepoRemote.repository.getCategories();
      await productRepoRemote.repository.getProducts();
      return await productRepo.repository.getAllCategories();
    }
  }

  Map<String, dynamic> getCategoriesGroupByKeyName(List<ConfigProductProperty> categories) {
    Map<String, dynamic> newGroupList = groupBy(categories, (ConfigProductProperty obj) => obj.keyName);
    tabLength(newGroupList.keys.length);
    return newGroupList;
  }
}