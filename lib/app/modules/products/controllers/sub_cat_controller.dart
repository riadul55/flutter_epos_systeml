import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/local_db/app_db.dart';
import '../../../data/local_db/data_modules/bindings/product.repository.binding.dart';

class SubCatController extends GetxController with GetSingleTickerProviderStateMixin {

  var selectedCategoryIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }
}