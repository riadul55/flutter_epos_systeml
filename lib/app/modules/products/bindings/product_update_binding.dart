import 'package:get/get.dart';

import '../controllers/main_products_controller.dart';

class ProductUpdateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainProductsController>(() => MainProductsController());
  }

}