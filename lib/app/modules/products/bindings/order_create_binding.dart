import 'package:epos_system/app/modules/products/controllers/order_create_controller.dart';
import 'package:get/get.dart';

class OrderCreateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrderCreateController>(
          () => OrderCreateController(),
    );
  }
}