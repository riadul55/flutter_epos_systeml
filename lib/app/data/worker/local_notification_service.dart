import 'package:firebase_messaging/firebase_messaging.dart';

class LocalNotificationService {
  static final String CHANNEL_KEY = "epos";

  static void display(RemoteMessage message) async {
    print("From firebase =====> ${message.notification!.title}");
    print("From firebase =====> ${message.notification!.body}");
  }
}