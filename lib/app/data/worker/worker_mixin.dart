import 'dart:isolate';

import 'package:easy_isolate/easy_isolate.dart';

abstract class WorkerMixin {
  /// Handle the messages coming from the main
   void isolatedProcess(dynamic data, SendPort mainSendPort, SendErrorFunction sendError);
  /// Handle the messages coming from the isolate
  void onMessage(dynamic data, SendPort isolateSendPort);
}