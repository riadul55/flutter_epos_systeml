import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:drift/drift.dart';
import 'package:drift/isolate.dart';
import 'package:drift/native.dart';
import 'package:easy_isolate/easy_isolate.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/data/local_db/db_injection.dart';
import 'package:path_provider/path_provider.dart' as paths;
import 'package:path/path.dart' as p;

class IsolateWorker {
  final String event;
  final Function(dynamic event) onListen;

  IsolateWorker({
    required this.event,
    required this.onListen,
  });

  final worker = Worker();

  /// Initiate the worker (new thread) and start listen from messages between
  /// the threads
  Future<void> init() async {
    await worker.init(
      onMessage,
      isolatedProcess,
      errorHandler: print,
    );
    worker.sendMessage(MainEvent(event));
  }

  static isolatedProcess(
      dynamic data, SendPort mainSendPort, SendErrorFunction sendError) async {
    // final executor = LazyDatabase(() async {
    //   final dataDir = await paths.getApplicationDocumentsDirectory();
    //   final dbFile = File(p.join(dataDir.path, 'epos-db.sqlite'));
    //   return NativeDatabase(dbFile, logStatements: true);
    // });
    // final driftIsolate = DriftIsolate.inCurrent(
    //         () => DatabaseConnection.fromExecutor(executor));
    // DatabaseConnection  connect = await driftIsolate.connect();
    // AppDb appDb = AppDb.connect(connect);
    // // AppDb appDb = openConnection();
    // appDb.testDao.watchAllTests().listen((List<Test> tests) {
    //   print("watching...");
    // });
    if (data is MainEvent && data.item == "progress") {
      final fragmentTime = 1 / 200;
      double progress = 0;
      Timer.periodic(
        Duration(seconds: 1),
        (timer) {
          if (progress < 1) {
            progress += fragmentTime;
            mainSendPort.send(ProcessEvent("$progress"));
          } else {
            timer.cancel();
          }
        },
      );
    } else {
      mainSendPort.send(ProcessEvent(data.item));
    }
  }


  void onMessage(dynamic data, SendPort isolateSendPort) {
    if (data is ProcessEvent) {
      onListen(data.process);
    }
  }
}

class MainEvent {
  final String item;

  MainEvent(this.item);
}

class ProcessEvent {
  final String process;

  ProcessEvent(this.process);
}