// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: type=lint
class Product extends DataClass implements Insertable<Product> {
  final String id;
  final String shortName;
  final String productType;
  final String description;
  final String status;
  final bool visible;
  final bool discountable;
  final String creatorProviderUuid;
  final String language;
  final bool isSubmitted;
  Product(
      {required this.id,
      required this.shortName,
      required this.productType,
      required this.description,
      required this.status,
      required this.visible,
      required this.discountable,
      required this.creatorProviderUuid,
      required this.language,
      required this.isSubmitted});
  factory Product.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Product(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      shortName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}short_name'])!,
      productType: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_type'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      status: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}status'])!,
      visible: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}visible'])!,
      discountable: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}discountable'])!,
      creatorProviderUuid: const StringType().mapFromDatabaseResponse(
          data['${effectivePrefix}creator_provider_uuid'])!,
      language: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}language'])!,
      isSubmitted: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}isSubmitted'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['short_name'] = Variable<String>(shortName);
    map['product_type'] = Variable<String>(productType);
    map['description'] = Variable<String>(description);
    map['status'] = Variable<String>(status);
    map['visible'] = Variable<bool>(visible);
    map['discountable'] = Variable<bool>(discountable);
    map['creator_provider_uuid'] = Variable<String>(creatorProviderUuid);
    map['language'] = Variable<String>(language);
    map['isSubmitted'] = Variable<bool>(isSubmitted);
    return map;
  }

  TbProductCompanion toCompanion(bool nullToAbsent) {
    return TbProductCompanion(
      id: Value(id),
      shortName: Value(shortName),
      productType: Value(productType),
      description: Value(description),
      status: Value(status),
      visible: Value(visible),
      discountable: Value(discountable),
      creatorProviderUuid: Value(creatorProviderUuid),
      language: Value(language),
      isSubmitted: Value(isSubmitted),
    );
  }

  factory Product.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Product(
      id: serializer.fromJson<String>(json['id']),
      shortName: serializer.fromJson<String>(json['shortName']),
      productType: serializer.fromJson<String>(json['productType']),
      description: serializer.fromJson<String>(json['description']),
      status: serializer.fromJson<String>(json['status']),
      visible: serializer.fromJson<bool>(json['visible']),
      discountable: serializer.fromJson<bool>(json['discountable']),
      creatorProviderUuid:
          serializer.fromJson<String>(json['creatorProviderUuid']),
      language: serializer.fromJson<String>(json['language']),
      isSubmitted: serializer.fromJson<bool>(json['isSubmitted']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'shortName': serializer.toJson<String>(shortName),
      'productType': serializer.toJson<String>(productType),
      'description': serializer.toJson<String>(description),
      'status': serializer.toJson<String>(status),
      'visible': serializer.toJson<bool>(visible),
      'discountable': serializer.toJson<bool>(discountable),
      'creatorProviderUuid': serializer.toJson<String>(creatorProviderUuid),
      'language': serializer.toJson<String>(language),
      'isSubmitted': serializer.toJson<bool>(isSubmitted),
    };
  }

  Product copyWith(
          {String? id,
          String? shortName,
          String? productType,
          String? description,
          String? status,
          bool? visible,
          bool? discountable,
          String? creatorProviderUuid,
          String? language,
          bool? isSubmitted}) =>
      Product(
        id: id ?? this.id,
        shortName: shortName ?? this.shortName,
        productType: productType ?? this.productType,
        description: description ?? this.description,
        status: status ?? this.status,
        visible: visible ?? this.visible,
        discountable: discountable ?? this.discountable,
        creatorProviderUuid: creatorProviderUuid ?? this.creatorProviderUuid,
        language: language ?? this.language,
        isSubmitted: isSubmitted ?? this.isSubmitted,
      );
  @override
  String toString() {
    return (StringBuffer('Product(')
          ..write('id: $id, ')
          ..write('shortName: $shortName, ')
          ..write('productType: $productType, ')
          ..write('description: $description, ')
          ..write('status: $status, ')
          ..write('visible: $visible, ')
          ..write('discountable: $discountable, ')
          ..write('creatorProviderUuid: $creatorProviderUuid, ')
          ..write('language: $language, ')
          ..write('isSubmitted: $isSubmitted')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      shortName,
      productType,
      description,
      status,
      visible,
      discountable,
      creatorProviderUuid,
      language,
      isSubmitted);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Product &&
          other.id == this.id &&
          other.shortName == this.shortName &&
          other.productType == this.productType &&
          other.description == this.description &&
          other.status == this.status &&
          other.visible == this.visible &&
          other.discountable == this.discountable &&
          other.creatorProviderUuid == this.creatorProviderUuid &&
          other.language == this.language &&
          other.isSubmitted == this.isSubmitted);
}

class TbProductCompanion extends UpdateCompanion<Product> {
  final Value<String> id;
  final Value<String> shortName;
  final Value<String> productType;
  final Value<String> description;
  final Value<String> status;
  final Value<bool> visible;
  final Value<bool> discountable;
  final Value<String> creatorProviderUuid;
  final Value<String> language;
  final Value<bool> isSubmitted;
  const TbProductCompanion({
    this.id = const Value.absent(),
    this.shortName = const Value.absent(),
    this.productType = const Value.absent(),
    this.description = const Value.absent(),
    this.status = const Value.absent(),
    this.visible = const Value.absent(),
    this.discountable = const Value.absent(),
    this.creatorProviderUuid = const Value.absent(),
    this.language = const Value.absent(),
    this.isSubmitted = const Value.absent(),
  });
  TbProductCompanion.insert({
    required String id,
    required String shortName,
    required String productType,
    required String description,
    required String status,
    required bool visible,
    required bool discountable,
    required String creatorProviderUuid,
    required String language,
    required bool isSubmitted,
  })  : id = Value(id),
        shortName = Value(shortName),
        productType = Value(productType),
        description = Value(description),
        status = Value(status),
        visible = Value(visible),
        discountable = Value(discountable),
        creatorProviderUuid = Value(creatorProviderUuid),
        language = Value(language),
        isSubmitted = Value(isSubmitted);
  static Insertable<Product> custom({
    Expression<String>? id,
    Expression<String>? shortName,
    Expression<String>? productType,
    Expression<String>? description,
    Expression<String>? status,
    Expression<bool>? visible,
    Expression<bool>? discountable,
    Expression<String>? creatorProviderUuid,
    Expression<String>? language,
    Expression<bool>? isSubmitted,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (shortName != null) 'short_name': shortName,
      if (productType != null) 'product_type': productType,
      if (description != null) 'description': description,
      if (status != null) 'status': status,
      if (visible != null) 'visible': visible,
      if (discountable != null) 'discountable': discountable,
      if (creatorProviderUuid != null)
        'creator_provider_uuid': creatorProviderUuid,
      if (language != null) 'language': language,
      if (isSubmitted != null) 'isSubmitted': isSubmitted,
    });
  }

  TbProductCompanion copyWith(
      {Value<String>? id,
      Value<String>? shortName,
      Value<String>? productType,
      Value<String>? description,
      Value<String>? status,
      Value<bool>? visible,
      Value<bool>? discountable,
      Value<String>? creatorProviderUuid,
      Value<String>? language,
      Value<bool>? isSubmitted}) {
    return TbProductCompanion(
      id: id ?? this.id,
      shortName: shortName ?? this.shortName,
      productType: productType ?? this.productType,
      description: description ?? this.description,
      status: status ?? this.status,
      visible: visible ?? this.visible,
      discountable: discountable ?? this.discountable,
      creatorProviderUuid: creatorProviderUuid ?? this.creatorProviderUuid,
      language: language ?? this.language,
      isSubmitted: isSubmitted ?? this.isSubmitted,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (shortName.present) {
      map['short_name'] = Variable<String>(shortName.value);
    }
    if (productType.present) {
      map['product_type'] = Variable<String>(productType.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (status.present) {
      map['status'] = Variable<String>(status.value);
    }
    if (visible.present) {
      map['visible'] = Variable<bool>(visible.value);
    }
    if (discountable.present) {
      map['discountable'] = Variable<bool>(discountable.value);
    }
    if (creatorProviderUuid.present) {
      map['creator_provider_uuid'] =
          Variable<String>(creatorProviderUuid.value);
    }
    if (language.present) {
      map['language'] = Variable<String>(language.value);
    }
    if (isSubmitted.present) {
      map['isSubmitted'] = Variable<bool>(isSubmitted.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbProductCompanion(')
          ..write('id: $id, ')
          ..write('shortName: $shortName, ')
          ..write('productType: $productType, ')
          ..write('description: $description, ')
          ..write('status: $status, ')
          ..write('visible: $visible, ')
          ..write('discountable: $discountable, ')
          ..write('creatorProviderUuid: $creatorProviderUuid, ')
          ..write('language: $language, ')
          ..write('isSubmitted: $isSubmitted')
          ..write(')'))
        .toString();
  }
}

class $TbProductTable extends TbProduct
    with TableInfo<$TbProductTable, Product> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbProductTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _shortNameMeta = const VerificationMeta('shortName');
  @override
  late final GeneratedColumn<String?> shortName = GeneratedColumn<String?>(
      'short_name', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 100),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _productTypeMeta =
      const VerificationMeta('productType');
  @override
  late final GeneratedColumn<String?> productType = GeneratedColumn<String?>(
      'product_type', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 45),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _statusMeta = const VerificationMeta('status');
  @override
  late final GeneratedColumn<String?> status = GeneratedColumn<String?>(
      'status', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 10),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _visibleMeta = const VerificationMeta('visible');
  @override
  late final GeneratedColumn<bool?> visible = GeneratedColumn<bool?>(
      'visible', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: true,
      defaultConstraints: 'CHECK (visible IN (0, 1))');
  final VerificationMeta _discountableMeta =
      const VerificationMeta('discountable');
  @override
  late final GeneratedColumn<bool?> discountable = GeneratedColumn<bool?>(
      'discountable', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: true,
      defaultConstraints: 'CHECK (discountable IN (0, 1))');
  final VerificationMeta _creatorProviderUuidMeta =
      const VerificationMeta('creatorProviderUuid');
  @override
  late final GeneratedColumn<String?> creatorProviderUuid =
      GeneratedColumn<String?>('creator_provider_uuid', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
              minTextLength: 1, maxTextLength: 50),
          type: const StringType(),
          requiredDuringInsert: true);
  final VerificationMeta _languageMeta = const VerificationMeta('language');
  @override
  late final GeneratedColumn<String?> language = GeneratedColumn<String?>(
      'language', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 5),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _isSubmittedMeta =
      const VerificationMeta('isSubmitted');
  @override
  late final GeneratedColumn<bool?> isSubmitted = GeneratedColumn<bool?>(
      'isSubmitted', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: true,
      defaultConstraints: 'CHECK (isSubmitted IN (0, 1))');
  @override
  List<GeneratedColumn> get $columns => [
        id,
        shortName,
        productType,
        description,
        status,
        visible,
        discountable,
        creatorProviderUuid,
        language,
        isSubmitted
      ];
  @override
  String get aliasedName => _alias ?? 'product';
  @override
  String get actualTableName => 'product';
  @override
  VerificationContext validateIntegrity(Insertable<Product> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('short_name')) {
      context.handle(_shortNameMeta,
          shortName.isAcceptableOrUnknown(data['short_name']!, _shortNameMeta));
    } else if (isInserting) {
      context.missing(_shortNameMeta);
    }
    if (data.containsKey('product_type')) {
      context.handle(
          _productTypeMeta,
          productType.isAcceptableOrUnknown(
              data['product_type']!, _productTypeMeta));
    } else if (isInserting) {
      context.missing(_productTypeMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status']!, _statusMeta));
    } else if (isInserting) {
      context.missing(_statusMeta);
    }
    if (data.containsKey('visible')) {
      context.handle(_visibleMeta,
          visible.isAcceptableOrUnknown(data['visible']!, _visibleMeta));
    } else if (isInserting) {
      context.missing(_visibleMeta);
    }
    if (data.containsKey('discountable')) {
      context.handle(
          _discountableMeta,
          discountable.isAcceptableOrUnknown(
              data['discountable']!, _discountableMeta));
    } else if (isInserting) {
      context.missing(_discountableMeta);
    }
    if (data.containsKey('creator_provider_uuid')) {
      context.handle(
          _creatorProviderUuidMeta,
          creatorProviderUuid.isAcceptableOrUnknown(
              data['creator_provider_uuid']!, _creatorProviderUuidMeta));
    } else if (isInserting) {
      context.missing(_creatorProviderUuidMeta);
    }
    if (data.containsKey('language')) {
      context.handle(_languageMeta,
          language.isAcceptableOrUnknown(data['language']!, _languageMeta));
    } else if (isInserting) {
      context.missing(_languageMeta);
    }
    if (data.containsKey('isSubmitted')) {
      context.handle(
          _isSubmittedMeta,
          isSubmitted.isAcceptableOrUnknown(
              data['isSubmitted']!, _isSubmittedMeta));
    } else if (isInserting) {
      context.missing(_isSubmittedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Product map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Product.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbProductTable createAlias(String alias) {
    return $TbProductTable(attachedDatabase, alias);
  }
}

class ProductProperty extends DataClass implements Insertable<ProductProperty> {
  final String id;
  final String productUuid;
  final String keyName;
  final String keyValue;
  ProductProperty(
      {required this.id,
      required this.productUuid,
      required this.keyName,
      required this.keyValue});
  factory ProductProperty.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return ProductProperty(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      productUuid: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_uuid'])!,
      keyName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}key_name'])!,
      keyValue: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}key_value'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['product_uuid'] = Variable<String>(productUuid);
    map['key_name'] = Variable<String>(keyName);
    map['key_value'] = Variable<String>(keyValue);
    return map;
  }

  TbProductPropertyCompanion toCompanion(bool nullToAbsent) {
    return TbProductPropertyCompanion(
      id: Value(id),
      productUuid: Value(productUuid),
      keyName: Value(keyName),
      keyValue: Value(keyValue),
    );
  }

  factory ProductProperty.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ProductProperty(
      id: serializer.fromJson<String>(json['id']),
      productUuid: serializer.fromJson<String>(json['productUuid']),
      keyName: serializer.fromJson<String>(json['keyName']),
      keyValue: serializer.fromJson<String>(json['keyValue']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'productUuid': serializer.toJson<String>(productUuid),
      'keyName': serializer.toJson<String>(keyName),
      'keyValue': serializer.toJson<String>(keyValue),
    };
  }

  ProductProperty copyWith(
          {String? id,
          String? productUuid,
          String? keyName,
          String? keyValue}) =>
      ProductProperty(
        id: id ?? this.id,
        productUuid: productUuid ?? this.productUuid,
        keyName: keyName ?? this.keyName,
        keyValue: keyValue ?? this.keyValue,
      );
  @override
  String toString() {
    return (StringBuffer('ProductProperty(')
          ..write('id: $id, ')
          ..write('productUuid: $productUuid, ')
          ..write('keyName: $keyName, ')
          ..write('keyValue: $keyValue')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, productUuid, keyName, keyValue);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductProperty &&
          other.id == this.id &&
          other.productUuid == this.productUuid &&
          other.keyName == this.keyName &&
          other.keyValue == this.keyValue);
}

class TbProductPropertyCompanion extends UpdateCompanion<ProductProperty> {
  final Value<String> id;
  final Value<String> productUuid;
  final Value<String> keyName;
  final Value<String> keyValue;
  const TbProductPropertyCompanion({
    this.id = const Value.absent(),
    this.productUuid = const Value.absent(),
    this.keyName = const Value.absent(),
    this.keyValue = const Value.absent(),
  });
  TbProductPropertyCompanion.insert({
    required String id,
    required String productUuid,
    required String keyName,
    required String keyValue,
  })  : id = Value(id),
        productUuid = Value(productUuid),
        keyName = Value(keyName),
        keyValue = Value(keyValue);
  static Insertable<ProductProperty> custom({
    Expression<String>? id,
    Expression<String>? productUuid,
    Expression<String>? keyName,
    Expression<String>? keyValue,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (productUuid != null) 'product_uuid': productUuid,
      if (keyName != null) 'key_name': keyName,
      if (keyValue != null) 'key_value': keyValue,
    });
  }

  TbProductPropertyCompanion copyWith(
      {Value<String>? id,
      Value<String>? productUuid,
      Value<String>? keyName,
      Value<String>? keyValue}) {
    return TbProductPropertyCompanion(
      id: id ?? this.id,
      productUuid: productUuid ?? this.productUuid,
      keyName: keyName ?? this.keyName,
      keyValue: keyValue ?? this.keyValue,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (productUuid.present) {
      map['product_uuid'] = Variable<String>(productUuid.value);
    }
    if (keyName.present) {
      map['key_name'] = Variable<String>(keyName.value);
    }
    if (keyValue.present) {
      map['key_value'] = Variable<String>(keyValue.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbProductPropertyCompanion(')
          ..write('id: $id, ')
          ..write('productUuid: $productUuid, ')
          ..write('keyName: $keyName, ')
          ..write('keyValue: $keyValue')
          ..write(')'))
        .toString();
  }
}

class $TbProductPropertyTable extends TbProductProperty
    with TableInfo<$TbProductPropertyTable, ProductProperty> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbProductPropertyTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _productUuidMeta =
      const VerificationMeta('productUuid');
  @override
  late final GeneratedColumn<String?> productUuid = GeneratedColumn<String?>(
      'product_uuid', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _keyNameMeta = const VerificationMeta('keyName');
  @override
  late final GeneratedColumn<String?> keyName = GeneratedColumn<String?>(
      'key_name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _keyValueMeta = const VerificationMeta('keyValue');
  @override
  late final GeneratedColumn<String?> keyValue = GeneratedColumn<String?>(
      'key_value', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, productUuid, keyName, keyValue];
  @override
  String get aliasedName => _alias ?? 'product_property';
  @override
  String get actualTableName => 'product_property';
  @override
  VerificationContext validateIntegrity(Insertable<ProductProperty> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('product_uuid')) {
      context.handle(
          _productUuidMeta,
          productUuid.isAcceptableOrUnknown(
              data['product_uuid']!, _productUuidMeta));
    } else if (isInserting) {
      context.missing(_productUuidMeta);
    }
    if (data.containsKey('key_name')) {
      context.handle(_keyNameMeta,
          keyName.isAcceptableOrUnknown(data['key_name']!, _keyNameMeta));
    } else if (isInserting) {
      context.missing(_keyNameMeta);
    }
    if (data.containsKey('key_value')) {
      context.handle(_keyValueMeta,
          keyValue.isAcceptableOrUnknown(data['key_value']!, _keyValueMeta));
    } else if (isInserting) {
      context.missing(_keyValueMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  ProductProperty map(Map<String, dynamic> data, {String? tablePrefix}) {
    return ProductProperty.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbProductPropertyTable createAlias(String alias) {
    return $TbProductPropertyTable(attachedDatabase, alias);
  }
}

class ProductPrice extends DataClass implements Insertable<ProductPrice> {
  final String id;
  final String productUuid;
  final String? price;
  final String? extraPrice;
  final String vat;
  final String currency;
  ProductPrice(
      {required this.id,
      required this.productUuid,
      this.price,
      this.extraPrice,
      required this.vat,
      required this.currency});
  factory ProductPrice.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return ProductPrice(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      productUuid: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_uuid'])!,
      price: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}price']),
      extraPrice: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}extra_price']),
      vat: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}vat'])!,
      currency: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}currency'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['product_uuid'] = Variable<String>(productUuid);
    if (!nullToAbsent || price != null) {
      map['price'] = Variable<String?>(price);
    }
    if (!nullToAbsent || extraPrice != null) {
      map['extra_price'] = Variable<String?>(extraPrice);
    }
    map['vat'] = Variable<String>(vat);
    map['currency'] = Variable<String>(currency);
    return map;
  }

  TbProductPriceCompanion toCompanion(bool nullToAbsent) {
    return TbProductPriceCompanion(
      id: Value(id),
      productUuid: Value(productUuid),
      price:
          price == null && nullToAbsent ? const Value.absent() : Value(price),
      extraPrice: extraPrice == null && nullToAbsent
          ? const Value.absent()
          : Value(extraPrice),
      vat: Value(vat),
      currency: Value(currency),
    );
  }

  factory ProductPrice.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ProductPrice(
      id: serializer.fromJson<String>(json['id']),
      productUuid: serializer.fromJson<String>(json['productUuid']),
      price: serializer.fromJson<String?>(json['price']),
      extraPrice: serializer.fromJson<String?>(json['extraPrice']),
      vat: serializer.fromJson<String>(json['vat']),
      currency: serializer.fromJson<String>(json['currency']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'productUuid': serializer.toJson<String>(productUuid),
      'price': serializer.toJson<String?>(price),
      'extraPrice': serializer.toJson<String?>(extraPrice),
      'vat': serializer.toJson<String>(vat),
      'currency': serializer.toJson<String>(currency),
    };
  }

  ProductPrice copyWith(
          {String? id,
          String? productUuid,
          String? price,
          String? extraPrice,
          String? vat,
          String? currency}) =>
      ProductPrice(
        id: id ?? this.id,
        productUuid: productUuid ?? this.productUuid,
        price: price ?? this.price,
        extraPrice: extraPrice ?? this.extraPrice,
        vat: vat ?? this.vat,
        currency: currency ?? this.currency,
      );
  @override
  String toString() {
    return (StringBuffer('ProductPrice(')
          ..write('id: $id, ')
          ..write('productUuid: $productUuid, ')
          ..write('price: $price, ')
          ..write('extraPrice: $extraPrice, ')
          ..write('vat: $vat, ')
          ..write('currency: $currency')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, productUuid, price, extraPrice, vat, currency);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductPrice &&
          other.id == this.id &&
          other.productUuid == this.productUuid &&
          other.price == this.price &&
          other.extraPrice == this.extraPrice &&
          other.vat == this.vat &&
          other.currency == this.currency);
}

class TbProductPriceCompanion extends UpdateCompanion<ProductPrice> {
  final Value<String> id;
  final Value<String> productUuid;
  final Value<String?> price;
  final Value<String?> extraPrice;
  final Value<String> vat;
  final Value<String> currency;
  const TbProductPriceCompanion({
    this.id = const Value.absent(),
    this.productUuid = const Value.absent(),
    this.price = const Value.absent(),
    this.extraPrice = const Value.absent(),
    this.vat = const Value.absent(),
    this.currency = const Value.absent(),
  });
  TbProductPriceCompanion.insert({
    required String id,
    required String productUuid,
    this.price = const Value.absent(),
    this.extraPrice = const Value.absent(),
    required String vat,
    required String currency,
  })  : id = Value(id),
        productUuid = Value(productUuid),
        vat = Value(vat),
        currency = Value(currency);
  static Insertable<ProductPrice> custom({
    Expression<String>? id,
    Expression<String>? productUuid,
    Expression<String?>? price,
    Expression<String?>? extraPrice,
    Expression<String>? vat,
    Expression<String>? currency,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (productUuid != null) 'product_uuid': productUuid,
      if (price != null) 'price': price,
      if (extraPrice != null) 'extra_price': extraPrice,
      if (vat != null) 'vat': vat,
      if (currency != null) 'currency': currency,
    });
  }

  TbProductPriceCompanion copyWith(
      {Value<String>? id,
      Value<String>? productUuid,
      Value<String?>? price,
      Value<String?>? extraPrice,
      Value<String>? vat,
      Value<String>? currency}) {
    return TbProductPriceCompanion(
      id: id ?? this.id,
      productUuid: productUuid ?? this.productUuid,
      price: price ?? this.price,
      extraPrice: extraPrice ?? this.extraPrice,
      vat: vat ?? this.vat,
      currency: currency ?? this.currency,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (productUuid.present) {
      map['product_uuid'] = Variable<String>(productUuid.value);
    }
    if (price.present) {
      map['price'] = Variable<String?>(price.value);
    }
    if (extraPrice.present) {
      map['extra_price'] = Variable<String?>(extraPrice.value);
    }
    if (vat.present) {
      map['vat'] = Variable<String>(vat.value);
    }
    if (currency.present) {
      map['currency'] = Variable<String>(currency.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbProductPriceCompanion(')
          ..write('id: $id, ')
          ..write('productUuid: $productUuid, ')
          ..write('price: $price, ')
          ..write('extraPrice: $extraPrice, ')
          ..write('vat: $vat, ')
          ..write('currency: $currency')
          ..write(')'))
        .toString();
  }
}

class $TbProductPriceTable extends TbProductPrice
    with TableInfo<$TbProductPriceTable, ProductPrice> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbProductPriceTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _productUuidMeta =
      const VerificationMeta('productUuid');
  @override
  late final GeneratedColumn<String?> productUuid = GeneratedColumn<String?>(
      'product_uuid', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _priceMeta = const VerificationMeta('price');
  @override
  late final GeneratedColumn<String?> price = GeneratedColumn<String?>(
      'price', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _extraPriceMeta = const VerificationMeta('extraPrice');
  @override
  late final GeneratedColumn<String?> extraPrice = GeneratedColumn<String?>(
      'extra_price', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _vatMeta = const VerificationMeta('vat');
  @override
  late final GeneratedColumn<String?> vat = GeneratedColumn<String?>(
      'vat', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _currencyMeta = const VerificationMeta('currency');
  @override
  late final GeneratedColumn<String?> currency = GeneratedColumn<String?>(
      'currency', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, productUuid, price, extraPrice, vat, currency];
  @override
  String get aliasedName => _alias ?? 'product_price';
  @override
  String get actualTableName => 'product_price';
  @override
  VerificationContext validateIntegrity(Insertable<ProductPrice> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('product_uuid')) {
      context.handle(
          _productUuidMeta,
          productUuid.isAcceptableOrUnknown(
              data['product_uuid']!, _productUuidMeta));
    } else if (isInserting) {
      context.missing(_productUuidMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price']!, _priceMeta));
    }
    if (data.containsKey('extra_price')) {
      context.handle(
          _extraPriceMeta,
          extraPrice.isAcceptableOrUnknown(
              data['extra_price']!, _extraPriceMeta));
    }
    if (data.containsKey('vat')) {
      context.handle(
          _vatMeta, vat.isAcceptableOrUnknown(data['vat']!, _vatMeta));
    } else if (isInserting) {
      context.missing(_vatMeta);
    }
    if (data.containsKey('currency')) {
      context.handle(_currencyMeta,
          currency.isAcceptableOrUnknown(data['currency']!, _currencyMeta));
    } else if (isInserting) {
      context.missing(_currencyMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  ProductPrice map(Map<String, dynamic> data, {String? tablePrefix}) {
    return ProductPrice.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbProductPriceTable createAlias(String alias) {
    return $TbProductPriceTable(attachedDatabase, alias);
  }
}

class ConfigProductProperty extends DataClass
    implements Insertable<ConfigProductProperty> {
  final String id;
  final String platform;
  final String keyName;
  final String value;
  final int sortOrder;
  final String displayName;
  final String description;
  final String filename;
  final String contentType;
  final bool isSubmitted;
  ConfigProductProperty(
      {required this.id,
      required this.platform,
      required this.keyName,
      required this.value,
      required this.sortOrder,
      required this.displayName,
      required this.description,
      required this.filename,
      required this.contentType,
      required this.isSubmitted});
  factory ConfigProductProperty.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return ConfigProductProperty(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      platform: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}platform'])!,
      keyName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}key_name'])!,
      value: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}value'])!,
      sortOrder: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}sort_order'])!,
      displayName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}display_name'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      filename: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}filename'])!,
      contentType: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}content_type'])!,
      isSubmitted: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}isSubmitted'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['platform'] = Variable<String>(platform);
    map['key_name'] = Variable<String>(keyName);
    map['value'] = Variable<String>(value);
    map['sort_order'] = Variable<int>(sortOrder);
    map['display_name'] = Variable<String>(displayName);
    map['description'] = Variable<String>(description);
    map['filename'] = Variable<String>(filename);
    map['content_type'] = Variable<String>(contentType);
    map['isSubmitted'] = Variable<bool>(isSubmitted);
    return map;
  }

  TbConfigProductPropertyCompanion toCompanion(bool nullToAbsent) {
    return TbConfigProductPropertyCompanion(
      id: Value(id),
      platform: Value(platform),
      keyName: Value(keyName),
      value: Value(value),
      sortOrder: Value(sortOrder),
      displayName: Value(displayName),
      description: Value(description),
      filename: Value(filename),
      contentType: Value(contentType),
      isSubmitted: Value(isSubmitted),
    );
  }

  factory ConfigProductProperty.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ConfigProductProperty(
      id: serializer.fromJson<String>(json['id']),
      platform: serializer.fromJson<String>(json['platform']),
      keyName: serializer.fromJson<String>(json['keyName']),
      value: serializer.fromJson<String>(json['value']),
      sortOrder: serializer.fromJson<int>(json['sortOrder']),
      displayName: serializer.fromJson<String>(json['displayName']),
      description: serializer.fromJson<String>(json['description']),
      filename: serializer.fromJson<String>(json['filename']),
      contentType: serializer.fromJson<String>(json['contentType']),
      isSubmitted: serializer.fromJson<bool>(json['isSubmitted']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'platform': serializer.toJson<String>(platform),
      'keyName': serializer.toJson<String>(keyName),
      'value': serializer.toJson<String>(value),
      'sortOrder': serializer.toJson<int>(sortOrder),
      'displayName': serializer.toJson<String>(displayName),
      'description': serializer.toJson<String>(description),
      'filename': serializer.toJson<String>(filename),
      'contentType': serializer.toJson<String>(contentType),
      'isSubmitted': serializer.toJson<bool>(isSubmitted),
    };
  }

  ConfigProductProperty copyWith(
          {String? id,
          String? platform,
          String? keyName,
          String? value,
          int? sortOrder,
          String? displayName,
          String? description,
          String? filename,
          String? contentType,
          bool? isSubmitted}) =>
      ConfigProductProperty(
        id: id ?? this.id,
        platform: platform ?? this.platform,
        keyName: keyName ?? this.keyName,
        value: value ?? this.value,
        sortOrder: sortOrder ?? this.sortOrder,
        displayName: displayName ?? this.displayName,
        description: description ?? this.description,
        filename: filename ?? this.filename,
        contentType: contentType ?? this.contentType,
        isSubmitted: isSubmitted ?? this.isSubmitted,
      );
  @override
  String toString() {
    return (StringBuffer('ConfigProductProperty(')
          ..write('id: $id, ')
          ..write('platform: $platform, ')
          ..write('keyName: $keyName, ')
          ..write('value: $value, ')
          ..write('sortOrder: $sortOrder, ')
          ..write('displayName: $displayName, ')
          ..write('description: $description, ')
          ..write('filename: $filename, ')
          ..write('contentType: $contentType, ')
          ..write('isSubmitted: $isSubmitted')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, platform, keyName, value, sortOrder,
      displayName, description, filename, contentType, isSubmitted);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ConfigProductProperty &&
          other.id == this.id &&
          other.platform == this.platform &&
          other.keyName == this.keyName &&
          other.value == this.value &&
          other.sortOrder == this.sortOrder &&
          other.displayName == this.displayName &&
          other.description == this.description &&
          other.filename == this.filename &&
          other.contentType == this.contentType &&
          other.isSubmitted == this.isSubmitted);
}

class TbConfigProductPropertyCompanion
    extends UpdateCompanion<ConfigProductProperty> {
  final Value<String> id;
  final Value<String> platform;
  final Value<String> keyName;
  final Value<String> value;
  final Value<int> sortOrder;
  final Value<String> displayName;
  final Value<String> description;
  final Value<String> filename;
  final Value<String> contentType;
  final Value<bool> isSubmitted;
  const TbConfigProductPropertyCompanion({
    this.id = const Value.absent(),
    this.platform = const Value.absent(),
    this.keyName = const Value.absent(),
    this.value = const Value.absent(),
    this.sortOrder = const Value.absent(),
    this.displayName = const Value.absent(),
    this.description = const Value.absent(),
    this.filename = const Value.absent(),
    this.contentType = const Value.absent(),
    this.isSubmitted = const Value.absent(),
  });
  TbConfigProductPropertyCompanion.insert({
    required String id,
    required String platform,
    required String keyName,
    required String value,
    required int sortOrder,
    required String displayName,
    required String description,
    required String filename,
    required String contentType,
    required bool isSubmitted,
  })  : id = Value(id),
        platform = Value(platform),
        keyName = Value(keyName),
        value = Value(value),
        sortOrder = Value(sortOrder),
        displayName = Value(displayName),
        description = Value(description),
        filename = Value(filename),
        contentType = Value(contentType),
        isSubmitted = Value(isSubmitted);
  static Insertable<ConfigProductProperty> custom({
    Expression<String>? id,
    Expression<String>? platform,
    Expression<String>? keyName,
    Expression<String>? value,
    Expression<int>? sortOrder,
    Expression<String>? displayName,
    Expression<String>? description,
    Expression<String>? filename,
    Expression<String>? contentType,
    Expression<bool>? isSubmitted,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (platform != null) 'platform': platform,
      if (keyName != null) 'key_name': keyName,
      if (value != null) 'value': value,
      if (sortOrder != null) 'sort_order': sortOrder,
      if (displayName != null) 'display_name': displayName,
      if (description != null) 'description': description,
      if (filename != null) 'filename': filename,
      if (contentType != null) 'content_type': contentType,
      if (isSubmitted != null) 'isSubmitted': isSubmitted,
    });
  }

  TbConfigProductPropertyCompanion copyWith(
      {Value<String>? id,
      Value<String>? platform,
      Value<String>? keyName,
      Value<String>? value,
      Value<int>? sortOrder,
      Value<String>? displayName,
      Value<String>? description,
      Value<String>? filename,
      Value<String>? contentType,
      Value<bool>? isSubmitted}) {
    return TbConfigProductPropertyCompanion(
      id: id ?? this.id,
      platform: platform ?? this.platform,
      keyName: keyName ?? this.keyName,
      value: value ?? this.value,
      sortOrder: sortOrder ?? this.sortOrder,
      displayName: displayName ?? this.displayName,
      description: description ?? this.description,
      filename: filename ?? this.filename,
      contentType: contentType ?? this.contentType,
      isSubmitted: isSubmitted ?? this.isSubmitted,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (platform.present) {
      map['platform'] = Variable<String>(platform.value);
    }
    if (keyName.present) {
      map['key_name'] = Variable<String>(keyName.value);
    }
    if (value.present) {
      map['value'] = Variable<String>(value.value);
    }
    if (sortOrder.present) {
      map['sort_order'] = Variable<int>(sortOrder.value);
    }
    if (displayName.present) {
      map['display_name'] = Variable<String>(displayName.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (filename.present) {
      map['filename'] = Variable<String>(filename.value);
    }
    if (contentType.present) {
      map['content_type'] = Variable<String>(contentType.value);
    }
    if (isSubmitted.present) {
      map['isSubmitted'] = Variable<bool>(isSubmitted.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbConfigProductPropertyCompanion(')
          ..write('id: $id, ')
          ..write('platform: $platform, ')
          ..write('keyName: $keyName, ')
          ..write('value: $value, ')
          ..write('sortOrder: $sortOrder, ')
          ..write('displayName: $displayName, ')
          ..write('description: $description, ')
          ..write('filename: $filename, ')
          ..write('contentType: $contentType, ')
          ..write('isSubmitted: $isSubmitted')
          ..write(')'))
        .toString();
  }
}

class $TbConfigProductPropertyTable extends TbConfigProductProperty
    with TableInfo<$TbConfigProductPropertyTable, ConfigProductProperty> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbConfigProductPropertyTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _platformMeta = const VerificationMeta('platform');
  @override
  late final GeneratedColumn<String?> platform = GeneratedColumn<String?>(
      'platform', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _keyNameMeta = const VerificationMeta('keyName');
  @override
  late final GeneratedColumn<String?> keyName = GeneratedColumn<String?>(
      'key_name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _valueMeta = const VerificationMeta('value');
  @override
  late final GeneratedColumn<String?> value = GeneratedColumn<String?>(
      'value', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _sortOrderMeta = const VerificationMeta('sortOrder');
  @override
  late final GeneratedColumn<int?> sortOrder = GeneratedColumn<int?>(
      'sort_order', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _displayNameMeta =
      const VerificationMeta('displayName');
  @override
  late final GeneratedColumn<String?> displayName = GeneratedColumn<String?>(
      'display_name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _filenameMeta = const VerificationMeta('filename');
  @override
  late final GeneratedColumn<String?> filename = GeneratedColumn<String?>(
      'filename', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _contentTypeMeta =
      const VerificationMeta('contentType');
  @override
  late final GeneratedColumn<String?> contentType = GeneratedColumn<String?>(
      'content_type', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _isSubmittedMeta =
      const VerificationMeta('isSubmitted');
  @override
  late final GeneratedColumn<bool?> isSubmitted = GeneratedColumn<bool?>(
      'isSubmitted', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: true,
      defaultConstraints: 'CHECK (isSubmitted IN (0, 1))');
  @override
  List<GeneratedColumn> get $columns => [
        id,
        platform,
        keyName,
        value,
        sortOrder,
        displayName,
        description,
        filename,
        contentType,
        isSubmitted
      ];
  @override
  String get aliasedName => _alias ?? 'config_product_property';
  @override
  String get actualTableName => 'config_product_property';
  @override
  VerificationContext validateIntegrity(
      Insertable<ConfigProductProperty> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('platform')) {
      context.handle(_platformMeta,
          platform.isAcceptableOrUnknown(data['platform']!, _platformMeta));
    } else if (isInserting) {
      context.missing(_platformMeta);
    }
    if (data.containsKey('key_name')) {
      context.handle(_keyNameMeta,
          keyName.isAcceptableOrUnknown(data['key_name']!, _keyNameMeta));
    } else if (isInserting) {
      context.missing(_keyNameMeta);
    }
    if (data.containsKey('value')) {
      context.handle(
          _valueMeta, value.isAcceptableOrUnknown(data['value']!, _valueMeta));
    } else if (isInserting) {
      context.missing(_valueMeta);
    }
    if (data.containsKey('sort_order')) {
      context.handle(_sortOrderMeta,
          sortOrder.isAcceptableOrUnknown(data['sort_order']!, _sortOrderMeta));
    } else if (isInserting) {
      context.missing(_sortOrderMeta);
    }
    if (data.containsKey('display_name')) {
      context.handle(
          _displayNameMeta,
          displayName.isAcceptableOrUnknown(
              data['display_name']!, _displayNameMeta));
    } else if (isInserting) {
      context.missing(_displayNameMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (data.containsKey('filename')) {
      context.handle(_filenameMeta,
          filename.isAcceptableOrUnknown(data['filename']!, _filenameMeta));
    } else if (isInserting) {
      context.missing(_filenameMeta);
    }
    if (data.containsKey('content_type')) {
      context.handle(
          _contentTypeMeta,
          contentType.isAcceptableOrUnknown(
              data['content_type']!, _contentTypeMeta));
    } else if (isInserting) {
      context.missing(_contentTypeMeta);
    }
    if (data.containsKey('isSubmitted')) {
      context.handle(
          _isSubmittedMeta,
          isSubmitted.isAcceptableOrUnknown(
              data['isSubmitted']!, _isSubmittedMeta));
    } else if (isInserting) {
      context.missing(_isSubmittedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  ConfigProductProperty map(Map<String, dynamic> data, {String? tablePrefix}) {
    return ConfigProductProperty.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbConfigProductPropertyTable createAlias(String alias) {
    return $TbConfigProductPropertyTable(attachedDatabase, alias);
  }
}

class Customer extends DataClass implements Insertable<Customer> {
  final String id;
  final String? business;
  final String? firstName;
  final String? lastName;
  final String? phone;
  final String? email;
  final String createdAt;
  final String updatedAt;
  Customer(
      {required this.id,
      this.business,
      this.firstName,
      this.lastName,
      this.phone,
      this.email,
      required this.createdAt,
      required this.updatedAt});
  factory Customer.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Customer(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      business: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}business']),
      firstName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}first_name']),
      lastName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}last_name']),
      phone: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}phone']),
      email: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}email']),
      createdAt: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at'])!,
      updatedAt: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    if (!nullToAbsent || business != null) {
      map['business'] = Variable<String?>(business);
    }
    if (!nullToAbsent || firstName != null) {
      map['first_name'] = Variable<String?>(firstName);
    }
    if (!nullToAbsent || lastName != null) {
      map['last_name'] = Variable<String?>(lastName);
    }
    if (!nullToAbsent || phone != null) {
      map['phone'] = Variable<String?>(phone);
    }
    if (!nullToAbsent || email != null) {
      map['email'] = Variable<String?>(email);
    }
    map['created_at'] = Variable<String>(createdAt);
    map['updated_at'] = Variable<String>(updatedAt);
    return map;
  }

  TbCustomerCompanion toCompanion(bool nullToAbsent) {
    return TbCustomerCompanion(
      id: Value(id),
      business: business == null && nullToAbsent
          ? const Value.absent()
          : Value(business),
      firstName: firstName == null && nullToAbsent
          ? const Value.absent()
          : Value(firstName),
      lastName: lastName == null && nullToAbsent
          ? const Value.absent()
          : Value(lastName),
      phone:
          phone == null && nullToAbsent ? const Value.absent() : Value(phone),
      email:
          email == null && nullToAbsent ? const Value.absent() : Value(email),
      createdAt: Value(createdAt),
      updatedAt: Value(updatedAt),
    );
  }

  factory Customer.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Customer(
      id: serializer.fromJson<String>(json['id']),
      business: serializer.fromJson<String?>(json['business']),
      firstName: serializer.fromJson<String?>(json['firstName']),
      lastName: serializer.fromJson<String?>(json['lastName']),
      phone: serializer.fromJson<String?>(json['phone']),
      email: serializer.fromJson<String?>(json['email']),
      createdAt: serializer.fromJson<String>(json['createdAt']),
      updatedAt: serializer.fromJson<String>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'business': serializer.toJson<String?>(business),
      'firstName': serializer.toJson<String?>(firstName),
      'lastName': serializer.toJson<String?>(lastName),
      'phone': serializer.toJson<String?>(phone),
      'email': serializer.toJson<String?>(email),
      'createdAt': serializer.toJson<String>(createdAt),
      'updatedAt': serializer.toJson<String>(updatedAt),
    };
  }

  Customer copyWith(
          {String? id,
          String? business,
          String? firstName,
          String? lastName,
          String? phone,
          String? email,
          String? createdAt,
          String? updatedAt}) =>
      Customer(
        id: id ?? this.id,
        business: business ?? this.business,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        phone: phone ?? this.phone,
        email: email ?? this.email,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Customer(')
          ..write('id: $id, ')
          ..write('business: $business, ')
          ..write('firstName: $firstName, ')
          ..write('lastName: $lastName, ')
          ..write('phone: $phone, ')
          ..write('email: $email, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, business, firstName, lastName, phone, email, createdAt, updatedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Customer &&
          other.id == this.id &&
          other.business == this.business &&
          other.firstName == this.firstName &&
          other.lastName == this.lastName &&
          other.phone == this.phone &&
          other.email == this.email &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class TbCustomerCompanion extends UpdateCompanion<Customer> {
  final Value<String> id;
  final Value<String?> business;
  final Value<String?> firstName;
  final Value<String?> lastName;
  final Value<String?> phone;
  final Value<String?> email;
  final Value<String> createdAt;
  final Value<String> updatedAt;
  const TbCustomerCompanion({
    this.id = const Value.absent(),
    this.business = const Value.absent(),
    this.firstName = const Value.absent(),
    this.lastName = const Value.absent(),
    this.phone = const Value.absent(),
    this.email = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  TbCustomerCompanion.insert({
    required String id,
    this.business = const Value.absent(),
    this.firstName = const Value.absent(),
    this.lastName = const Value.absent(),
    this.phone = const Value.absent(),
    this.email = const Value.absent(),
    required String createdAt,
    required String updatedAt,
  })  : id = Value(id),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Customer> custom({
    Expression<String>? id,
    Expression<String?>? business,
    Expression<String?>? firstName,
    Expression<String?>? lastName,
    Expression<String?>? phone,
    Expression<String?>? email,
    Expression<String>? createdAt,
    Expression<String>? updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (business != null) 'business': business,
      if (firstName != null) 'first_name': firstName,
      if (lastName != null) 'last_name': lastName,
      if (phone != null) 'phone': phone,
      if (email != null) 'email': email,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  TbCustomerCompanion copyWith(
      {Value<String>? id,
      Value<String?>? business,
      Value<String?>? firstName,
      Value<String?>? lastName,
      Value<String?>? phone,
      Value<String?>? email,
      Value<String>? createdAt,
      Value<String>? updatedAt}) {
    return TbCustomerCompanion(
      id: id ?? this.id,
      business: business ?? this.business,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (business.present) {
      map['business'] = Variable<String?>(business.value);
    }
    if (firstName.present) {
      map['first_name'] = Variable<String?>(firstName.value);
    }
    if (lastName.present) {
      map['last_name'] = Variable<String?>(lastName.value);
    }
    if (phone.present) {
      map['phone'] = Variable<String?>(phone.value);
    }
    if (email.present) {
      map['email'] = Variable<String?>(email.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<String>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<String>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbCustomerCompanion(')
          ..write('id: $id, ')
          ..write('business: $business, ')
          ..write('firstName: $firstName, ')
          ..write('lastName: $lastName, ')
          ..write('phone: $phone, ')
          ..write('email: $email, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $TbCustomerTable extends TbCustomer
    with TableInfo<$TbCustomerTable, Customer> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbCustomerTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _businessMeta = const VerificationMeta('business');
  @override
  late final GeneratedColumn<String?> business = GeneratedColumn<String?>(
      'business', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _firstNameMeta = const VerificationMeta('firstName');
  @override
  late final GeneratedColumn<String?> firstName = GeneratedColumn<String?>(
      'first_name', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _lastNameMeta = const VerificationMeta('lastName');
  @override
  late final GeneratedColumn<String?> lastName = GeneratedColumn<String?>(
      'last_name', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _phoneMeta = const VerificationMeta('phone');
  @override
  late final GeneratedColumn<String?> phone = GeneratedColumn<String?>(
      'phone', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _emailMeta = const VerificationMeta('email');
  @override
  late final GeneratedColumn<String?> email = GeneratedColumn<String?>(
      'email', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<String?> createdAt = GeneratedColumn<String?>(
      'created_at', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 40),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<String?> updatedAt = GeneratedColumn<String?>(
      'updated_at', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 40),
      type: const StringType(),
      requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, business, firstName, lastName, phone, email, createdAt, updatedAt];
  @override
  String get aliasedName => _alias ?? 'customer';
  @override
  String get actualTableName => 'customer';
  @override
  VerificationContext validateIntegrity(Insertable<Customer> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('business')) {
      context.handle(_businessMeta,
          business.isAcceptableOrUnknown(data['business']!, _businessMeta));
    }
    if (data.containsKey('first_name')) {
      context.handle(_firstNameMeta,
          firstName.isAcceptableOrUnknown(data['first_name']!, _firstNameMeta));
    }
    if (data.containsKey('last_name')) {
      context.handle(_lastNameMeta,
          lastName.isAcceptableOrUnknown(data['last_name']!, _lastNameMeta));
    }
    if (data.containsKey('phone')) {
      context.handle(
          _phoneMeta, phone.isAcceptableOrUnknown(data['phone']!, _phoneMeta));
    }
    if (data.containsKey('email')) {
      context.handle(
          _emailMeta, email.isAcceptableOrUnknown(data['email']!, _emailMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Customer map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Customer.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbCustomerTable createAlias(String alias) {
    return $TbCustomerTable(attachedDatabase, alias);
  }
}

class CustomerAddress extends DataClass implements Insertable<CustomerAddress> {
  final String id;
  final String onetimeConsumerId;
  final String? type;
  final String? streetName;
  final String? building;
  final String? city;
  final String? state;
  final String? zip;
  CustomerAddress(
      {required this.id,
      required this.onetimeConsumerId,
      this.type,
      this.streetName,
      this.building,
      this.city,
      this.state,
      this.zip});
  factory CustomerAddress.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return CustomerAddress(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      onetimeConsumerId: const StringType().mapFromDatabaseResponse(
          data['${effectivePrefix}onetime_consumer_id'])!,
      type: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}type']),
      streetName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}street_name']),
      building: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}building']),
      city: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}city']),
      state: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}state']),
      zip: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}zip']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['onetime_consumer_id'] = Variable<String>(onetimeConsumerId);
    if (!nullToAbsent || type != null) {
      map['type'] = Variable<String?>(type);
    }
    if (!nullToAbsent || streetName != null) {
      map['street_name'] = Variable<String?>(streetName);
    }
    if (!nullToAbsent || building != null) {
      map['building'] = Variable<String?>(building);
    }
    if (!nullToAbsent || city != null) {
      map['city'] = Variable<String?>(city);
    }
    if (!nullToAbsent || state != null) {
      map['state'] = Variable<String?>(state);
    }
    if (!nullToAbsent || zip != null) {
      map['zip'] = Variable<String?>(zip);
    }
    return map;
  }

  TbCustomerAddressCompanion toCompanion(bool nullToAbsent) {
    return TbCustomerAddressCompanion(
      id: Value(id),
      onetimeConsumerId: Value(onetimeConsumerId),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
      streetName: streetName == null && nullToAbsent
          ? const Value.absent()
          : Value(streetName),
      building: building == null && nullToAbsent
          ? const Value.absent()
          : Value(building),
      city: city == null && nullToAbsent ? const Value.absent() : Value(city),
      state:
          state == null && nullToAbsent ? const Value.absent() : Value(state),
      zip: zip == null && nullToAbsent ? const Value.absent() : Value(zip),
    );
  }

  factory CustomerAddress.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CustomerAddress(
      id: serializer.fromJson<String>(json['id']),
      onetimeConsumerId: serializer.fromJson<String>(json['onetimeConsumerId']),
      type: serializer.fromJson<String?>(json['type']),
      streetName: serializer.fromJson<String?>(json['streetName']),
      building: serializer.fromJson<String?>(json['building']),
      city: serializer.fromJson<String?>(json['city']),
      state: serializer.fromJson<String?>(json['state']),
      zip: serializer.fromJson<String?>(json['zip']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'onetimeConsumerId': serializer.toJson<String>(onetimeConsumerId),
      'type': serializer.toJson<String?>(type),
      'streetName': serializer.toJson<String?>(streetName),
      'building': serializer.toJson<String?>(building),
      'city': serializer.toJson<String?>(city),
      'state': serializer.toJson<String?>(state),
      'zip': serializer.toJson<String?>(zip),
    };
  }

  CustomerAddress copyWith(
          {String? id,
          String? onetimeConsumerId,
          String? type,
          String? streetName,
          String? building,
          String? city,
          String? state,
          String? zip}) =>
      CustomerAddress(
        id: id ?? this.id,
        onetimeConsumerId: onetimeConsumerId ?? this.onetimeConsumerId,
        type: type ?? this.type,
        streetName: streetName ?? this.streetName,
        building: building ?? this.building,
        city: city ?? this.city,
        state: state ?? this.state,
        zip: zip ?? this.zip,
      );
  @override
  String toString() {
    return (StringBuffer('CustomerAddress(')
          ..write('id: $id, ')
          ..write('onetimeConsumerId: $onetimeConsumerId, ')
          ..write('type: $type, ')
          ..write('streetName: $streetName, ')
          ..write('building: $building, ')
          ..write('city: $city, ')
          ..write('state: $state, ')
          ..write('zip: $zip')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, onetimeConsumerId, type, streetName, building, city, state, zip);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CustomerAddress &&
          other.id == this.id &&
          other.onetimeConsumerId == this.onetimeConsumerId &&
          other.type == this.type &&
          other.streetName == this.streetName &&
          other.building == this.building &&
          other.city == this.city &&
          other.state == this.state &&
          other.zip == this.zip);
}

class TbCustomerAddressCompanion extends UpdateCompanion<CustomerAddress> {
  final Value<String> id;
  final Value<String> onetimeConsumerId;
  final Value<String?> type;
  final Value<String?> streetName;
  final Value<String?> building;
  final Value<String?> city;
  final Value<String?> state;
  final Value<String?> zip;
  const TbCustomerAddressCompanion({
    this.id = const Value.absent(),
    this.onetimeConsumerId = const Value.absent(),
    this.type = const Value.absent(),
    this.streetName = const Value.absent(),
    this.building = const Value.absent(),
    this.city = const Value.absent(),
    this.state = const Value.absent(),
    this.zip = const Value.absent(),
  });
  TbCustomerAddressCompanion.insert({
    required String id,
    required String onetimeConsumerId,
    this.type = const Value.absent(),
    this.streetName = const Value.absent(),
    this.building = const Value.absent(),
    this.city = const Value.absent(),
    this.state = const Value.absent(),
    this.zip = const Value.absent(),
  })  : id = Value(id),
        onetimeConsumerId = Value(onetimeConsumerId);
  static Insertable<CustomerAddress> custom({
    Expression<String>? id,
    Expression<String>? onetimeConsumerId,
    Expression<String?>? type,
    Expression<String?>? streetName,
    Expression<String?>? building,
    Expression<String?>? city,
    Expression<String?>? state,
    Expression<String?>? zip,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (onetimeConsumerId != null) 'onetime_consumer_id': onetimeConsumerId,
      if (type != null) 'type': type,
      if (streetName != null) 'street_name': streetName,
      if (building != null) 'building': building,
      if (city != null) 'city': city,
      if (state != null) 'state': state,
      if (zip != null) 'zip': zip,
    });
  }

  TbCustomerAddressCompanion copyWith(
      {Value<String>? id,
      Value<String>? onetimeConsumerId,
      Value<String?>? type,
      Value<String?>? streetName,
      Value<String?>? building,
      Value<String?>? city,
      Value<String?>? state,
      Value<String?>? zip}) {
    return TbCustomerAddressCompanion(
      id: id ?? this.id,
      onetimeConsumerId: onetimeConsumerId ?? this.onetimeConsumerId,
      type: type ?? this.type,
      streetName: streetName ?? this.streetName,
      building: building ?? this.building,
      city: city ?? this.city,
      state: state ?? this.state,
      zip: zip ?? this.zip,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (onetimeConsumerId.present) {
      map['onetime_consumer_id'] = Variable<String>(onetimeConsumerId.value);
    }
    if (type.present) {
      map['type'] = Variable<String?>(type.value);
    }
    if (streetName.present) {
      map['street_name'] = Variable<String?>(streetName.value);
    }
    if (building.present) {
      map['building'] = Variable<String?>(building.value);
    }
    if (city.present) {
      map['city'] = Variable<String?>(city.value);
    }
    if (state.present) {
      map['state'] = Variable<String?>(state.value);
    }
    if (zip.present) {
      map['zip'] = Variable<String?>(zip.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbCustomerAddressCompanion(')
          ..write('id: $id, ')
          ..write('onetimeConsumerId: $onetimeConsumerId, ')
          ..write('type: $type, ')
          ..write('streetName: $streetName, ')
          ..write('building: $building, ')
          ..write('city: $city, ')
          ..write('state: $state, ')
          ..write('zip: $zip')
          ..write(')'))
        .toString();
  }
}

class $TbCustomerAddressTable extends TbCustomerAddress
    with TableInfo<$TbCustomerAddressTable, CustomerAddress> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbCustomerAddressTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _onetimeConsumerIdMeta =
      const VerificationMeta('onetimeConsumerId');
  @override
  late final GeneratedColumn<String?> onetimeConsumerId =
      GeneratedColumn<String?>('onetime_consumer_id', aliasedName, false,
          type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String?> type = GeneratedColumn<String?>(
      'type', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _streetNameMeta = const VerificationMeta('streetName');
  @override
  late final GeneratedColumn<String?> streetName = GeneratedColumn<String?>(
      'street_name', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _buildingMeta = const VerificationMeta('building');
  @override
  late final GeneratedColumn<String?> building = GeneratedColumn<String?>(
      'building', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _cityMeta = const VerificationMeta('city');
  @override
  late final GeneratedColumn<String?> city = GeneratedColumn<String?>(
      'city', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _stateMeta = const VerificationMeta('state');
  @override
  late final GeneratedColumn<String?> state = GeneratedColumn<String?>(
      'state', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _zipMeta = const VerificationMeta('zip');
  @override
  late final GeneratedColumn<String?> zip = GeneratedColumn<String?>(
      'zip', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [id, onetimeConsumerId, type, streetName, building, city, state, zip];
  @override
  String get aliasedName => _alias ?? 'customer_address';
  @override
  String get actualTableName => 'customer_address';
  @override
  VerificationContext validateIntegrity(Insertable<CustomerAddress> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('onetime_consumer_id')) {
      context.handle(
          _onetimeConsumerIdMeta,
          onetimeConsumerId.isAcceptableOrUnknown(
              data['onetime_consumer_id']!, _onetimeConsumerIdMeta));
    } else if (isInserting) {
      context.missing(_onetimeConsumerIdMeta);
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    }
    if (data.containsKey('street_name')) {
      context.handle(
          _streetNameMeta,
          streetName.isAcceptableOrUnknown(
              data['street_name']!, _streetNameMeta));
    }
    if (data.containsKey('building')) {
      context.handle(_buildingMeta,
          building.isAcceptableOrUnknown(data['building']!, _buildingMeta));
    }
    if (data.containsKey('city')) {
      context.handle(
          _cityMeta, city.isAcceptableOrUnknown(data['city']!, _cityMeta));
    }
    if (data.containsKey('state')) {
      context.handle(
          _stateMeta, state.isAcceptableOrUnknown(data['state']!, _stateMeta));
    }
    if (data.containsKey('zip')) {
      context.handle(
          _zipMeta, zip.isAcceptableOrUnknown(data['zip']!, _zipMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  CustomerAddress map(Map<String, dynamic> data, {String? tablePrefix}) {
    return CustomerAddress.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbCustomerAddressTable createAlias(String alias) {
    return $TbCustomerAddressTable(attachedDatabase, alias);
  }
}

class CartItem extends DataClass implements Insertable<CartItem> {
  final String id;
  final String localId;
  final String uuid;
  final String shortName;
  final String description;
  final String price;
  final String offerPrice;
  final String subTotal;
  final String total;
  final int quantity;
  final String comment;
  final String productType;
  final String printOrder;
  final int isSubmitted;
  final String discountable;
  final String offerAble;
  final String offered;
  final String kitchenItem;
  final String category;
  CartItem(
      {required this.id,
      required this.localId,
      required this.uuid,
      required this.shortName,
      required this.description,
      required this.price,
      required this.offerPrice,
      required this.subTotal,
      required this.total,
      required this.quantity,
      required this.comment,
      required this.productType,
      required this.printOrder,
      required this.isSubmitted,
      required this.discountable,
      required this.offerAble,
      required this.offered,
      required this.kitchenItem,
      required this.category});
  factory CartItem.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return CartItem(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      localId: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}localId'])!,
      uuid: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_uuid'])!,
      shortName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}short_name'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      price: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}price'])!,
      offerPrice: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}offerPrice'])!,
      subTotal: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}subTotal'])!,
      total: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}total'])!,
      quantity: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}quantity'])!,
      comment: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}comment'])!,
      productType: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_type'])!,
      printOrder: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}print_order'])!,
      isSubmitted: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}print_order'])!,
      discountable: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}discountable'])!,
      offerAble: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}offerAble'])!,
      offered: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}offered'])!,
      kitchenItem: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}kitchen_item'])!,
      category: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}category'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['localId'] = Variable<String>(localId);
    map['product_uuid'] = Variable<String>(uuid);
    map['short_name'] = Variable<String>(shortName);
    map['description'] = Variable<String>(description);
    map['price'] = Variable<String>(price);
    map['offerPrice'] = Variable<String>(offerPrice);
    map['subTotal'] = Variable<String>(subTotal);
    map['total'] = Variable<String>(total);
    map['quantity'] = Variable<int>(quantity);
    map['comment'] = Variable<String>(comment);
    map['product_type'] = Variable<String>(productType);
    map['print_order'] = Variable<String>(printOrder);
    map['print_order'] = Variable<int>(isSubmitted);
    map['discountable'] = Variable<String>(discountable);
    map['offerAble'] = Variable<String>(offerAble);
    map['offered'] = Variable<String>(offered);
    map['kitchen_item'] = Variable<String>(kitchenItem);
    map['category'] = Variable<String>(category);
    return map;
  }

  TbCartItemCompanion toCompanion(bool nullToAbsent) {
    return TbCartItemCompanion(
      id: Value(id),
      localId: Value(localId),
      uuid: Value(uuid),
      shortName: Value(shortName),
      description: Value(description),
      price: Value(price),
      offerPrice: Value(offerPrice),
      subTotal: Value(subTotal),
      total: Value(total),
      quantity: Value(quantity),
      comment: Value(comment),
      productType: Value(productType),
      printOrder: Value(printOrder),
      isSubmitted: Value(isSubmitted),
      discountable: Value(discountable),
      offerAble: Value(offerAble),
      offered: Value(offered),
      kitchenItem: Value(kitchenItem),
      category: Value(category),
    );
  }

  factory CartItem.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CartItem(
      id: serializer.fromJson<String>(json['id']),
      localId: serializer.fromJson<String>(json['localId']),
      uuid: serializer.fromJson<String>(json['uuid']),
      shortName: serializer.fromJson<String>(json['shortName']),
      description: serializer.fromJson<String>(json['description']),
      price: serializer.fromJson<String>(json['price']),
      offerPrice: serializer.fromJson<String>(json['offerPrice']),
      subTotal: serializer.fromJson<String>(json['subTotal']),
      total: serializer.fromJson<String>(json['total']),
      quantity: serializer.fromJson<int>(json['quantity']),
      comment: serializer.fromJson<String>(json['comment']),
      productType: serializer.fromJson<String>(json['productType']),
      printOrder: serializer.fromJson<String>(json['printOrder']),
      isSubmitted: serializer.fromJson<int>(json['isSubmitted']),
      discountable: serializer.fromJson<String>(json['discountable']),
      offerAble: serializer.fromJson<String>(json['offerAble']),
      offered: serializer.fromJson<String>(json['offered']),
      kitchenItem: serializer.fromJson<String>(json['kitchenItem']),
      category: serializer.fromJson<String>(json['category']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'localId': serializer.toJson<String>(localId),
      'uuid': serializer.toJson<String>(uuid),
      'shortName': serializer.toJson<String>(shortName),
      'description': serializer.toJson<String>(description),
      'price': serializer.toJson<String>(price),
      'offerPrice': serializer.toJson<String>(offerPrice),
      'subTotal': serializer.toJson<String>(subTotal),
      'total': serializer.toJson<String>(total),
      'quantity': serializer.toJson<int>(quantity),
      'comment': serializer.toJson<String>(comment),
      'productType': serializer.toJson<String>(productType),
      'printOrder': serializer.toJson<String>(printOrder),
      'isSubmitted': serializer.toJson<int>(isSubmitted),
      'discountable': serializer.toJson<String>(discountable),
      'offerAble': serializer.toJson<String>(offerAble),
      'offered': serializer.toJson<String>(offered),
      'kitchenItem': serializer.toJson<String>(kitchenItem),
      'category': serializer.toJson<String>(category),
    };
  }

  CartItem copyWith(
          {String? id,
          String? localId,
          String? uuid,
          String? shortName,
          String? description,
          String? price,
          String? offerPrice,
          String? subTotal,
          String? total,
          int? quantity,
          String? comment,
          String? productType,
          String? printOrder,
          int? isSubmitted,
          String? discountable,
          String? offerAble,
          String? offered,
          String? kitchenItem,
          String? category}) =>
      CartItem(
        id: id ?? this.id,
        localId: localId ?? this.localId,
        uuid: uuid ?? this.uuid,
        shortName: shortName ?? this.shortName,
        description: description ?? this.description,
        price: price ?? this.price,
        offerPrice: offerPrice ?? this.offerPrice,
        subTotal: subTotal ?? this.subTotal,
        total: total ?? this.total,
        quantity: quantity ?? this.quantity,
        comment: comment ?? this.comment,
        productType: productType ?? this.productType,
        printOrder: printOrder ?? this.printOrder,
        isSubmitted: isSubmitted ?? this.isSubmitted,
        discountable: discountable ?? this.discountable,
        offerAble: offerAble ?? this.offerAble,
        offered: offered ?? this.offered,
        kitchenItem: kitchenItem ?? this.kitchenItem,
        category: category ?? this.category,
      );
  @override
  String toString() {
    return (StringBuffer('CartItem(')
          ..write('id: $id, ')
          ..write('localId: $localId, ')
          ..write('uuid: $uuid, ')
          ..write('shortName: $shortName, ')
          ..write('description: $description, ')
          ..write('price: $price, ')
          ..write('offerPrice: $offerPrice, ')
          ..write('subTotal: $subTotal, ')
          ..write('total: $total, ')
          ..write('quantity: $quantity, ')
          ..write('comment: $comment, ')
          ..write('productType: $productType, ')
          ..write('printOrder: $printOrder, ')
          ..write('isSubmitted: $isSubmitted, ')
          ..write('discountable: $discountable, ')
          ..write('offerAble: $offerAble, ')
          ..write('offered: $offered, ')
          ..write('kitchenItem: $kitchenItem, ')
          ..write('category: $category')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      localId,
      uuid,
      shortName,
      description,
      price,
      offerPrice,
      subTotal,
      total,
      quantity,
      comment,
      productType,
      printOrder,
      isSubmitted,
      discountable,
      offerAble,
      offered,
      kitchenItem,
      category);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CartItem &&
          other.id == this.id &&
          other.localId == this.localId &&
          other.uuid == this.uuid &&
          other.shortName == this.shortName &&
          other.description == this.description &&
          other.price == this.price &&
          other.offerPrice == this.offerPrice &&
          other.subTotal == this.subTotal &&
          other.total == this.total &&
          other.quantity == this.quantity &&
          other.comment == this.comment &&
          other.productType == this.productType &&
          other.printOrder == this.printOrder &&
          other.isSubmitted == this.isSubmitted &&
          other.discountable == this.discountable &&
          other.offerAble == this.offerAble &&
          other.offered == this.offered &&
          other.kitchenItem == this.kitchenItem &&
          other.category == this.category);
}

class TbCartItemCompanion extends UpdateCompanion<CartItem> {
  final Value<String> id;
  final Value<String> localId;
  final Value<String> uuid;
  final Value<String> shortName;
  final Value<String> description;
  final Value<String> price;
  final Value<String> offerPrice;
  final Value<String> subTotal;
  final Value<String> total;
  final Value<int> quantity;
  final Value<String> comment;
  final Value<String> productType;
  final Value<String> printOrder;
  final Value<int> isSubmitted;
  final Value<String> discountable;
  final Value<String> offerAble;
  final Value<String> offered;
  final Value<String> kitchenItem;
  final Value<String> category;
  const TbCartItemCompanion({
    this.id = const Value.absent(),
    this.localId = const Value.absent(),
    this.uuid = const Value.absent(),
    this.shortName = const Value.absent(),
    this.description = const Value.absent(),
    this.price = const Value.absent(),
    this.offerPrice = const Value.absent(),
    this.subTotal = const Value.absent(),
    this.total = const Value.absent(),
    this.quantity = const Value.absent(),
    this.comment = const Value.absent(),
    this.productType = const Value.absent(),
    this.printOrder = const Value.absent(),
    this.isSubmitted = const Value.absent(),
    this.discountable = const Value.absent(),
    this.offerAble = const Value.absent(),
    this.offered = const Value.absent(),
    this.kitchenItem = const Value.absent(),
    this.category = const Value.absent(),
  });
  TbCartItemCompanion.insert({
    required String id,
    required String localId,
    required String uuid,
    required String shortName,
    required String description,
    required String price,
    required String offerPrice,
    required String subTotal,
    required String total,
    required int quantity,
    required String comment,
    required String productType,
    required String printOrder,
    required int isSubmitted,
    required String discountable,
    required String offerAble,
    required String offered,
    required String kitchenItem,
    required String category,
  })  : id = Value(id),
        localId = Value(localId),
        uuid = Value(uuid),
        shortName = Value(shortName),
        description = Value(description),
        price = Value(price),
        offerPrice = Value(offerPrice),
        subTotal = Value(subTotal),
        total = Value(total),
        quantity = Value(quantity),
        comment = Value(comment),
        productType = Value(productType),
        printOrder = Value(printOrder),
        isSubmitted = Value(isSubmitted),
        discountable = Value(discountable),
        offerAble = Value(offerAble),
        offered = Value(offered),
        kitchenItem = Value(kitchenItem),
        category = Value(category);
  static Insertable<CartItem> custom({
    Expression<String>? id,
    Expression<String>? localId,
    Expression<String>? uuid,
    Expression<String>? shortName,
    Expression<String>? description,
    Expression<String>? price,
    Expression<String>? offerPrice,
    Expression<String>? subTotal,
    Expression<String>? total,
    Expression<int>? quantity,
    Expression<String>? comment,
    Expression<String>? productType,
    Expression<String>? printOrder,
    Expression<int>? isSubmitted,
    Expression<String>? discountable,
    Expression<String>? offerAble,
    Expression<String>? offered,
    Expression<String>? kitchenItem,
    Expression<String>? category,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (localId != null) 'localId': localId,
      if (uuid != null) 'product_uuid': uuid,
      if (shortName != null) 'short_name': shortName,
      if (description != null) 'description': description,
      if (price != null) 'price': price,
      if (offerPrice != null) 'offerPrice': offerPrice,
      if (subTotal != null) 'subTotal': subTotal,
      if (total != null) 'total': total,
      if (quantity != null) 'quantity': quantity,
      if (comment != null) 'comment': comment,
      if (productType != null) 'product_type': productType,
      if (printOrder != null) 'print_order': printOrder,
      if (isSubmitted != null) 'print_order': isSubmitted,
      if (discountable != null) 'discountable': discountable,
      if (offerAble != null) 'offerAble': offerAble,
      if (offered != null) 'offered': offered,
      if (kitchenItem != null) 'kitchen_item': kitchenItem,
      if (category != null) 'category': category,
    });
  }

  TbCartItemCompanion copyWith(
      {Value<String>? id,
      Value<String>? localId,
      Value<String>? uuid,
      Value<String>? shortName,
      Value<String>? description,
      Value<String>? price,
      Value<String>? offerPrice,
      Value<String>? subTotal,
      Value<String>? total,
      Value<int>? quantity,
      Value<String>? comment,
      Value<String>? productType,
      Value<String>? printOrder,
      Value<int>? isSubmitted,
      Value<String>? discountable,
      Value<String>? offerAble,
      Value<String>? offered,
      Value<String>? kitchenItem,
      Value<String>? category}) {
    return TbCartItemCompanion(
      id: id ?? this.id,
      localId: localId ?? this.localId,
      uuid: uuid ?? this.uuid,
      shortName: shortName ?? this.shortName,
      description: description ?? this.description,
      price: price ?? this.price,
      offerPrice: offerPrice ?? this.offerPrice,
      subTotal: subTotal ?? this.subTotal,
      total: total ?? this.total,
      quantity: quantity ?? this.quantity,
      comment: comment ?? this.comment,
      productType: productType ?? this.productType,
      printOrder: printOrder ?? this.printOrder,
      isSubmitted: isSubmitted ?? this.isSubmitted,
      discountable: discountable ?? this.discountable,
      offerAble: offerAble ?? this.offerAble,
      offered: offered ?? this.offered,
      kitchenItem: kitchenItem ?? this.kitchenItem,
      category: category ?? this.category,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (localId.present) {
      map['localId'] = Variable<String>(localId.value);
    }
    if (uuid.present) {
      map['product_uuid'] = Variable<String>(uuid.value);
    }
    if (shortName.present) {
      map['short_name'] = Variable<String>(shortName.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (price.present) {
      map['price'] = Variable<String>(price.value);
    }
    if (offerPrice.present) {
      map['offerPrice'] = Variable<String>(offerPrice.value);
    }
    if (subTotal.present) {
      map['subTotal'] = Variable<String>(subTotal.value);
    }
    if (total.present) {
      map['total'] = Variable<String>(total.value);
    }
    if (quantity.present) {
      map['quantity'] = Variable<int>(quantity.value);
    }
    if (comment.present) {
      map['comment'] = Variable<String>(comment.value);
    }
    if (productType.present) {
      map['product_type'] = Variable<String>(productType.value);
    }
    if (printOrder.present) {
      map['print_order'] = Variable<String>(printOrder.value);
    }
    if (isSubmitted.present) {
      map['print_order'] = Variable<int>(isSubmitted.value);
    }
    if (discountable.present) {
      map['discountable'] = Variable<String>(discountable.value);
    }
    if (offerAble.present) {
      map['offerAble'] = Variable<String>(offerAble.value);
    }
    if (offered.present) {
      map['offered'] = Variable<String>(offered.value);
    }
    if (kitchenItem.present) {
      map['kitchen_item'] = Variable<String>(kitchenItem.value);
    }
    if (category.present) {
      map['category'] = Variable<String>(category.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbCartItemCompanion(')
          ..write('id: $id, ')
          ..write('localId: $localId, ')
          ..write('uuid: $uuid, ')
          ..write('shortName: $shortName, ')
          ..write('description: $description, ')
          ..write('price: $price, ')
          ..write('offerPrice: $offerPrice, ')
          ..write('subTotal: $subTotal, ')
          ..write('total: $total, ')
          ..write('quantity: $quantity, ')
          ..write('comment: $comment, ')
          ..write('productType: $productType, ')
          ..write('printOrder: $printOrder, ')
          ..write('isSubmitted: $isSubmitted, ')
          ..write('discountable: $discountable, ')
          ..write('offerAble: $offerAble, ')
          ..write('offered: $offered, ')
          ..write('kitchenItem: $kitchenItem, ')
          ..write('category: $category')
          ..write(')'))
        .toString();
  }
}

class $TbCartItemTable extends TbCartItem
    with TableInfo<$TbCartItemTable, CartItem> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbCartItemTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _localIdMeta = const VerificationMeta('localId');
  @override
  late final GeneratedColumn<String?> localId = GeneratedColumn<String?>(
      'localId', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String?> uuid = GeneratedColumn<String?>(
      'product_uuid', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _shortNameMeta = const VerificationMeta('shortName');
  @override
  late final GeneratedColumn<String?> shortName = GeneratedColumn<String?>(
      'short_name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _priceMeta = const VerificationMeta('price');
  @override
  late final GeneratedColumn<String?> price = GeneratedColumn<String?>(
      'price', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _offerPriceMeta = const VerificationMeta('offerPrice');
  @override
  late final GeneratedColumn<String?> offerPrice = GeneratedColumn<String?>(
      'offerPrice', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _subTotalMeta = const VerificationMeta('subTotal');
  @override
  late final GeneratedColumn<String?> subTotal = GeneratedColumn<String?>(
      'subTotal', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _totalMeta = const VerificationMeta('total');
  @override
  late final GeneratedColumn<String?> total = GeneratedColumn<String?>(
      'total', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _quantityMeta = const VerificationMeta('quantity');
  @override
  late final GeneratedColumn<int?> quantity = GeneratedColumn<int?>(
      'quantity', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _commentMeta = const VerificationMeta('comment');
  @override
  late final GeneratedColumn<String?> comment = GeneratedColumn<String?>(
      'comment', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _productTypeMeta =
      const VerificationMeta('productType');
  @override
  late final GeneratedColumn<String?> productType = GeneratedColumn<String?>(
      'product_type', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _printOrderMeta = const VerificationMeta('printOrder');
  @override
  late final GeneratedColumn<String?> printOrder = GeneratedColumn<String?>(
      'print_order', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _isSubmittedMeta =
      const VerificationMeta('isSubmitted');
  @override
  late final GeneratedColumn<int?> isSubmitted = GeneratedColumn<int?>(
      'print_order', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _discountableMeta =
      const VerificationMeta('discountable');
  @override
  late final GeneratedColumn<String?> discountable = GeneratedColumn<String?>(
      'discountable', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _offerAbleMeta = const VerificationMeta('offerAble');
  @override
  late final GeneratedColumn<String?> offerAble = GeneratedColumn<String?>(
      'offerAble', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _offeredMeta = const VerificationMeta('offered');
  @override
  late final GeneratedColumn<String?> offered = GeneratedColumn<String?>(
      'offered', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _kitchenItemMeta =
      const VerificationMeta('kitchenItem');
  @override
  late final GeneratedColumn<String?> kitchenItem = GeneratedColumn<String?>(
      'kitchen_item', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _categoryMeta = const VerificationMeta('category');
  @override
  late final GeneratedColumn<String?> category = GeneratedColumn<String?>(
      'category', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        localId,
        uuid,
        shortName,
        description,
        price,
        offerPrice,
        subTotal,
        total,
        quantity,
        comment,
        productType,
        printOrder,
        isSubmitted,
        discountable,
        offerAble,
        offered,
        kitchenItem,
        category
      ];
  @override
  String get aliasedName => _alias ?? 'cart_item';
  @override
  String get actualTableName => 'cart_item';
  @override
  VerificationContext validateIntegrity(Insertable<CartItem> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('localId')) {
      context.handle(_localIdMeta,
          localId.isAcceptableOrUnknown(data['localId']!, _localIdMeta));
    } else if (isInserting) {
      context.missing(_localIdMeta);
    }
    if (data.containsKey('product_uuid')) {
      context.handle(_uuidMeta,
          uuid.isAcceptableOrUnknown(data['product_uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('short_name')) {
      context.handle(_shortNameMeta,
          shortName.isAcceptableOrUnknown(data['short_name']!, _shortNameMeta));
    } else if (isInserting) {
      context.missing(_shortNameMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price']!, _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    if (data.containsKey('offerPrice')) {
      context.handle(
          _offerPriceMeta,
          offerPrice.isAcceptableOrUnknown(
              data['offerPrice']!, _offerPriceMeta));
    } else if (isInserting) {
      context.missing(_offerPriceMeta);
    }
    if (data.containsKey('subTotal')) {
      context.handle(_subTotalMeta,
          subTotal.isAcceptableOrUnknown(data['subTotal']!, _subTotalMeta));
    } else if (isInserting) {
      context.missing(_subTotalMeta);
    }
    if (data.containsKey('total')) {
      context.handle(
          _totalMeta, total.isAcceptableOrUnknown(data['total']!, _totalMeta));
    } else if (isInserting) {
      context.missing(_totalMeta);
    }
    if (data.containsKey('quantity')) {
      context.handle(_quantityMeta,
          quantity.isAcceptableOrUnknown(data['quantity']!, _quantityMeta));
    } else if (isInserting) {
      context.missing(_quantityMeta);
    }
    if (data.containsKey('comment')) {
      context.handle(_commentMeta,
          comment.isAcceptableOrUnknown(data['comment']!, _commentMeta));
    } else if (isInserting) {
      context.missing(_commentMeta);
    }
    if (data.containsKey('product_type')) {
      context.handle(
          _productTypeMeta,
          productType.isAcceptableOrUnknown(
              data['product_type']!, _productTypeMeta));
    } else if (isInserting) {
      context.missing(_productTypeMeta);
    }
    if (data.containsKey('print_order')) {
      context.handle(
          _printOrderMeta,
          printOrder.isAcceptableOrUnknown(
              data['print_order']!, _printOrderMeta));
    } else if (isInserting) {
      context.missing(_printOrderMeta);
    }
    if (data.containsKey('print_order')) {
      context.handle(
          _isSubmittedMeta,
          isSubmitted.isAcceptableOrUnknown(
              data['print_order']!, _isSubmittedMeta));
    } else if (isInserting) {
      context.missing(_isSubmittedMeta);
    }
    if (data.containsKey('discountable')) {
      context.handle(
          _discountableMeta,
          discountable.isAcceptableOrUnknown(
              data['discountable']!, _discountableMeta));
    } else if (isInserting) {
      context.missing(_discountableMeta);
    }
    if (data.containsKey('offerAble')) {
      context.handle(_offerAbleMeta,
          offerAble.isAcceptableOrUnknown(data['offerAble']!, _offerAbleMeta));
    } else if (isInserting) {
      context.missing(_offerAbleMeta);
    }
    if (data.containsKey('offered')) {
      context.handle(_offeredMeta,
          offered.isAcceptableOrUnknown(data['offered']!, _offeredMeta));
    } else if (isInserting) {
      context.missing(_offeredMeta);
    }
    if (data.containsKey('kitchen_item')) {
      context.handle(
          _kitchenItemMeta,
          kitchenItem.isAcceptableOrUnknown(
              data['kitchen_item']!, _kitchenItemMeta));
    } else if (isInserting) {
      context.missing(_kitchenItemMeta);
    }
    if (data.containsKey('category')) {
      context.handle(_categoryMeta,
          category.isAcceptableOrUnknown(data['category']!, _categoryMeta));
    } else if (isInserting) {
      context.missing(_categoryMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  CartItem map(Map<String, dynamic> data, {String? tablePrefix}) {
    return CartItem.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbCartItemTable createAlias(String alias) {
    return $TbCartItemTable(attachedDatabase, alias);
  }
}

class CartComponent extends DataClass implements Insertable<CartComponent> {
  final String id;
  final String uuid;
  final String shortName;
  final String description;
  final String price;
  final String relationType;
  final String relationGroup;
  CartComponent(
      {required this.id,
      required this.uuid,
      required this.shortName,
      required this.description,
      required this.price,
      required this.relationType,
      required this.relationGroup});
  factory CartComponent.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return CartComponent(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      uuid: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_uuid'])!,
      shortName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}short_name'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      price: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}price'])!,
      relationType: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}relation_type'])!,
      relationGroup: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}relation_group'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['product_uuid'] = Variable<String>(uuid);
    map['short_name'] = Variable<String>(shortName);
    map['description'] = Variable<String>(description);
    map['price'] = Variable<String>(price);
    map['relation_type'] = Variable<String>(relationType);
    map['relation_group'] = Variable<String>(relationGroup);
    return map;
  }

  TbCartComponentCompanion toCompanion(bool nullToAbsent) {
    return TbCartComponentCompanion(
      id: Value(id),
      uuid: Value(uuid),
      shortName: Value(shortName),
      description: Value(description),
      price: Value(price),
      relationType: Value(relationType),
      relationGroup: Value(relationGroup),
    );
  }

  factory CartComponent.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CartComponent(
      id: serializer.fromJson<String>(json['id']),
      uuid: serializer.fromJson<String>(json['uuid']),
      shortName: serializer.fromJson<String>(json['shortName']),
      description: serializer.fromJson<String>(json['description']),
      price: serializer.fromJson<String>(json['price']),
      relationType: serializer.fromJson<String>(json['relationType']),
      relationGroup: serializer.fromJson<String>(json['relationGroup']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'uuid': serializer.toJson<String>(uuid),
      'shortName': serializer.toJson<String>(shortName),
      'description': serializer.toJson<String>(description),
      'price': serializer.toJson<String>(price),
      'relationType': serializer.toJson<String>(relationType),
      'relationGroup': serializer.toJson<String>(relationGroup),
    };
  }

  CartComponent copyWith(
          {String? id,
          String? uuid,
          String? shortName,
          String? description,
          String? price,
          String? relationType,
          String? relationGroup}) =>
      CartComponent(
        id: id ?? this.id,
        uuid: uuid ?? this.uuid,
        shortName: shortName ?? this.shortName,
        description: description ?? this.description,
        price: price ?? this.price,
        relationType: relationType ?? this.relationType,
        relationGroup: relationGroup ?? this.relationGroup,
      );
  @override
  String toString() {
    return (StringBuffer('CartComponent(')
          ..write('id: $id, ')
          ..write('uuid: $uuid, ')
          ..write('shortName: $shortName, ')
          ..write('description: $description, ')
          ..write('price: $price, ')
          ..write('relationType: $relationType, ')
          ..write('relationGroup: $relationGroup')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, uuid, shortName, description, price, relationType, relationGroup);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CartComponent &&
          other.id == this.id &&
          other.uuid == this.uuid &&
          other.shortName == this.shortName &&
          other.description == this.description &&
          other.price == this.price &&
          other.relationType == this.relationType &&
          other.relationGroup == this.relationGroup);
}

class TbCartComponentCompanion extends UpdateCompanion<CartComponent> {
  final Value<String> id;
  final Value<String> uuid;
  final Value<String> shortName;
  final Value<String> description;
  final Value<String> price;
  final Value<String> relationType;
  final Value<String> relationGroup;
  const TbCartComponentCompanion({
    this.id = const Value.absent(),
    this.uuid = const Value.absent(),
    this.shortName = const Value.absent(),
    this.description = const Value.absent(),
    this.price = const Value.absent(),
    this.relationType = const Value.absent(),
    this.relationGroup = const Value.absent(),
  });
  TbCartComponentCompanion.insert({
    required String id,
    required String uuid,
    required String shortName,
    required String description,
    required String price,
    required String relationType,
    required String relationGroup,
  })  : id = Value(id),
        uuid = Value(uuid),
        shortName = Value(shortName),
        description = Value(description),
        price = Value(price),
        relationType = Value(relationType),
        relationGroup = Value(relationGroup);
  static Insertable<CartComponent> custom({
    Expression<String>? id,
    Expression<String>? uuid,
    Expression<String>? shortName,
    Expression<String>? description,
    Expression<String>? price,
    Expression<String>? relationType,
    Expression<String>? relationGroup,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (uuid != null) 'product_uuid': uuid,
      if (shortName != null) 'short_name': shortName,
      if (description != null) 'description': description,
      if (price != null) 'price': price,
      if (relationType != null) 'relation_type': relationType,
      if (relationGroup != null) 'relation_group': relationGroup,
    });
  }

  TbCartComponentCompanion copyWith(
      {Value<String>? id,
      Value<String>? uuid,
      Value<String>? shortName,
      Value<String>? description,
      Value<String>? price,
      Value<String>? relationType,
      Value<String>? relationGroup}) {
    return TbCartComponentCompanion(
      id: id ?? this.id,
      uuid: uuid ?? this.uuid,
      shortName: shortName ?? this.shortName,
      description: description ?? this.description,
      price: price ?? this.price,
      relationType: relationType ?? this.relationType,
      relationGroup: relationGroup ?? this.relationGroup,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (uuid.present) {
      map['product_uuid'] = Variable<String>(uuid.value);
    }
    if (shortName.present) {
      map['short_name'] = Variable<String>(shortName.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (price.present) {
      map['price'] = Variable<String>(price.value);
    }
    if (relationType.present) {
      map['relation_type'] = Variable<String>(relationType.value);
    }
    if (relationGroup.present) {
      map['relation_group'] = Variable<String>(relationGroup.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbCartComponentCompanion(')
          ..write('id: $id, ')
          ..write('uuid: $uuid, ')
          ..write('shortName: $shortName, ')
          ..write('description: $description, ')
          ..write('price: $price, ')
          ..write('relationType: $relationType, ')
          ..write('relationGroup: $relationGroup')
          ..write(')'))
        .toString();
  }
}

class $TbCartComponentTable extends TbCartComponent
    with TableInfo<$TbCartComponentTable, CartComponent> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TbCartComponentTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String?> uuid = GeneratedColumn<String?>(
      'product_uuid', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _shortNameMeta = const VerificationMeta('shortName');
  @override
  late final GeneratedColumn<String?> shortName = GeneratedColumn<String?>(
      'short_name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _priceMeta = const VerificationMeta('price');
  @override
  late final GeneratedColumn<String?> price = GeneratedColumn<String?>(
      'price', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _relationTypeMeta =
      const VerificationMeta('relationType');
  @override
  late final GeneratedColumn<String?> relationType = GeneratedColumn<String?>(
      'relation_type', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _relationGroupMeta =
      const VerificationMeta('relationGroup');
  @override
  late final GeneratedColumn<String?> relationGroup = GeneratedColumn<String?>(
      'relation_group', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, uuid, shortName, description, price, relationType, relationGroup];
  @override
  String get aliasedName => _alias ?? 'cart_component';
  @override
  String get actualTableName => 'cart_component';
  @override
  VerificationContext validateIntegrity(Insertable<CartComponent> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('product_uuid')) {
      context.handle(_uuidMeta,
          uuid.isAcceptableOrUnknown(data['product_uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('short_name')) {
      context.handle(_shortNameMeta,
          shortName.isAcceptableOrUnknown(data['short_name']!, _shortNameMeta));
    } else if (isInserting) {
      context.missing(_shortNameMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price']!, _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    if (data.containsKey('relation_type')) {
      context.handle(
          _relationTypeMeta,
          relationType.isAcceptableOrUnknown(
              data['relation_type']!, _relationTypeMeta));
    } else if (isInserting) {
      context.missing(_relationTypeMeta);
    }
    if (data.containsKey('relation_group')) {
      context.handle(
          _relationGroupMeta,
          relationGroup.isAcceptableOrUnknown(
              data['relation_group']!, _relationGroupMeta));
    } else if (isInserting) {
      context.missing(_relationGroupMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  CartComponent map(Map<String, dynamic> data, {String? tablePrefix}) {
    return CartComponent.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $TbCartComponentTable createAlias(String alias) {
    return $TbCartComponentTable(attachedDatabase, alias);
  }
}

abstract class _$AppDb extends GeneratedDatabase {
  _$AppDb(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  _$AppDb.connect(DatabaseConnection c) : super.connect(c);
  late final $TbProductTable tbProduct = $TbProductTable(this);
  late final $TbProductPropertyTable tbProductProperty =
      $TbProductPropertyTable(this);
  late final $TbProductPriceTable tbProductPrice = $TbProductPriceTable(this);
  late final $TbConfigProductPropertyTable tbConfigProductProperty =
      $TbConfigProductPropertyTable(this);
  late final $TbCustomerTable tbCustomer = $TbCustomerTable(this);
  late final $TbCustomerAddressTable tbCustomerAddress =
      $TbCustomerAddressTable(this);
  late final $TbCartItemTable tbCartItem = $TbCartItemTable(this);
  late final $TbCartComponentTable tbCartComponent =
      $TbCartComponentTable(this);
  late final ProductDao productDao = ProductDao(this as AppDb);
  late final CustomerDao customerDao = CustomerDao(this as AppDb);
  late final CartDao cartDao = CartDao(this as AppDb);
  Selectable<FetchProductByCategoryResult> fetchProductByCategory(String var1) {
    return customSelect(
        'SELECT * FROM product AS P, product_property AS PP, product_price AS PR WHERE P.id = PR.product_uuid AND P.id = PP.product_uuid AND PP.key_name = \'categories_epos\' AND PP.key_value = ?',
        variables: [
          Variable<String>(var1)
        ],
        readsFrom: {
          tbProduct,
          tbProductProperty,
          tbProductPrice,
        }).map((QueryRow row) {
      return FetchProductByCategoryResult(
        id: row.read<String>('id'),
        shortName: row.read<String>('short_name'),
        productType: row.read<String>('product_type'),
        description: row.read<String>('description'),
        status: row.read<String>('status'),
        visible: row.read<bool>('visible'),
        discountable: row.read<bool>('discountable'),
        creatorProviderUuid: row.read<String>('creator_provider_uuid'),
        language: row.read<String>('language'),
        isSubmitted: row.read<bool>('isSubmitted'),
        id1: row.read<String>('id'),
        productUuid: row.read<String>('product_uuid'),
        keyName: row.read<String>('key_name'),
        keyValue: row.read<String>('key_value'),
        id2: row.read<String>('id'),
        productUuid1: row.read<String>('product_uuid'),
        price: row.read<String?>('price'),
        extraPrice: row.read<String?>('extra_price'),
        vat: row.read<String>('vat'),
        currency: row.read<String>('currency'),
      );
    });
  }

  Future<int> deleteAllProduct() {
    return customUpdate(
      'DELETE FROM product',
      variables: [],
      updates: {tbProduct},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllProductPrice() {
    return customUpdate(
      'DELETE FROM product_price',
      variables: [],
      updates: {tbProductPrice},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllProductProperty() {
    return customUpdate(
      'DELETE FROM product_property',
      variables: [],
      updates: {tbProductProperty},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteCategoryById(String var1) {
    return customUpdate(
      'Delete FROM config_product_property WHERE id = ?',
      variables: [Variable<String>(var1)],
      updates: {tbConfigProductProperty},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllCategories() {
    return customUpdate(
      'DELETE FROM config_product_property',
      variables: [],
      updates: {tbConfigProductProperty},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllCustomer() {
    return customUpdate(
      'DELETE FROM customer',
      variables: [],
      updates: {tbCustomer},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllCustomerAddress() {
    return customUpdate(
      'DELETE FROM customer_address',
      variables: [],
      updates: {tbCustomerAddress},
      updateKind: UpdateKind.delete,
    );
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        tbProduct,
        tbProductProperty,
        tbProductPrice,
        tbConfigProductProperty,
        tbCustomer,
        tbCustomerAddress,
        tbCartItem,
        tbCartComponent
      ];
}

class FetchProductByCategoryResult {
  final String id;
  final String shortName;
  final String productType;
  final String description;
  final String status;
  final bool visible;
  final bool discountable;
  final String creatorProviderUuid;
  final String language;
  final bool isSubmitted;
  final String id1;
  final String productUuid;
  final String keyName;
  final String keyValue;
  final String id2;
  final String productUuid1;
  final String? price;
  final String? extraPrice;
  final String vat;
  final String currency;
  FetchProductByCategoryResult({
    required this.id,
    required this.shortName,
    required this.productType,
    required this.description,
    required this.status,
    required this.visible,
    required this.discountable,
    required this.creatorProviderUuid,
    required this.language,
    required this.isSubmitted,
    required this.id1,
    required this.productUuid,
    required this.keyName,
    required this.keyValue,
    required this.id2,
    required this.productUuid1,
    this.price,
    this.extraPrice,
    required this.vat,
    required this.currency,
  });
}
