import 'dart:convert';

import 'package:drift/drift.dart';

import '../../remote_db/services/product/dto/category.response.dart';

class PropertyItemConverter extends TypeConverter<List<CategoryItem>, String> {
  const PropertyItemConverter();

  @override
  List<CategoryItem>? mapToDart(String? fromDb) {
    return fromDb == null ? null : (json.decode(fromDb)).map((i) => CategoryItem.fromJson(i)).toList();
  }

  @override
  String? mapToSql(List<CategoryItem>? value) {
    return json.encode(value!.map((e) => e.toJson()).toList());
  }
}