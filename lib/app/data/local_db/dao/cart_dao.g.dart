// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CartDaoMixin on DatabaseAccessor<AppDb> {
  $TbCartItemTable get tbCartItem => attachedDatabase.tbCartItem;
  $TbCartComponentTable get tbCartComponent => attachedDatabase.tbCartComponent;
}
