import 'package:drift/drift.dart';
import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:rxdart/rxdart.dart';

import '../app_db.dart';
import '../models/product_entry.model.dart';
import '../tables/tb_config_product_property.dart';
import '../tables/tb_product.dart';
import '../tables/tb_product_price.dart';
import '../tables/tb_product_property.dart';
import 'package:drift/drift.dart' as drift;

part 'product_dao.g.dart';

@DriftAccessor(tables: [
  TbProduct,
  TbProductProperty,
  TbProductPrice,
  TbConfigProductProperty
])
class ProductDao extends DatabaseAccessor<AppDb> with _$ProductDaoMixin {
  ProductDao(AppDb db) : super(db);

  // products
  // addProduct(TbProductCompanion entity) => into(tbProduct).insert(entity);
  addAllProducts(List<TbProductCompanion> entitys) =>
      batch((batch) => batch.insertAll(tbProduct, entitys));

  Future<List<Product>> getAllProducts() => select(tbProduct).get();

  Stream<List<Product>> watchAllProducts() => select(tbProduct).watch();

  // Stream<List<Product>> watchAllProducts() {
  //   final query = select(tbProduct).join([
  //     innerJoin(tbProductPrice, tbProductPrice.productUuid.equalsExp(tbProduct.id)),
  //   ]);
  //
  //   query.watch().map((rows) {
  //     return
  //   }).toList();
  //   return
  // };

  Future<List<ProductPrice>> getAllProductPrice() {
    return select(tbProductPrice).get();
  }

  Future<List<ProductProperty>> getAllProductProperties() {
    return select(tbProductProperty).get();
  }

  Future<dynamic> addProduct(ProductEntry entry) async {
    var productEntry = TbProductCompanion(
      id: drift.Value(entry.id),
      shortName: drift.Value(entry.shortName),
      productType: drift.Value(entry.productType),
      description: drift.Value(entry.description),
      status: drift.Value(entry.status),
      visible: drift.Value(entry.visible),
      discountable: drift.Value(entry.discountable),
      creatorProviderUuid: drift.Value(entry.providerId),
      language: drift.Value(entry.language),
      isSubmitted: drift.Value(entry.isSubmitted),
    );

    var propertyEntries = entry.property
        .map((PropertyEntry property) => TbProductPropertyCompanion(
              id: drift.Value(property.id),
              productUuid: drift.Value(property.productUuid),
              keyName: drift.Value(property.keyName),
              keyValue: drift.Value(property.keyValue),
            ))
        .toList();

    into(tbProduct).insert(productEntry).then((value) {
      addAllProductProperty(propertyEntries);

      into(tbProductPrice).insert(TbProductPriceCompanion(
        id: drift.Value(entry.price!.id),
        productUuid: drift.Value(entry.price!.productUuid),
        price: drift.Value(entry.price?.price),
        extraPrice: drift.Value(entry.price?.extraPrice),
        vat: drift.Value(entry.price!.vat),
        currency: drift.Value(entry.price!.currency),
      ));
    });
  }

  //product property
  addAllProductProperty(List<TbProductPropertyCompanion> entities) =>
      batch((batch) => batch.insertAll(tbProductProperty, entities));

  // categories
  addCategory(TbConfigProductPropertyCompanion entity) =>
      into(tbConfigProductProperty).insert(entity);

  updateCategory(TbConfigProductPropertyCompanion entity) =>
      update(tbConfigProductProperty).replace(entity);

  deleteCategory(String id) => db.deleteCategoryById(id);

  Future<List<ConfigProductProperty>> getAllCategory() =>
      select(tbConfigProductProperty).get();

  Stream<List<ConfigProductProperty>> watchAllCategory() =>
      (select(tbConfigProductProperty)
            ..orderBy([(t) => OrderingTerm(expression: t.sortOrder)]))
          .watch();
}
