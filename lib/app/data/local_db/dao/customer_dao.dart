import 'package:drift/drift.dart';
import 'package:epos_system/app/data/local_db/tables/tb_customer.dart';
import 'package:epos_system/app/data/local_db/tables/tb_customer_address.dart';

import '../app_db.dart';

part 'customer_dao.g.dart';

@DriftAccessor(
  tables: [TbCustomer, TbCustomerAddress],
)
class CustomerDao extends DatabaseAccessor<AppDb> with _$CustomerDaoMixin {
  CustomerDao(AppDb db) : super(db);

  Future<List<Customer>> getAllCustomer() => select(tbCustomer).get();

  Stream<List<Customer>> watchAllCustomer() => select(tbCustomer).watch();

  // customerInfo
  addCustomer(TbCustomerCompanion entity) =>
      into(tbCustomer).insert(entity);
  // customerAddresses
  addCustomerAddress(TbCustomerAddressCompanion entity) =>
      into(tbCustomerAddress).insert(entity);

  deleteCustomer() => db.deleteAllCustomer();
  deleteCustomerAddress() => db.deleteAllCustomerAddress();
}
