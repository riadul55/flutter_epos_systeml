import 'package:drift/drift.dart';
import 'package:epos_system/app/data/local_db/tables/tb_cart_item.dart';
import 'package:epos_system/app/data/local_db/tables/tb_cart_component.dart';

import '../app_db.dart';

part 'cart_dao.g.dart';

@DriftAccessor(
  tables: [TbCartItem, TbCartComponent],
)
class CartDao extends DatabaseAccessor<AppDb> with _$CartDaoMixin {
  CartDao(AppDb db) : super(db);

// cartItem
  addCartItem(TbCartItemCompanion entity) =>
      into(tbCartItem).insert(entity);

// cartComponent
  addCartComponent(TbCartComponentCompanion entity) =>
      into(tbCartComponent).insert(entity);
}