// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ProductDaoMixin on DatabaseAccessor<AppDb> {
  $TbProductTable get tbProduct => attachedDatabase.tbProduct;
  $TbProductPropertyTable get tbProductProperty =>
      attachedDatabase.tbProductProperty;
  $TbProductPriceTable get tbProductPrice => attachedDatabase.tbProductPrice;
  $TbConfigProductPropertyTable get tbConfigProductProperty =>
      attachedDatabase.tbConfigProductProperty;
}
