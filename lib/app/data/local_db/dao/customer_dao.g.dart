// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CustomerDaoMixin on DatabaseAccessor<AppDb> {
  $TbCustomerTable get tbCustomer => attachedDatabase.tbCustomer;
  $TbCustomerAddressTable get tbCustomerAddress =>
      attachedDatabase.tbCustomerAddress;
}
