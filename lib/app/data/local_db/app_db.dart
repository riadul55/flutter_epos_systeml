import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:epos_system/app/data/local_db/custom_queries.dart';
import 'package:epos_system/app/data/local_db/tables/tb_cart_component.dart';
import 'package:epos_system/app/data/local_db/tables/tb_cart_item.dart';
import 'package:epos_system/app/data/local_db/tables/tb_customer.dart';
import 'package:epos_system/app/data/local_db/tables/tb_customer_address.dart';
import 'package:epos_system/app/data/local_db/tables/tb_global_parameter.dart';
import '/app/data/local_db/dao/product_dao.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

import 'dao/cart_dao.dart';
import 'dao/customer_dao.dart';
import 'tables/tb_config_product_property.dart';
import 'tables/tb_product.dart';
import 'tables/tb_product_price.dart';
import 'tables/tb_product_property.dart';

part 'app_db.g.dart';

//Don't delete this command => flutter packages pub run build_runner build/watch
//Don't delete this command => flutter packages pub run build_runner build --delete-conflicting-outputs
@DriftDatabase(tables: [
  TbProduct,
  TbProductProperty,
  TbProductPrice,
  TbConfigProductProperty,
  TbCustomer,
  TbCustomerAddress,
  TbCartItem,
  TbCartComponent
], daos: [
  ProductDao,
  CustomerDao,
  CartDao,
], queries: customQueries)
class AppDb extends _$AppDb {
  // we tell the database where to store the data with this constructor
  AppDb(QueryExecutor executor) : super(executor);

  AppDb.connect(DatabaseConnection connection) : super.connect(connection);

  // you should bump this number whenever you change or add a table definition. Migrations
  // are covered later in this readme.
  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      onCreate: (Migrator m) {
        return m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {},
      beforeOpen: (details) async {},
    );
  }
}

AppDb openConnection({bool logStatements = false}) {
  if (Platform.isIOS || Platform.isAndroid) {
    final executor = LazyDatabase(() async {
      final dataDir = await getApplicationDocumentsDirectory();
      final dbFile = File(p.join(dataDir.path, 'epos-db.sqlite'));
      return NativeDatabase(dbFile, logStatements: logStatements);
    });
    return AppDb(executor);
  }
  return AppDb(NativeDatabase.memory(logStatements: logStatements));
}
