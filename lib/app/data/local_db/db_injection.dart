
import 'package:get_it/get_it.dart';

import 'app_db.dart';

final getIt = GetIt.instance;

Future<void> init() async {
  getIt.registerSingleton<AppDb>(openConnection());
}