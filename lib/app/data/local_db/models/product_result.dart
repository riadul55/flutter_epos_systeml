import 'package:epos_system/app/data/local_db/app_db.dart';

class ProductResult {
  final String? id;
  final String? shortName;
  final String? productType;
  final String? description;
  final String? status;
  final bool? visible;
  final bool? discountable;
  final String? creatorProviderUuid;
  final String? language;
  final bool? isSubmitted;
  final Properties? properties;
  final Price? price;

  ProductResult({
    this.id,
    this.shortName,
    this.productType,
    this.description,
    this.status,
    this.visible,
    this.discountable,
    this.creatorProviderUuid,
    this.language,
    this.isSubmitted,
    this.properties,
    this.price,
  });

  static ProductResult fromData(FetchProductByCategoryResult fetchData, List<ProductProperty> propertyList) {
    var propertyMap = <String, dynamic>{};
    for (ProductProperty property in propertyList) {
      propertyMap[property.keyName] = property.keyValue;
    }
    var properties = Properties.fromJson(propertyMap);

    double price = 0.0;
    if (fetchData.price != null) {
      price = double.parse("${fetchData.price}");
    } else if (fetchData.extraPrice != null) {
      price = double.parse("${fetchData.extraPrice}");
    }
    var priceObj = Price(
      fetchData.id2,
      fetchData.productUuid,
      price,
      fetchData.vat,
      fetchData.currency,
    );
    return ProductResult(
        id: fetchData.id,
        shortName: fetchData.shortName,
        productType: fetchData.productType,
        description: fetchData.description,
        status: fetchData.status,
        visible: fetchData.visible,
        discountable: fetchData.discountable,
        creatorProviderUuid: fetchData.creatorProviderUuid,
        language: fetchData.language,
        isSubmitted: fetchData.isSubmitted,
        properties: properties,
        price: priceObj,
    );
  }
}

class Price {
  final String id;
  final String productUuid;
  final double price;
  final String vat;
  final String currency;

  Price(this.id, this.productUuid, this.price, this.vat, this.currency);
}

class Properties {
/*
{
  "bundle_group": "Main Dish",
  "available_on": "both",
  "D": "false",
  "sell_on_its_own": "1",
  "G": "false",
  "available": "1",
  "print_order": "3",
  "print_item_component_order": "1",
  "N": "false",
  "spiced": "false",
  "offer_price": "0",
  "VE": "false",
  "offer": "1",
  "categories_epos": "classicdishes",
  "V": "false",
  "categories_key": "",
  "review_enabled": "0",
  "categories": "classicdishes",
  "sort_order": "33"
}
*/

  String? bundleGroup;
  String? availableOn;
  String? D;
  String? sellOnItsOwn;
  String? G;
  String? available;
  String? printOrder;
  String? printItemComponentOrder;
  String? N;
  String? spiced;
  String? offerPrice;
  String? VE;
  String? offer;
  String? categoriesEpos;
  String? V;
  String? categoriesKey;
  String? reviewEnabled;
  String? categories;
  String? sortOrder;

  Properties({
    this.bundleGroup,
    this.availableOn,
    this.D,
    this.sellOnItsOwn,
    this.G,
    this.available,
    this.printOrder,
    this.printItemComponentOrder,
    this.N,
    this.spiced,
    this.offerPrice,
    this.VE,
    this.offer,
    this.categoriesEpos,
    this.V,
    this.categoriesKey,
    this.reviewEnabled,
    this.categories,
    this.sortOrder,
  });
  Properties.fromJson(Map<String, dynamic> json) {
    bundleGroup = json['bundle_group']?.toString();
    availableOn = json['available_on']?.toString();
    D = json['D']?.toString();
    sellOnItsOwn = json['sell_on_its_own']?.toString();
    G = json['G']?.toString();
    available = json['available']?.toString();
    printOrder = json['print_order']?.toString();
    printItemComponentOrder = json['print_item_component_order']?.toString();
    N = json['N']?.toString();
    spiced = json['spiced']?.toString();
    offerPrice = json['offer_price']?.toString();
    VE = json['VE']?.toString();
    offer = json['offer']?.toString();
    categoriesEpos = json['categories_epos']?.toString();
    V = json['V']?.toString();
    categoriesKey = json['categories_key']?.toString();
    reviewEnabled = json['review_enabled']?.toString();
    categories = json['categories']?.toString();
    sortOrder = json['sort_order']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['bundle_group'] = bundleGroup;
    data['available_on'] = availableOn;
    data['D'] = D;
    data['sell_on_its_own'] = sellOnItsOwn;
    data['G'] = G;
    data['available'] = available;
    data['print_order'] = printOrder;
    data['print_item_component_order'] = printItemComponentOrder;
    data['N'] = N;
    data['spiced'] = spiced;
    data['offer_price'] = offerPrice;
    data['VE'] = VE;
    data['offer'] = offer;
    data['categories_epos'] = categoriesEpos;
    data['V'] = V;
    data['categories_key'] = categoriesKey;
    data['review_enabled'] = reviewEnabled;
    data['categories'] = categories;
    data['sort_order'] = sortOrder;
    return data;
  }
}