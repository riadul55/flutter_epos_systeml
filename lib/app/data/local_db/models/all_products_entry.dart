import '../tables/tb_product.dart';
import '../tables/tb_product_price.dart';
import '../tables/tb_product_property.dart';

class AllProductsEntry {
  final TbProduct product;
  final TbProductPrice? price;
  final List<TbProductProperty> properties;

  AllProductsEntry({
    required this.product,
    this.price,
    required this.properties,
  });
}
