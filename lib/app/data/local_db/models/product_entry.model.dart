class ProductEntry {
  String id;
  String shortName;
  String productType;
  String description;
  String status;
  bool visible;
  bool discountable;
  String providerId;
  String language;
  PriceEntry? price;
  List<PropertyEntry> property;
  bool isSubmitted;

  ProductEntry({
    required this.id,
    required this.shortName,
    required this.productType,
    required this.description,
    required this.status,
    required this.visible,
    required this.discountable,
    required this.providerId,
    required this.language,
    this.price,
    required this.property,
    required this.isSubmitted,
  });
}

class PropertyEntry {
  String id;
  String productUuid;
  String keyName;
  String keyValue;

  PropertyEntry({
    required this.id,
    required this.productUuid,
    required this.keyName,
    required this.keyValue,
  });
}

class PriceEntry {
  String id;
  String productUuid;
  String? price;
  String? extraPrice;
  String vat;
  String currency;

  PriceEntry({
    required this.id,
    required this.productUuid,
    this.price,
    this.extraPrice,
    required this.vat,
    required this.currency,
  });
}
