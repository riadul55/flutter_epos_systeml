import 'package:drift/drift.dart';

@DataClassName("GlobalParameter")
class TbGlobalParameter extends Table {
  TextColumn get businessName => text().withLength(min: 1, max: 50).named("business_name")();
  TextColumn get businessStatus => text().withLength(min: 1, max: 10).named("business_status")();
  TextColumn get businessAddress => text().withLength(min: 1, max: 50).named("business_address")();
  TextColumn get businessPostcode=> text().withLength(min: 1, max: 20).named("business_postcode")();
  TextColumn get businessEmail=> text().withLength(min: 1, max: 50).named("business_email")();
  TextColumn get businessPhone=> text().withLength(min: 1, max: 20).named("business_phone")();
  TextColumn get vat => text().withLength(min: 1, max: 10).named("vat")();

  @override
  String get tableName => 'global_parameter';
}