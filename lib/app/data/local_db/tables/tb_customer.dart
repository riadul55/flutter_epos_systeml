import 'package:drift/drift.dart';

@DataClassName("Customer")
class TbCustomer extends Table {
  TextColumn get id => text().named("id")();

  TextColumn get business =>
      text().nullable().named("business")();

  TextColumn get firstName =>
      text().nullable().named("first_name")();

  TextColumn get lastName =>
      text().nullable().named("last_name")();

  TextColumn get phone => text().nullable().named("phone")();

  TextColumn get email => text().nullable().named("email")();

  TextColumn get createdAt =>
      text().withLength(min: 1, max: 40).named("created_at")();

  TextColumn get updatedAt =>
      text().withLength(min: 1, max: 40).named("updated_at")();

  @override
  String get tableName => 'customer';
}
