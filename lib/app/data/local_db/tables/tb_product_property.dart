import 'package:drift/drift.dart';

@DataClassName("ProductProperty")
class TbProductProperty extends Table {
  TextColumn get id => text().named("id")();
  TextColumn get productUuid => text().named("product_uuid")();
  TextColumn get keyName => text().named("key_name")();
  TextColumn get keyValue => text().named("key_value")();

  @override
  String get tableName => 'product_property';
}