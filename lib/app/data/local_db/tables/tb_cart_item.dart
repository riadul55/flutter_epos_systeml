import 'package:drift/drift.dart';

@DataClassName("CartItem")
class TbCartItem extends Table {
  TextColumn get id => text().named("id")();
  TextColumn get localId => text().named("localId")();
  TextColumn get uuid => text().named("product_uuid")();
  TextColumn get shortName => text().named("short_name")();
  TextColumn get description => text().named("description")();
  TextColumn get price => text().named("price")();
  TextColumn get offerPrice => text().named("offerPrice")();
  TextColumn get subTotal => text().named("subTotal")();
  TextColumn get total => text().named("total")();
  IntColumn get quantity => integer().named("quantity")();
  TextColumn get comment => text().named("comment")();
  TextColumn get productType => text().named("product_type")();
  TextColumn get printOrder => text().named("print_order")();
  IntColumn get isSubmitted => integer().named("print_order")();
  TextColumn get discountable => text().named("discountable")();
  TextColumn get offerAble => text().named("offerAble")();
  TextColumn get offered => text().named("offered")();
  TextColumn get kitchenItem => text().named("kitchen_item")();
  TextColumn get category => text().named("category")();

  @override
  String get tableName => 'cart_item';
}