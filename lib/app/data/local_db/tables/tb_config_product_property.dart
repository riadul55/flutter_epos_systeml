import 'package:drift/drift.dart';

@DataClassName("ConfigProductProperty")
class TbConfigProductProperty extends Table {
  TextColumn get id => text().named("id")();
  TextColumn get platform => text().named("platform")();
  TextColumn get keyName => text().named("key_name")();
  TextColumn get value => text().named("value")();
  IntColumn get sortOrder => integer().named("sort_order")();
  TextColumn get displayName => text().named("display_name")();
  TextColumn get description => text().named("description")();
  TextColumn get filename => text().named("filename")();
  TextColumn get contentType => text().named("content_type")();
  BoolColumn get isSubmitted => boolean().named("isSubmitted")();

  @override
  String get tableName => 'config_product_property';
}