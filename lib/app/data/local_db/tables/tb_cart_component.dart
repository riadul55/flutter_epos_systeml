import 'package:drift/drift.dart';

@DataClassName("CartComponent")
class TbCartComponent extends Table {
  TextColumn get id => text().named("id")();
  TextColumn get uuid => text().named("product_uuid")();
  TextColumn get shortName => text().named("short_name")();
  TextColumn get description => text().named("description")();
  TextColumn get price => text().named("price")();
  TextColumn get relationType => text().named("relation_type")();
  TextColumn get relationGroup => text().named("relation_group")();

  @override
  String get tableName => 'cart_component';
}