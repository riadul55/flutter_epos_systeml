import 'package:drift/drift.dart';

@DataClassName("ProductPrice")
class TbProductPrice extends Table {
  TextColumn get id => text().named("id")();
  TextColumn get productUuid => text().named("product_uuid")();
  TextColumn get price => text().named("price").nullable()();
  TextColumn get extraPrice => text().named("extra_price").nullable()();
  TextColumn get vat => text().named("vat")();
  TextColumn get currency => text().named("currency")();

  @override
  String get tableName => 'product_price';
}