import 'package:drift/drift.dart';

@DataClassName("CustomerAddress")
class TbCustomerAddress extends Table {
  TextColumn get id => text().named("id")();

  TextColumn get onetimeConsumerId => text().named("onetime_consumer_id")();

  TextColumn get type => text().nullable().named("type")();

  TextColumn get streetName =>
      text().nullable().named("street_name")();

  TextColumn get building =>
      text().nullable().named("building")();

  TextColumn get city => text().nullable().named("city")();

  TextColumn get state => text().nullable().named("state")();

  TextColumn get zip => text().nullable().named("zip")();

  @override
  String get tableName => 'customer_address';
}
