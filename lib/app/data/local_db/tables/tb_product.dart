
import 'package:drift/drift.dart';

@DataClassName("Product")
class TbProduct extends Table {
  TextColumn get id => text().named("id")();
  TextColumn get shortName => text().withLength(min: 1, max: 100).named("short_name")();
  TextColumn get productType => text().withLength(min: 1, max: 45).named("product_type")();
  TextColumn get description => text().named("description")();
  TextColumn get status => text().withLength(min: 1, max: 10).named("status")();
  BoolColumn get visible => boolean().named("visible")();
  BoolColumn get discountable => boolean().named("discountable")();
  TextColumn get creatorProviderUuid => text().withLength(min: 1, max: 50).named("creator_provider_uuid")();
  TextColumn get language => text().withLength(min: 1, max: 5).named("language")();
  BoolColumn get isSubmitted => boolean().named("isSubmitted")();

  @override
  String get tableName => 'product';
}