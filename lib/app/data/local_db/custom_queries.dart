const customQueries = {
  'fetchProductByCategory' : "SELECT * FROM product AS P, product_property AS PP, product_price AS PR WHERE P.id = PR.product_uuid AND P.id = PP.product_uuid AND PP.key_name = 'categories_epos' AND PP.key_value = ?",
  'deleteAllProduct' : 'DELETE FROM product',
  'deleteAllProductPrice' : 'DELETE FROM product_price',
  'deleteAllProductProperty' : 'DELETE FROM product_property',
  'deleteCategoryById' : 'Delete FROM config_product_property WHERE id = ?',
  'deleteAllCategories' : 'DELETE FROM config_product_property',
  'deleteAllCustomer' : 'DELETE FROM customer',
  'deleteAllCustomerAddress' : 'DELETE FROM customer_address',
};