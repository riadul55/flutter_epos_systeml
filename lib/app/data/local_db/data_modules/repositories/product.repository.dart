import 'package:drift/drift.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:rxdart/rxdart.dart';
import '../../models/product_entry.model.dart';
import '../../models/product_result.dart';

class ProductRepository {
  final AppDb _appDb;

  ProductRepository({required AppDb appDb}) : _appDb = appDb;

  addProduct(ProductEntry entry) {
    return _appDb.productDao.addProduct(entry);
  }

  Stream<List<Product>> watchAllProducts() {
    return _appDb.productDao.watchAllProducts();
  }

  Future<List<ProductResult>> getProductsByCategory(String value) async {
    final productList = await _appDb.fetchProductByCategory(value).get();
    final propertyList = await _appDb.productDao.getAllProductProperties();
    List<ProductResult> resultList = <ProductResult>[];
    for (FetchProductByCategoryResult product in productList) {
      final properties = propertyList.where((element) => element.productUuid == product.id).toList();
      resultList.add(ProductResult.fromData(product, properties));
    }
    return resultList;
    // return CombineLatestStream.combine3(
    //     productStream, pricesStream, propertiesStream,
    //         (List<FetchProductByCategoryResult> a, List<ProductPrice> b, List<ProductProperty> c) {
    //       return a.map((product) {
    //         final prices = b.where((element) => element.productUuid == product.id).first;
    //         final properties = c.where((element) => element.productUuid == product.id).toList();
    //         return ProductResult.fromData(product, prices, properties);
    //       }).toList();
    //     });
  }

  void updateCategory(TbConfigProductPropertyCompanion table) {
    _appDb.productDao.updateCategory(table);
  }

  void addCategory(TbConfigProductPropertyCompanion table) {
    _appDb.productDao.addCategory(table);
  }

  Future<List<ConfigProductProperty>> getAllCategories() async {
    return await _appDb.productDao.getAllCategory();
  }

  Stream<List<ConfigProductProperty>> watchAllCategory() {
    return _appDb.productDao.watchAllCategory();
  }

  deleteAllProduct() async {
    await _appDb.deleteAllProduct();
    await _appDb.deleteAllProductPrice();
    await _appDb.deleteAllProductProperty();
  }

  deleteCategory(String id) {
    return _appDb.productDao.deleteCategory(id);
  }

  deleteAllCategories() async {
    return _appDb.deleteAllCategories();
  }
}