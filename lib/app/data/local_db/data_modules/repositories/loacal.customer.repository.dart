import 'package:drift/drift.dart';
import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/data/local_db/tables/tb_customer.dart';
import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:rxdart/rxdart.dart';
import '../../models/product_entry.model.dart';
import '../../models/product_result.dart';

class LocalCustomerRepository {
  final AppDb _appDb;

  LocalCustomerRepository({required AppDb appDb}) : _appDb = appDb;

  void addCustomer(TbCustomerCompanion table) {
    _appDb.customerDao.addCustomer(table);
  }
  Stream<List<Customer>> watchAllCustomer() {
    return _appDb.customerDao.watchAllCustomer();
  }
  Future<List<Customer>> getAllCustomer() async {
    return await _appDb.customerDao.getAllCustomer();
  }
  void addCustomerAddress(TbCustomerAddressCompanion table) {
    _appDb.customerDao.addCustomerAddress(table);
  }
  deleteCustomer() async {
    return _appDb.deleteAllCustomer();
  }
  deleteCustomerAddress() async {
    return _appDb.deleteAllCustomerAddress();
  }
}