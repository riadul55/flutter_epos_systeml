
import '../../app_db.dart';
import '../../db_injection.dart';
import '../repositories/product.repository.dart';

class ProductRepositoryBinding {
  late ProductRepository _productRepository;

  ProductRepository get repository => _productRepository;

  ProductRepositoryBinding() {
    AppDb appDb = getIt();
    _productRepository = ProductRepository(appDb: appDb);
  }
}