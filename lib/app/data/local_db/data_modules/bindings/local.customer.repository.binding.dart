
import '../../app_db.dart';
import '../../db_injection.dart';
import '../repositories/loacal.customer.repository.dart';

class LocalCustomerRepositoryBinding {
  late LocalCustomerRepository _customerRepository;

  LocalCustomerRepository get repository => _customerRepository;

  LocalCustomerRepositoryBinding() {
    AppDb appDb = getIt();
    _customerRepository = LocalCustomerRepository(appDb: appDb);
  }
}