import '../../app_db.dart';
import '../../db_injection.dart';
import '../repositories/order.repository.dart';

class OrderRepositoryBinding {
  late OrderRepository _orderRepository;

  OrderRepository get repository => _orderRepository;

  OrderRepositoryBinding() {
    AppDb appDb = getIt();
    _orderRepository = OrderRepository(appDb: appDb);
  }
}