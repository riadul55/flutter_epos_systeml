import 'package:epos_system/app/data/remote_db/services/global_parameter/dto/global.parameter.response.dart';
import 'package:get/get.dart';

import '../../../../../config/config.dart';
import '../../../../modules/globals/log_info.dart';

class GlobalParameterService {
  final GetConnect _connect;

  const GlobalParameterService(GetConnect connect) : _connect = connect;

  String get prefix => "/delivery";

  Future<GlobalParameterResponse> getGlobalParameterData() async {
    final env = ConfigEnvironments.getEnvironments();
    return await _connect.get(
      "$prefix/config/global/parameter",
      headers: {
        "providersession": "${env.providerSession}",
        "AdminSession": "${env.adminSession}",
      },
    ).then((response) {
      if (response.statusCode == 200) {
        logData("Global api response code ==>${response.statusCode}");
        return GlobalParameterResponse.fromJson(response.body);
      } else {
        return GlobalParameterResponse.withError({"message": "error"});
      }
    }).onError((error, stackTrace) {
      print("onError==>$error");
      return GlobalParameterResponse.withError({"message": error.toString()});
    }).catchError((error, stackTrace) {
      print("catchError==>$error");
      return GlobalParameterResponse.withError({"message": error.toString()});
    }).timeout(_connect.timeout);
  }


  Future<GlobalParameterResponse> updateGlobalParameterData(Map requestBody) async {
    final env = ConfigEnvironments.getEnvironments();
    return await _connect.post(
      "$prefix/config/global/parameter",
      requestBody,
      headers: {
        "providersession": "${env.providerSession}",
        "AdminSession": "${env.adminSession}",
      },
    ).then((response) {
      if (response.statusCode == 200) {
        logData("${response.statusCode}");
        return GlobalParameterResponse.fromJson(response.body);
      } else {
        return GlobalParameterResponse.withError({"message": "error"});
      }
    }).onError((error, stackTrace) {
      logError("onError==>$error");
      return GlobalParameterResponse.withError({"message": error.toString()});
    }).catchError((error, stackTrace) {
      logError("catchError==>$error");
      return GlobalParameterResponse.withError({"message": error.toString()});
    }).timeout(_connect.timeout);
  }
}
