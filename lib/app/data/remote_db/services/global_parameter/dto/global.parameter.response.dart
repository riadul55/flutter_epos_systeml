class GlobalParameterResponse {
  GlobalParameter? globalParameter;
  ErrorResponse? errorResponse;

  GlobalParameterResponse(
      {this.globalParameter, this.errorResponse});

  GlobalParameterResponse.fromJson(Map<String, dynamic> json)
      : globalParameter = GlobalParameter.fromJson(json),
        errorResponse = null;

  GlobalParameterResponse.withError(Map<String, dynamic> json)
      : globalParameter = null,
        errorResponse = ErrorResponse.fromJson(json);
}


class GlobalParameter {
/*
{
  "business_name": "Redmango",
  "business_status": "open",
  "business_postcode": "LU3 1AW",
  "business_address": "Biscot Road",
  // "business_address": "Sedgewick Road",
  "business_email": "muntasir@tech-novo.uk",
  "business_start_date": "2021-09-01",
  "business_phone": "01536760077",
  "maximum_delivery_area": "4",
  "delivery_option": "postcodeToPostcode",
  "delivery_charge": "3.50",
  "offday": "",
  "business_off_date": "2022-08-27",
  "opening_hour": "13:00:00",
  "closing_hour": "02:00:00",
  "openingTime_friday_to_saturday": "1:00pm to 11:00pm",
  "openingTime_monday_to_thursday": "1:00pm to 10:00pm",
  "openingTime_tuesday_to_thursday": "1:00pm to 10:00pm",
  "openingTime_sunday": "2:00pm to 10:00pm",
  "opening_time_saturday": "13:00:00",
  "opening_time_sunday": "14:00:00",
  "opening_time_monday": "13:00:00",
  "opening_time_tuesday": "13:00:00",
  "opening_time_wednesday": "13:00:00",
  "opening_time_thursday": "13:00:00",
  "opening_time_friday": "13:00:00",
  "closing_time_saturday": "23:00:00",
  "closing_time_sunday": "23:18:00",
  "closing_time_monday": "23:56:00",
  "closing_time_tuesday": "22:00:00",
  "closing_time_wednesday": "22:00:00",
  "closing_time_thursday": "22:00:00",
  "closing_time_friday": "23:00:00",
  "delivery_time": "17:00",
  "delivery_time_saturday": "13:00:00",
  "delivery_time_sunday": "14:00:00",
  "delivery_time_monday": "13:00:00",
  "delivery_time_tuesday": "13:00:00",
  "delivery_time_wednesday": "13:00:00",
  "delivery_time_thursday": "13:00",
  "delivery_time_friday": "14:00:00",
  "delivery_area_check_active": "1",
  "minimum_delivery": "0",
  "pickup_time": "13:00",
  "pickup_time_saturday": "13:00:00",
  "pickup_time_sunday": "14:00:00",
  "pickup_time_monday": "13:00:00",
  "pickup_time_tuesday": "13:00:00",
  "pickup_time_wednesday": "13:00:00",
  "pickup_time_thursday": "13:00",
  "pickup_time_friday": "13:00:00",
  "discount": "0,5",
  "discount_on_amount_saturday": "0",
  "discount_on_amount_sunday": "0",
  "discount_on_amount_monday": "1",
  "discount_on_amount_tuesday": "0",
  "discount_on_amount_wednesday": "0",
  "discount_on_amount_thursday": "1",
  "discount_on_amount_friday": "0",
  "discount_option": "1",
  "student_discount": "1",
  "min_amount_for_discount": "10",
  "new_customer_discount": "0",
  "nhsstaff_discount": "0",
  "discount_on_all_order": "0",
  "discount_on_amount": "1",
  "tuesday_discount": "1",
  "zuhri_discount": "1"
  "order_advance": "0",
  "meta_keywords_homepage": "Meta Keywords Homepage",
  "table_booking": "1",
  "meta_title_homepage": "Redmango",
  "meta_title": "Redmango",
  "meta_keywords": "Redmango, food, online food, Luton",
  "meta_title_offerpage": "Meta Title Offerpage",
  "meta_title_gallerypage": "Meta Title Gallerypage",
  "meta_description_menupage": "Meta Description Menupage",
  "meta_description_contactpage": "Meta Description Contactpage",
  "new_property": "a value",
  "delivery_outside_area_min_amount": "0",
  "order_advance_time": "0",
  "sms_service": "1",
  "fb_page_id": "104295695439770",
  "meta_title_menupage": "Meta Title Menupage",
  "meta_keywords_offerpage": "Meta Keywords Offerpage",
  "free_delivery_on_amount_active": "0",
  "dynamic_delivery_charge": "[{ key : below10 , value : 3.50 , min : 0 , max : 9.99 },{ key : 10to20 , value : 1.50 , min : 10 , max : 19.99 }]",
  "business_fb_link": "www.fb.com",
  "order_purging_duraton": "3",
  "meta_title_contactpage": "Meta | Title Contactpage",
  "extra_category_price": "0.6",
  "fb_chat_option": "0",
  "meta_description_gallerypage": "Meta Description Gallerypage",
  "meta_description_offerpage": "Meta Description Offerpage",
  "plastic_bag": "0",
  "free_delivery_on_amount": "15",
  "meta_keywords_gallerypage": "Meta Keywords Gallerypage",
  "meta_description_homepage": "Meta Description Homepage",
  "service_charge": "0.7",
  "meta_keywords_menupage": "Meta Keywords Menupage",
  "offday": "",
  "delivery_option": "postcodeToPostcode",
  "enable_all_item_comment": "1",
  "estimated_takeout_duration": "20",
  "estimated_delivery_duration": "40",
  "vat": "0",
  "show_advertise": "0",
  "meta_keywords_contactpage": "Meta Keywords Contactpage",
  "meta_description": "Online Ordering Platform",
  "vat_5": "5",
}
*/

  String? metaTitleHomepage;
  String? deliveryAreaCheckActive;
  String? discount;
  String? minimumDelivery;
  String? studentDiscount;
  String? enableAllItemComment;
  String? deliveryTimeSaturday;
  String? discountOnAmountTuesday;
  String? closingHour;
  String? estimatedTakeoutDuration;
  String? businessName;
  String? businessStatus;
  String? pickupTimeFriday;
  String? vat;
  String? showAdvertise;
  String? metaKeywordsContactpage;
  String? metaDescription;
  String? vat_5;
  String? pickupTimeThursday;
  String? businessEmail;
  String? pickupTime;
  String? openingTimeMondayToThursday;
  String? closingTimeMonday;
  String? orderAdvance;
  String? metaKeywordsHomepage;
  String? tableBooking;
  String? metaKeywords;
  String? openingTimeFridayToSaturday;
  String? metaTitleOfferpage;
  String? closingTimeSaturday;
  String? smsService;
  String? closingTimeSunday;
  String? closingTimeWednesday;
  String? discountOnAmountFriday;
  String? minAmountForDiscount;
  String? pickupTimeTuesday;
  String? fbPageId;
  String? metaTitleGallerypage;
  String? orderAdvanceTime;
  String? discountOnAmountWednesday;
  String? metaDescriptionMenupage;
  String? pickupTimeSaturday;
  String? metaDescriptionContactpage;
  String? closingTimeThursday;
  String? newProperty;
  String? deliveryOutsideAreaMinAmount;
  String? deliveryTimeTuesday;
  String? openingTimeWednesday;
  String? deliveryTimeMonday;
  String? pickupTimeSunday;
  String? openingTimeSunday;
  String? openingTimeTuesday;
  String? businessAddress;
  String? deliveryTimeFriday;
  String? metaTitleMenupage;
  String? openingHour;
  String? deliveryTime;
  String? maximumDeliveryArea;
  String? discountOption;
  String? openingTime_Sunday;
  String? metaKeywordsOfferpage;
  String? openingTimeMonday;
  String? openingTimeTuesdayToThursday;
  String? freeDeliveryOnAmountActive;
  String? discountOnAmountMonday;
  String? deliveryTimeWednesday;
  String? dynamicDeliveryCharge;
  String? businessFbLink;
  String? metaTitle;
  String? discountOnAmountThursday;
  String? deliveryTimeThursday;
  String? deliveryTimeSunday;
  String? openingTimeSaturday;
  String? openingTimeFriday;
  String? deliveryCharge;
  String? orderPurgingDuraton;
  String? metaTitleContactpage;
  String? closingTimeFriday;
  String? openingTimeThursday;
  String? businessStartDate;
  String? newCustomerDiscount;
  String? discountOnAmountSunday;
  String? nhsstaffDiscount;
  String? pickupTimeMonday;
  String? businessPostcode;
  String? extraCategoryPrice;
  String? businessPhone;
  String? fbChatOption;
  String? metaDescriptionGallerypage;
  String? estimatedDeliveryDuration;
  String? metaDescriptionOfferpage;
  String? plasticBag;
  String? discountOnAllOrder;
  String? discountOnAmount;
  String? tuesdayDiscount;
  String? freeDeliveryOnAmount;
  String? metaKeywordsGallerypage;
  String? metaDescriptionHomepage;
  String? pickupTimeWednesday;
  String? serviceCharge;
  String? metaKeywordsMenupage;
  String? closingTimeTuesday;
  String? offday;
  String? businessOffDate;
  String? discountOnAmountSaturday;
  String? deliveryOption;
  String? zuhriDiscount;

  GlobalParameter({
    this.metaTitleHomepage,
    this.deliveryAreaCheckActive,
    this.discount,
    this.minimumDelivery,
    this.studentDiscount,
    this.enableAllItemComment,
    this.deliveryTimeSaturday,
    this.discountOnAmountTuesday,
    this.closingHour,
    this.estimatedTakeoutDuration,
    this.businessName,
    this.businessStatus,
    this.pickupTimeFriday,
    this.vat,
    this.showAdvertise,
    this.metaKeywordsContactpage,
    this.metaDescription,
    this.vat_5,
    this.pickupTimeThursday,
    this.businessEmail,
    this.pickupTime,
    this.openingTimeMondayToThursday,
    this.closingTimeMonday,
    this.orderAdvance,
    this.metaKeywordsHomepage,
    this.tableBooking,
    this.metaKeywords,
    this.openingTimeFridayToSaturday,
    this.metaTitleOfferpage,
    this.closingTimeSaturday,
    this.smsService,
    this.closingTimeSunday,
    this.closingTimeWednesday,
    this.discountOnAmountFriday,
    this.minAmountForDiscount,
    this.pickupTimeTuesday,
    this.fbPageId,
    this.metaTitleGallerypage,
    this.orderAdvanceTime,
    this.discountOnAmountWednesday,
    this.metaDescriptionMenupage,
    this.pickupTimeSaturday,
    this.metaDescriptionContactpage,
    this.closingTimeThursday,
    this.newProperty,
    this.deliveryOutsideAreaMinAmount,
    this.deliveryTimeTuesday,
    this.openingTimeWednesday,
    this.deliveryTimeMonday,
    this.pickupTimeSunday,
    this.openingTimeSunday,
    this.openingTimeTuesday,
    this.businessAddress,
    this.deliveryTimeFriday,
    this.metaTitleMenupage,
    this.openingHour,
    this.deliveryTime,
    this.maximumDeliveryArea,
    this.discountOption,
    this.openingTime_Sunday,
    this.metaKeywordsOfferpage,
    this.openingTimeMonday,
    this.openingTimeTuesdayToThursday,
    this.freeDeliveryOnAmountActive,
    this.discountOnAmountMonday,
    this.deliveryTimeWednesday,
    this.dynamicDeliveryCharge,
    this.businessFbLink,
    this.metaTitle,
    this.discountOnAmountThursday,
    this.deliveryTimeThursday,
    this.deliveryTimeSunday,
    this.openingTimeSaturday,
    this.openingTimeFriday,
    this.deliveryCharge,
    this.orderPurgingDuraton,
    this.metaTitleContactpage,
    this.closingTimeFriday,
    this.openingTimeThursday,
    this.businessStartDate,
    this.newCustomerDiscount,
    this.discountOnAmountSunday,
    this.nhsstaffDiscount,
    this.pickupTimeMonday,
    this.businessPostcode,
    this.extraCategoryPrice,
    this.businessPhone,
    this.fbChatOption,
    this.metaDescriptionGallerypage,
    this.estimatedDeliveryDuration,
    this.metaDescriptionOfferpage,
    this.plasticBag,
    this.discountOnAllOrder,
    this.discountOnAmount,
    this.tuesdayDiscount,
    this.freeDeliveryOnAmount,
    this.metaKeywordsGallerypage,
    this.metaDescriptionHomepage,
    this.pickupTimeWednesday,
    this.serviceCharge,
    this.metaKeywordsMenupage,
    this.closingTimeTuesday,
    this.offday,
    this.businessOffDate,
    this.discountOnAmountSaturday,
    this.deliveryOption,
    this.zuhriDiscount,
  });
  GlobalParameter.fromJson(Map<String, dynamic> json) {
    metaTitleHomepage = json['meta_title_homepage']?.toString();
    deliveryAreaCheckActive = json['delivery_area_check_active']?.toString();
    discount = json['discount']?.toString();
    minimumDelivery = json['minimum_delivery']?.toString();
    studentDiscount = json['student_discount']?.toString();
    enableAllItemComment = json['enable_all_item_comment']?.toString();
    deliveryTimeSaturday = json['delivery_time_saturday']?.toString();
    discountOnAmountTuesday = json['discount_on_amount_tuesday']?.toString();
    closingHour = json['closing_hour']?.toString();
    estimatedTakeoutDuration = json['estimated_takeout_duration']?.toString();
    businessName = json['business_name']?.toString();
    businessStatus = json['business_status']?.toString();
    pickupTimeFriday = json['pickup_time_friday']?.toString();
    vat = json['vat']?.toString();
    showAdvertise = json['show_advertise']?.toString();
    metaKeywordsContactpage = json['meta_keywords_contactpage']?.toString();
    metaDescription = json['meta_description']?.toString();
    vat_5 = json['vat_5']?.toString();
    pickupTimeThursday = json['pickup_time_thursday']?.toString();
    businessEmail = json['business_email']?.toString();
    pickupTime = json['pickup_time']?.toString();
    openingTimeMondayToThursday = json['openingTime_monday_to_thursday']?.toString();
    closingTimeMonday = json['closing_time_monday']?.toString();
    orderAdvance = json['order_advance']?.toString();
    metaKeywordsHomepage = json['meta_keywords_homepage']?.toString();
    tableBooking = json['table_booking']?.toString();
    metaKeywords = json['meta_keywords']?.toString();
    openingTimeFridayToSaturday = json['openingTime_friday_to_saturday']?.toString();
    metaTitleOfferpage = json['meta_title_offerpage']?.toString();
    closingTimeSaturday = json['closing_time_saturday']?.toString();
    smsService = json['sms_service']?.toString();
    closingTimeSunday = json['closing_time_sunday']?.toString();
    closingTimeWednesday = json['closing_time_wednesday']?.toString();
    discountOnAmountFriday = json['discount_on_amount_friday']?.toString();
    minAmountForDiscount = json['min_amount_for_discount']?.toString();
    pickupTimeTuesday = json['pickup_time_tuesday']?.toString();
    fbPageId = json['fb_page_id']?.toString();
    metaTitleGallerypage = json['meta_title_gallerypage']?.toString();
    orderAdvanceTime = json['order_advance_time']?.toString();
    discountOnAmountWednesday = json['discount_on_amount_wednesday']?.toString();
    metaDescriptionMenupage = json['meta_description_menupage']?.toString();
    pickupTimeSaturday = json['pickup_time_saturday']?.toString();
    metaDescriptionContactpage = json['meta_description_contactpage']?.toString();
    closingTimeThursday = json['closing_time_thursday']?.toString();
    newProperty = json['new_property']?.toString();
    deliveryOutsideAreaMinAmount = json['delivery_outside_area_min_amount']?.toString();
    deliveryTimeTuesday = json['delivery_time_tuesday']?.toString();
    openingTimeWednesday = json['opening_time_wednesday']?.toString();
    deliveryTimeMonday = json['delivery_time_monday']?.toString();
    pickupTimeSunday = json['pickup_time_sunday']?.toString();
    openingTimeSunday = json['opening_time_sunday']?.toString();
    openingTimeTuesday = json['opening_time_tuesday']?.toString();
    businessAddress = json['business_address']?.toString();
    deliveryTimeFriday = json['delivery_time_friday']?.toString();
    metaTitleMenupage = json['meta_title_menupage']?.toString();
    openingHour = json['opening_hour']?.toString();
    deliveryTime = json['delivery_time']?.toString();
    maximumDeliveryArea = json['maximum_delivery_area']?.toString();
    discountOption = json['discount_option']?.toString();
    openingTime_Sunday = json['openingTime_sunday']?.toString();
    metaKeywordsOfferpage = json['meta_keywords_offerpage']?.toString();
    openingTimeMonday = json['opening_time_monday']?.toString();
    openingTimeTuesdayToThursday = json['openingTime_tuesday_to_thursday']?.toString();
    freeDeliveryOnAmountActive = json['free_delivery_on_amount_active']?.toString();
    discountOnAmountMonday = json['discount_on_amount_monday']?.toString();
    deliveryTimeWednesday = json['delivery_time_wednesday']?.toString();
    dynamicDeliveryCharge = json['dynamic_delivery_charge']?.toString();
    businessFbLink = json['business_fb_link']?.toString();
    metaTitle = json['meta_title']?.toString();
    discountOnAmountThursday = json['discount_on_amount_thursday']?.toString();
    deliveryTimeThursday = json['delivery_time_thursday']?.toString();
    deliveryTimeSunday = json['delivery_time_sunday']?.toString();
    openingTimeSaturday = json['opening_time_saturday']?.toString();
    openingTimeFriday = json['opening_time_friday']?.toString();
    deliveryCharge = json['delivery_charge']?.toString();
    orderPurgingDuraton = json['order_purging_duraton']?.toString();
    metaTitleContactpage = json['meta_title_contactpage']?.toString();
    closingTimeFriday = json['closing_time_friday']?.toString();
    openingTimeThursday = json['opening_time_thursday']?.toString();
    businessStartDate = json['business_start_date']?.toString();
    newCustomerDiscount = json['new_customer_discount']?.toString();
    discountOnAmountSunday = json['discount_on_amount_sunday']?.toString();
    nhsstaffDiscount = json['nhsstaff_discount']?.toString();
    pickupTimeMonday = json['pickup_time_monday']?.toString();
    businessPostcode = json['business_postcode']?.toString();
    extraCategoryPrice = json['extra_category_price']?.toString();
    businessPhone = json['business_phone']?.toString();
    fbChatOption = json['fb_chat_option']?.toString();
    metaDescriptionGallerypage = json['meta_description_gallerypage']?.toString();
    estimatedDeliveryDuration = json['estimated_delivery_duration']?.toString();
    metaDescriptionOfferpage = json['meta_description_offerpage']?.toString();
    plasticBag = json['plastic_bag']?.toString();
    discountOnAllOrder = json['discount_on_all_order']?.toString();
    discountOnAmount = json['discount_on_amount']?.toString();
    tuesdayDiscount = json['tuesday_discount']?.toString();
    freeDeliveryOnAmount = json['free_delivery_on_amount']?.toString();
    metaKeywordsGallerypage = json['meta_keywords_gallerypage']?.toString();
    metaDescriptionHomepage = json['meta_description_homepage']?.toString();
    pickupTimeWednesday = json['pickup_time_wednesday']?.toString();
    serviceCharge = json['service_charge']?.toString();
    metaKeywordsMenupage = json['meta_keywords_menupage']?.toString();
    closingTimeTuesday = json['closing_time_tuesday']?.toString();
    offday = json['offday']?.toString();
    businessOffDate =( json['business_off_date'] != null)? json['business_off_date']?.toString():"";
    discountOnAmountSaturday = json['discount_on_amount_saturday']?.toString();
    deliveryOption = json['delivery_option']?.toString();
    zuhriDiscount = json['zuhri_discount']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['meta_title_homepage'] = metaTitleHomepage;
    data['delivery_area_check_active'] = deliveryAreaCheckActive;
    data['discount'] = discount;
    data['minimum_delivery'] = minimumDelivery;
    data['student_discount'] = studentDiscount;
    data['enable_all_item_comment'] = enableAllItemComment;
    data['delivery_time_saturday'] = deliveryTimeSaturday;
    data['discount_on_amount_tuesday'] = discountOnAmountTuesday;
    data['closing_hour'] = closingHour;
    data['estimated_takeout_duration'] = estimatedTakeoutDuration;
    data['business_name'] = businessName;
    data['business_status'] = businessStatus;
    data['pickup_time_friday'] = pickupTimeFriday;
    data['vat'] = vat;
    data['show_advertise'] = showAdvertise;
    data['meta_keywords_contactpage'] = metaKeywordsContactpage;
    data['meta_description'] = metaDescription;
    data['vat_5'] = vat_5;
    data['pickup_time_thursday'] = pickupTimeThursday;
    data['business_email'] = businessEmail;
    data['pickup_time'] = pickupTime;
    data['openingTime_monday_to_thursday'] = openingTimeMondayToThursday;
    data['closing_time_monday'] = closingTimeMonday;
    data['order_advance'] = orderAdvance;
    data['meta_keywords_homepage'] = metaKeywordsHomepage;
    data['table_booking'] = tableBooking;
    data['meta_keywords'] = metaKeywords;
    data['openingTime_friday_to_saturday'] = openingTimeFridayToSaturday;
    data['meta_title_offerpage'] = metaTitleOfferpage;
    data['closing_time_saturday'] = closingTimeSaturday;
    data['sms_service'] = smsService;
    data['closing_time_sunday'] = closingTimeSunday;
    data['closing_time_wednesday'] = closingTimeWednesday;
    data['discount_on_amount_friday'] = discountOnAmountFriday;
    data['min_amount_for_discount'] = minAmountForDiscount;
    data['pickup_time_tuesday'] = pickupTimeTuesday;
    data['fb_page_id'] = fbPageId;
    data['meta_title_gallerypage'] = metaTitleGallerypage;
    data['order_advance_time'] = orderAdvanceTime;
    data['discount_on_amount_wednesday'] = discountOnAmountWednesday;
    data['meta_description_menupage'] = metaDescriptionMenupage;
    data['pickup_time_saturday'] = pickupTimeSaturday;
    data['meta_description_contactpage'] = metaDescriptionContactpage;
    data['closing_time_thursday'] = closingTimeThursday;
    data['new_property'] = newProperty;
    data['delivery_outside_area_min_amount'] = deliveryOutsideAreaMinAmount;
    data['delivery_time_tuesday'] = deliveryTimeTuesday;
    data['opening_time_wednesday'] = openingTimeWednesday;
    data['delivery_time_monday'] = deliveryTimeMonday;
    data['pickup_time_sunday'] = pickupTimeSunday;
    data['opening_time_sunday'] = openingTimeSunday;
    data['opening_time_tuesday'] = openingTimeTuesday;
    data['business_address'] = businessAddress;
    data['delivery_time_friday'] = deliveryTimeFriday;
    data['meta_title_menupage'] = metaTitleMenupage;
    data['opening_hour'] = openingHour;
    data['delivery_time'] = deliveryTime;
    data['maximum_delivery_area'] = maximumDeliveryArea;
    data['discount_option'] = discountOption;
    data['openingTime_sunday'] = openingTime_Sunday;
    data['meta_keywords_offerpage'] = metaKeywordsOfferpage;
    data['opening_time_monday'] = openingTimeMonday;
    data['openingTime_tuesday_to_thursday'] = openingTimeTuesdayToThursday;
    data['free_delivery_on_amount_active'] = freeDeliveryOnAmountActive;
    data['discount_on_amount_monday'] = discountOnAmountMonday;
    data['delivery_time_wednesday'] = deliveryTimeWednesday;
    data['dynamic_delivery_charge'] = dynamicDeliveryCharge;
    data['business_fb_link'] = businessFbLink;
    data['meta_title'] = metaTitle;
    data['discount_on_amount_thursday'] = discountOnAmountThursday;
    data['delivery_time_thursday'] = deliveryTimeThursday;
    data['delivery_time_sunday'] = deliveryTimeSunday;
    data['opening_time_saturday'] = openingTimeSaturday;
    data['opening_time_friday'] = openingTimeFriday;
    data['delivery_charge'] = deliveryCharge;
    data['order_purging_duraton'] = orderPurgingDuraton;
    data['meta_title_contactpage'] = metaTitleContactpage;
    data['closing_time_friday'] = closingTimeFriday;
    data['opening_time_thursday'] = openingTimeThursday;
    data['business_start_date'] = businessStartDate;
    data['new_customer_discount'] = newCustomerDiscount;
    data['discount_on_amount_sunday'] = discountOnAmountSunday;
    data['nhsstaff_discount'] = nhsstaffDiscount;
    data['pickup_time_monday'] = pickupTimeMonday;
    data['business_postcode'] = businessPostcode;
    data['extra_category_price'] = extraCategoryPrice;
    data['business_phone'] = businessPhone;
    data['fb_chat_option'] = fbChatOption;
    data['meta_description_gallerypage'] = metaDescriptionGallerypage;
    data['estimated_delivery_duration'] = estimatedDeliveryDuration;
    data['meta_description_offerpage'] = metaDescriptionOfferpage;
    data['plastic_bag'] = plasticBag;
    data['discount_on_all_order'] = discountOnAllOrder;
    data['discount_on_amount'] = discountOnAmount;
    data['tuesday_discount'] = tuesdayDiscount;
    data['free_delivery_on_amount'] = freeDeliveryOnAmount;
    data['meta_keywords_gallerypage'] = metaKeywordsGallerypage;
    data['meta_description_homepage'] = metaDescriptionHomepage;
    data['pickup_time_wednesday'] = pickupTimeWednesday;
    data['service_charge'] = serviceCharge;
    data['meta_keywords_menupage'] = metaKeywordsMenupage;
    data['closing_time_tuesday'] = closingTimeTuesday;
    data['offday'] = offday;
    data['business_off_date'] = businessOffDate;
    data['discount_on_amount_saturday'] = discountOnAmountSaturday;
    data['delivery_option'] = deliveryOption;
    data['zuhri_discount'] = zuhriDiscount;
    return data;
  }
}


class ErrorResponse {
  String? errorCode;
  String? errorDescription;

  ErrorResponse({
    this.errorCode,
    this.errorDescription,
  });
  ErrorResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['error_code']?.toString();
    errorDescription = json['error_description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error_code'] = errorCode;
    data['error_description'] = errorDescription;
    return data;
  }
}
