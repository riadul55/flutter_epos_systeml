import 'package:epos_system/app/data/remote_db/services/customer/dto/customer.response.dart';
import 'package:get/get.dart';

import '../../../../../config/config.dart';
import '../../../../modules/globals/log_info.dart';

class CustomerService {
  final GetConnect _connect;

  const CustomerService(GetConnect connect) : _connect = connect;

  String get prefix => "/delivery";

  Future<CustomerResponse> importCustomerInformation() async {
    final env = ConfigEnvironments.getEnvironments();
    //TODO. need to change url
    return await _connect.get(
      "https://cust.tech-novo.uk/api/consumer?business=${env.business}",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer 12|ptw64uDtUTyZ7gawKX6FTgVcYzLp6VPMsdJwNw6g"
      },
    ).then((response) {
      if (response.statusCode == 200) {
        logData("${response.statusCode}");
        return CustomerResponse.fromJson(response.body);
      } else {
        return CustomerResponse.withError(
            {"message": "Failed Import customer"});
      }
    }).onError((error, stackTrace) {
      print("onError==>$error");
      return CustomerResponse.withError({"message": error.toString()});
    }).catchError((error, stackTrace) {
      print("catchError==>$error");
      return CustomerResponse.withError({"message": error.toString()});
    }).timeout(_connect.timeout);
  }
}
