
class CustomerResponse {
  List<CustomerInformation>? customerInformation;
  ErrorsResponse? errorsResponse;

  CustomerResponse({this.customerInformation,this.errorsResponse});

  CustomerResponse.fromJson(List<dynamic> json)
      :customerInformation=(json).map((i) => CustomerInformation.fromJson(i)).toList(),
        errorsResponse=null;

  CustomerResponse.withError(Map<String, dynamic> json)
      :customerInformation=[],
        errorsResponse=ErrorsResponse.fromJson(json);

}


class CustomerAddresses {
/*
{
  "id": 165,
  "onetime_consumer_id": 171,
  "type": "PRIMARY",
  "street_name": "Argyll Avenue",
  "building": "100",
  "city": "LUTON",
  "state": "",
  "zip": "LU3 1EH"
}
*/

  int? id;
  int? onetimeConsumerId;
  String? type;
  String? streetName;
  String? building;
  String? city;
  String? state;
  String? zip;

  CustomerAddresses({
    this.id,
    this.onetimeConsumerId,
    this.type,
    this.streetName,
    this.building,
    this.city,
    this.state,
    this.zip,
  });
  CustomerAddresses.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    onetimeConsumerId = json['onetime_consumer_id']?.toInt();
    type = json['type']?.toString();
    streetName = json['street_name']?.toString();
    building = json['building']?.toString();
    city = json['city']?.toString();
    state = json['state']?.toString();
    zip = json['zip']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['onetime_consumer_id'] = onetimeConsumerId;
    data['type'] = type;
    data['street_name'] = streetName;
    data['building'] = building;
    data['city'] = city;
    data['state'] = state;
    data['zip'] = zip;
    return data;
  }
}

class CustomerInformation {
/*
{
  "id": 171,
  "business": "LAB",
  "first_name": "Fosiul",
  "last_name": "Alam",
  "phone": "07785221203",
  "email": "",
  "address_id": 165,
  "created_at": "2021-10-17T10:54:25.000000Z",
  "updated_at": "2021-10-17T10:54:25.000000Z",
  "addresses": [
    {
      "id": 165,
      "onetime_consumer_id": 171,
      "type": "PRIMARY",
      "street_name": "Argyll Avenue",
      "building": "100",
      "city": "LUTON",
      "state": "",
      "zip": "LU3 1EH"
    }
  ]
}
*/

  int? id;
  String? business;
  String? firstName;
  String? lastName;
  String? phone;
  String? email;
  int? addressId;
  String? createdAt;
  String? updatedAt;
  List<CustomerAddresses?>? addresses;

  CustomerInformation({
    this.id,
    this.business,
    this.firstName,
    this.lastName,
    this.phone,
    this.email,
    this.addressId,
    this.createdAt,
    this.updatedAt,
    this.addresses,
  });
  CustomerInformation.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    business = json['business']?.toString();
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    phone = json['phone']?.toString();
    email = json['email']?.toString();
    addressId = json['address_id']?.toInt();
    createdAt = json['created_at']?.toString();
    updatedAt = json['updated_at']?.toString();
    if (json['addresses'] != null) {
      final v = json['addresses'];
      final arr0 = <CustomerAddresses>[];
      v.forEach((v) {
        arr0.add(CustomerAddresses.fromJson(v));
      });
      addresses = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['business'] = business;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['phone'] = phone;
    data['email'] = email;
    data['address_id'] = addressId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (addresses != null) {
      final v = addresses;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['addresses'] = arr0;
    }
    return data;
  }
}

class ErrorsResponse {
  String? message;
  ErrorsResponse({
    this.message,
  });
  ErrorsResponse.fromJson(Map<String, dynamic> json) {
    message = json['message']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }

}