
class ProductResponse {
  List<Product>? product;
  ErrorResponse? errorResponse;

  ProductResponse({this.product, this.errorResponse});

  ProductResponse.fromJson(List<dynamic> json)
      : product =(json).map((i) => Product.fromJson(i)).toList(),
        errorResponse = null;

  ProductResponse.withError(Map<String, dynamic> json)
      : product = [],
        errorResponse =ErrorResponse.fromJson(json);
}
//
// class ProductComponentsProperties {
// /*
// {
//   "offer": "0",
//   "available_on": "both",
//   "categories_epos": "",
//   "item_name": "",
//   "print_order": "1",
//   "print_item_component_order": "0",
//   "sort_order": "2",
//   "offer_price": "0"
// }
// */
//
//   String? offer;
//   String? availableOn;
//   String? categoriesEpos;
//   String? itemName;
//   String? printOrder;
//   String? printItemComponentOrder;
//   String? sortOrder;
//   String? offerPrice;
//
//   ProductComponentsProperties({
//     this.offer,
//     this.availableOn,
//     this.categoriesEpos,
//     this.itemName,
//     this.printOrder,
//     this.printItemComponentOrder,
//     this.sortOrder,
//     this.offerPrice,
//   });
//   ProductComponentsProperties.fromJson(Map<String, dynamic> json) {
//     offer = json['offer']?.toString();
//     availableOn = json['available_on']?.toString();
//     categoriesEpos = json['categories_epos']?.toString();
//     itemName = json['item_name']?.toString();
//     printOrder = json['print_order']?.toString();
//     printItemComponentOrder = json['print_item_component_order']?.toString();
//     sortOrder = json['sort_order']?.toString();
//     offerPrice = json['offer_price']?.toString();
//   }
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['offer'] = offer;
//     data['available_on'] = availableOn;
//     data['categories_epos'] = categoriesEpos;
//     data['item_name'] = itemName;
//     data['print_order'] = printOrder;
//     data['print_item_component_order'] = printItemComponentOrder;
//     data['sort_order'] = sortOrder;
//     data['offer_price'] = offerPrice;
//     return data;
//   }
// }
//
// class ProductComponent {
// /*
// {
//   "product_type": "COMPONENT",
//   "product_uuid": "70145fef-9067-478e-94db-4daa143975f4",
//   "short_name": "Lamb",
//   "description": "",
//   "status": "APPROVED",
//   "price": {
//     "price_uuid": "a4698998-7475-433a-8810-13feaa992e8a",
//     "extra_price": 3,
//     "currency": "GBP",
//     "start_date": "2021-09-04"
//   },
//   "average_rate": -1,
//   "creator_provider_uuid": "a7033019-3dee-457b-a9da-50df238b3c9p",
//   "language": "en",
//   "discountable": false,
//   "visible": true,
//   "creation_date": "2021-05-24 07:07:01",
//   "last_update": "2022-04-03 04:10:35",
//   "properties": {
//     "offer": "0",
//     "available_on": "both",
//     "categories_epos": "",
//     "item_name": "",
//     "print_order": "1",
//     "print_item_component_order": "0",
//     "sort_order": "2",
//     "offer_price": "0"
//   },
//   "min_units": 0,
//   "max_units": 1,
//   "default_units": 0,
//   "skip_children_price": false,
//   "relation_type": "ONE_OF",
//   "relation_group": "Protain",
//   "components": [
//     {
//       "product_type": "COMPONENT",
//       "product_uuid": "bottled-cobra-cobra",
//       "short_name": "Bottled Cobra",
//       "description": "660ml",
//       "status": "APPROVED",
//       "price": {
//         "price_uuid": "f1a0d6b0-0480-4a2b-83f2-36804d059728",
//         "extra_price": 3.5,
//         "currency": "GBP",
//         "start_date": "2021-09-04"
//       },
//       "average_rate": -1,
//       "creator_provider_uuid": "a7033019-3dee-457b-a9da-50df238b3c9p",
//       "language": "en",
//       "discountable": false,
//       "visible": true,
//       "creation_date": "2021-08-18 09:57:32",
//       "last_update": "2022-04-03 04:10:35",
//       "properties": {
//         "offer": "0",
//         "available_on": "both",
//         "categories_epos": "",
//         "item_name": "",
//         "print_order": "1",
//         "print_item_component_order": "0",
//         "sort_order": "3",
//         "offer_price": "0"
//       },
//       "min_units": 0,
//       "max_units": 1,
//       "default_units": 0,
//       "skip_children_price": false,
//       "relation_type": "ONE_OF",
//       "relation_group": "Options"
//     }
//   ]
// }
// */
//
//   String? productType;
//   String? productUuid;
//   String? shortName;
//   String? description;
//   String? status;
//   ProductPrice? price;
//   int? averageRate;
//   String? creatorProviderUuid;
//   String? language;
//   bool? discountable;
//   bool? visible;
//   String? creationDate;
//   String? lastUpdate;
//   ProductComponentsProperties? properties;
//   int? minUnits;
//   int? maxUnits;
//   int? defaultUnits;
//   bool? skipChildrenPrice;
//   String? relationType;
//   String? relationGroup;
//   List<ProductComponent>? components;
//
//   ProductComponent({
//     this.productType,
//     this.productUuid,
//     this.shortName,
//     this.description,
//     this.status,
//     this.price,
//     this.averageRate,
//     this.creatorProviderUuid,
//     this.language,
//     this.discountable,
//     this.visible,
//     this.creationDate,
//     this.lastUpdate,
//     this.properties,
//     this.minUnits,
//     this.maxUnits,
//     this.defaultUnits,
//     this.skipChildrenPrice,
//     this.relationType,
//     this.relationGroup,
//     this.components,
//   });
//   ProductComponent.fromJson(Map<String, dynamic> json) {
//     productType = json['product_type']?.toString();
//     productUuid = json['product_uuid']?.toString();
//     shortName = json['short_name']?.toString();
//     description = json['description']?.toString();
//     status = json['status']?.toString();
//     price = (json['price'] != null) ? ProductPrice.fromJson(json['price']) : null;
//     averageRate = json['average_rate']?.toInt();
//     creatorProviderUuid = json['creator_provider_uuid']?.toString();
//     language = json['language']?.toString();
//     discountable = json['discountable'];
//     visible = json['visible'];
//     creationDate = json['creation_date']?.toString();
//     lastUpdate = json['last_update']?.toString();
//     properties = (json['properties'] != null) ? ProductComponentsProperties.fromJson(json['properties']) : null;
//     minUnits = json['min_units']?.toInt();
//     maxUnits = json['max_units']?.toInt();
//     defaultUnits = json['default_units']?.toInt();
//     skipChildrenPrice = json['skip_children_price'];
//     relationType = json['relation_type']?.toString();
//     relationGroup = json['relation_group']?.toString();
//     if (json['components'] != null) {
//       final v = json['components'];
//       final arr0 = <ProductComponent>[];
//       v.forEach((v) {
//         arr0.add(ProductComponent.fromJson(v));
//       });
//       components = arr0;
//     }else{
//       components=[];
//     }
//   }
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['product_type'] = productType;
//     data['product_uuid'] = productUuid;
//     data['short_name'] = shortName;
//     data['description'] = description;
//     data['status'] = status;
//     if (price != null) {
//       data['price'] = price!.toJson();
//     }
//     data['average_rate'] = averageRate;
//     data['creator_provider_uuid'] = creatorProviderUuid;
//     data['language'] = language;
//     data['discountable'] = discountable;
//     data['visible'] = visible;
//     data['creation_date'] = creationDate;
//     data['last_update'] = lastUpdate;
//     if (properties != null) {
//       data['properties'] = properties!.toJson();
//     }
//     data['min_units'] = minUnits;
//     data['max_units'] = maxUnits;
//     data['default_units'] = defaultUnits;
//     data['skip_children_price'] = skipChildrenPrice;
//     data['relation_type'] = relationType;
//     data['relation_group'] = relationGroup;
//     if (components != null) {
//       final v = components;
//       final arr0 = [];
//       v!.forEach((v) {
//         arr0.add(v!.toJson());
//       });
//       data['components'] = arr0;
//     }
//     return data;
//   }
// }

class ProductPrice {
/*
{
  "price_uuid": "4eff000d-bc64-4341-bcd4-953a13de3940",
  "price": 6.5,
  "currency": "GBP",
  "start_date": "2021-05-24"
}
*/

  String? priceUuid;
  double? price;
  String? currency;
  double? vat;
  String? startDate;

  ProductPrice({
    this.priceUuid,
    this.price,
    this.vat,
    this.currency,
    this.startDate,
  });
  ProductPrice.fromJson(Map<String, dynamic> json) {
    priceUuid = json['price_uuid']?.toString();
    if (json['price'] != null) {
      price = json['price']?.toDouble();
    } else if (json['extra_price'] != null) {
      price = json['extra_price']?.toDouble();
    } else {
      price = 0.0;
    }
    vat = json['vat']?.toDouble();
    currency = json['currency']?.toString();
    startDate = json['start_date']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['price_uuid'] = priceUuid;
    data['price'] = price;
    data['vat'] = vat;
    data['currency'] = currency;
    data['start_date'] = startDate;
    return data;
  }
}

class Product{
/*
{
  "product_uuid": "garlic",
  "product_type": "ITEM",
  "short_name": "Garlic",
  "description": "Medium, garnished in extra garlic rich sauce",
  "status": "APPROVED",
  "price": {
    "price_uuid": "4eff000d-bc64-4341-bcd4-953a13de3940",
    "price": 6.5,
    "currency": "GBP",
    "start_date": "2021-05-24"
  },
  "average_rate": -1,
  "creator_provider_uuid": "a7033019-3dee-457b-a9da-50df238b3c9p",
  "language": "en",
  "discountable": true,
  "visible": true,
  "creation_date": "2021-05-23 08:59:58",
  "last_update": "2022-04-03 04:10:35",
  "properties": {
    "bundle_group": "Main Dish",
    "available_on": "both",
    "D": "false",
    "sell_on_its_own": "1",
    "G": "false",
    "available": "1",
    "print_order": "3",
    "print_item_component_order": "1",
    "N": "false",
    "spiced": "false",
    "offer_price": "0",
    "VE": "false",
    "offer": "1",
    "categories_epos": "classicdishes",
    "V": "false",
    "categories_key": "",
    "review_enabled": "0",
    "categories": "classicdishes",
    "sort_order": "33"
  },
  "min_units": 0,
  "max_units": 0,
  "default_units": 0,
  "skip_children_price": false,
  "components": [
    {
      "product_type": "COMPONENT",
      "product_uuid": "70145fef-9067-478e-94db-4daa143975f4",
      "short_name": "Lamb",
      "description": "",
      "status": "APPROVED",
      "price": {
        "price_uuid": "a4698998-7475-433a-8810-13feaa992e8a",
        "extra_price": 3,
        "currency": "GBP",
        "start_date": "2021-09-04"
      },
      "average_rate": -1,
      "creator_provider_uuid": "a7033019-3dee-457b-a9da-50df238b3c9p",
      "language": "en",
      "discountable": false,
      "visible": true,
      "creation_date": "2021-05-24 07:07:01",
      "last_update": "2022-04-03 04:10:35",
      "properties": {
        "offer": "0",
        "available_on": "both",
        "categories_epos": "",
        "item_name": "",
        "print_order": "1",
        "print_item_component_order": "0",
        "sort_order": "2",
        "offer_price": "0"
      },
      "min_units": 0,
      "max_units": 1,
      "default_units": 0,
      "skip_children_price": false,
      "relation_type": "ONE_OF",
      "relation_group": "Protain",
      "components": [
        {
          "product_type": "COMPONENT",
          "product_uuid": "bottled-cobra-cobra",
          "short_name": "Bottled Cobra",
          "description": "660ml",
          "status": "APPROVED",
          "price": {
            "price_uuid": "f1a0d6b0-0480-4a2b-83f2-36804d059728",
            "extra_price": 3.5,
            "currency": "GBP",
            "start_date": "2021-09-04"
          },
          "average_rate": -1,
          "creator_provider_uuid": "a7033019-3dee-457b-a9da-50df238b3c9p",
          "language": "en",
          "discountable": false,
          "visible": true,
          "creation_date": "2021-08-18 09:57:32",
          "last_update": "2022-04-03 04:10:35",
          "properties": {
            "offer": "0",
            "available_on": "both",
            "categories_epos": "",
            "item_name": "",
            "print_order": "1",
            "print_item_component_order": "0",
            "sort_order": "3",
            "offer_price": "0"
          },
          "min_units": 0,
          "max_units": 1,
          "default_units": 0,
          "skip_children_price": false,
          "relation_type": "ONE_OF",
          "relation_group": "Options"
        }
      ]
    }
  ]
}
*/

  String? productUuid;
  String? productType;
  String? shortName;
  String? description;
  String? status;
  ProductPrice? price;
  int? averageRate;
  String? creatorProviderUuid;
  String? language;
  bool? discountable;
  bool? visible;
  String? creationDate;
  String? lastUpdate;
  Map<String, dynamic>? properties;
  int? minUnits;
  int? maxUnits;
  int? defaultUnits;
  bool? skipChildrenPrice;
  List<Product>? components;

  Product({
    this.productUuid,
    this.productType,
    this.shortName,
    this.description,
    this.status,
    this.price,
    this.averageRate,
    this.creatorProviderUuid,
    this.language,
    this.discountable,
    this.visible,
    this.creationDate,
    this.lastUpdate,
    this.properties,
    this.minUnits,
    this.maxUnits,
    this.defaultUnits,
    this.skipChildrenPrice,
    this.components,
  });
  Product.fromJson(Map<String, dynamic> json) {
    productUuid = json['product_uuid']?.toString();
    productType = json['product_type']?.toString();
    shortName = json['short_name']?.toString();
    description = json['description']?.toString();
    status = json['status']?.toString();
    price = (json['price'] != null) ? ProductPrice.fromJson(json['price']) : null;
    averageRate = json['average_rate']?.toInt();
    creatorProviderUuid = json['creator_provider_uuid']?.toString();
    language = json['language']?.toString();
    discountable = json['discountable'];
    visible = json['visible'];
    creationDate = json['creation_date']?.toString();
    lastUpdate = json['last_update']?.toString();
    properties = json['properties'];
    minUnits = json['min_units']?.toInt();
    maxUnits = json['max_units']?.toInt();
    defaultUnits = json['default_units']?.toInt();
    skipChildrenPrice = json['skip_children_price'];
    if (json['components'] != null) {
      final v = json['components'];
      final arr0 = <Product>[];
      v.forEach((v) {
        arr0.add(Product.fromJson(v));
      });
      components = arr0;
    }else{
      components=[];
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['product_uuid'] = productUuid;
    data['product_type'] = productType;
    data['short_name'] = shortName;
    data['description'] = description;
    data['status'] = status;
    if (price != null) {
      data['price'] = price!.toJson();
    }
    data['average_rate'] = averageRate;
    data['creator_provider_uuid'] = creatorProviderUuid;
    data['language'] = language;
    data['discountable'] = discountable;
    data['visible'] = visible;
    data['creation_date'] = creationDate;
    data['last_update'] = lastUpdate;
    if (properties != null) {
      data['properties'] = properties;
    }
    data['min_units'] = minUnits;
    data['max_units'] = maxUnits;
    data['default_units'] = defaultUnits;
    data['skip_children_price'] = skipChildrenPrice;
    if (components != null) {
      final v = components;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['components'] = arr0;
    }
    return data;
  }
}

class ErrorResponse {
  String? message;
  ErrorResponse({
    this.message,
  });
  ErrorResponse.fromJson(Map<String, dynamic> json) {
    message = json['message']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }
}