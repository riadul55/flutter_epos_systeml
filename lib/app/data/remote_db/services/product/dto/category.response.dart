
import 'package:epos_system/app/modules/globals/log_info.dart';

class CategoryResponse {
  List<Category>? categories;
  CategoryErrorsResponse? categoryErrorsResponse;

  CategoryResponse({this.categories,this.categoryErrorsResponse});

  CategoryResponse.fromJson(Map<String, dynamic> json) {
    List<Category> categoryList = [];
    for (MapEntry element in json.entries) {
      categoryList.add(Category.fromJson({
        "keyName": element.key,
        "items": element.value,
      }));
    }
    categories = categoryList;
    categoryErrorsResponse=null;
  }

  CategoryResponse.withError(Map<String, dynamic> json)
      :categories=[],
        categoryErrorsResponse=CategoryErrorsResponse.fromJson(json);

}

class Category {
  String? keyName;
  List<CategoryItem>? items;

  Category({this.keyName, this.items});

  Category.fromJson(Map<String, dynamic> json) {
    keyName = json['keyName'];
    List<CategoryItem> itemList = [];
    for (var item in json['items']) {
      itemList.add(CategoryItem.fromJson(item));
    }
    items = itemList;
  }
}


class CategoryItem {
/*
{
  "platform": "WEB",
  "key_name": "category",
  "value": "baltidishes",
  "display_name": "Balti dishes",
  "description": "Price includes Nan Bread. Balti cuisine uses its own distinctive range of spices with focus on fresh coriander & methi (fenugreek) in a rich aromatic sauce.",
  "content_type": "webp",
  "filename": "baltidishes_20210529091653_balti-dishes.webp",
  "creation_date": "2021-05-23 07:22:27",
  "last_update": "2021-05-23 07:22:27",
  "sort_order": 9
}
*/

  String? platform;
  String? keyName;
  String? value;
  String? displayName;
  String? description;
  String? contentType;
  String? filename;
  String? creationDate;
  String? lastUpdate;
  int? sortOrder;

  CategoryItem({
    this.platform,
    this.keyName,
    this.value,
    this.displayName,
    this.description,
    this.contentType,
    this.filename,
    this.creationDate,
    this.lastUpdate,
    this.sortOrder,
  });
  CategoryItem.fromJson(Map<String, dynamic> json) {
    logError(json);
    platform = json['platform']?.toString();
    keyName = json['key_name']?.toString();
    value = json['value']?.toString();
    displayName = json['display_name']?.toString();
    description = json['description']?.toString();
    contentType = json['content_type']?.toString();
    filename = json['filename']?.toString();
    creationDate = json['creation_date']?.toString();
    lastUpdate = json['last_update']?.toString();
    sortOrder = json['sort_order']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['platform'] = platform;
    data['key_name'] = keyName;
    data['value'] = value;
    data['display_name'] = displayName;
    data['description'] = description;
    data['content_type'] = contentType;
    data['filename'] = filename;
    data['creation_date'] = creationDate;
    data['last_update'] = lastUpdate;
    data['sort_order'] = sortOrder;
    return data;
  }
}


class CategoryErrorsResponse {
  String? message;
  CategoryErrorsResponse({
    this.message,
  });
  CategoryErrorsResponse.fromJson(Map<String, dynamic> json) {
    message = json['message']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }

}