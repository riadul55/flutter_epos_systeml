import 'package:epos_system/app/data/remote_db/services/product/dto/product.response.dart';
import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:get/get.dart';

import 'dto/category.response.dart';

class ProductService {
  final GetConnect _connect;

  const ProductService(GetConnect connect) : _connect = connect;

  String get prefix => "/delivery";

  Future<ProductResponse> getProducts() async {
    return await _connect.get(
      "$prefix/product",
      query: {
        "show_hierarchy": "true",
      },
    ).then((response) {
      if (response.statusCode == 200) {
        logData("product api response code==>${response.statusCode}");
        return ProductResponse.fromJson(response.body);
      } else {
        return ProductResponse.withError({"message": "error"});
      }
    }).onError((error, stackTrace) {
      print("onError==>$error");
      return ProductResponse.withError({"message": error.toString()});
    }).catchError((error, stackTrace) {
      print("catchError==>$error");
      return ProductResponse.withError({"message": error.toString()});
    }).timeout(Duration(minutes: 1));
  }

  Future<CategoryResponse> getCategories() async {
    return await _connect.get("$prefix/config/product/property", query: {
      "platform" : "EPOS",
    })
        .then((response) {
      if (response.statusCode == 200) {
        logData("category api response code==>${response.statusCode}");
        return CategoryResponse.fromJson(response.body);
      } else {
        return CategoryResponse.withError({"message": "error"});
      }
    }).onError((error, stackTrace) {
      print("onError==>$error");
      return CategoryResponse.withError({"message": error.toString()});
    }).catchError((error, stackTrace) {
      print("catchError==>$error");
      return CategoryResponse.withError({"message": error.toString()});
    }).timeout(_connect.timeout);
  }

// Future<dynamic> getProducts() async {
//   return await _connect.get("$prefix/end_points",
//     headers: {},
//     contentType: "",
//     query: {},
//   );
// }
}
