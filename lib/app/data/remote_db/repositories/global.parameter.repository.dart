import 'package:epos_system/app/core/utilities/storage_key.dart';
import 'package:epos_system/app/data/remote_db/services/global_parameter/dto/global.parameter.response.dart';
import 'package:epos_system/app/data/remote_db/services/product/product.service.dart';
import 'package:epos_system/app/modules/globals/log_info.dart';
import 'package:get/get.dart';

import '../../../core/utilities/my_storage.dart';
import '../services/global_parameter/global.parameter.service.dart';

class GlobalParameterRepository {
  final GlobalParameterService _globalParameterService;

  GlobalParameterRepository({required GlobalParameterService service}) : _globalParameterService = service;

  final storage = Get.find<MyStorage>();
  // return response with view model
  Future<void> getGlobalParameter() async {
    try {
      final response = await _globalParameterService.getGlobalParameterData();
      if(response.globalParameter != null){
        logSuccess("Global Parameter API Success");
        // storage.globalParameter.val=<String, String>{
        //         StorageKey.BUSINESS_NAME : response.globalParameter!.businessName!,
        //         StorageKey.BUSINESS_PHONE : response.globalParameter!.businessPhone!,
        //         StorageKey.BUSINESS_EMAIL: response.globalParameter!.businessEmail!,
        //         StorageKey.BUSINESS_STATUS : response.globalParameter!.businessStatus!,
        //         StorageKey.BUSINESS_ADDRESS : response.globalParameter!.businessAddress!,
        //         StorageKey.BUSINESS_POSTCODE : response.globalParameter!.businessPostcode!,
        //         StorageKey.BUSINESS_VAT : response.globalParameter!.vat!,
        // };
        storage.businessName.val= response.globalParameter!.businessName!;
        storage.businessAddress.val= response.globalParameter!.businessAddress!;
        storage.businessPostCode.val= response.globalParameter!.businessPostcode!;
        storage.businessEmail.val= response.globalParameter!.businessEmail!;
        storage.businessPhone.val= response.globalParameter!.businessPhone!;
        storage.businessStatus.val= response.globalParameter!.businessStatus!;
        storage.businessVat.val= response.globalParameter!.vat!;
        storage.businessOpeningTime.val= response.globalParameter!.openingHour!;
      }
    } catch (e) {
      print(e);
    }
  }


  Future<void> updateGlobalParameter(Map requestBody) async {
    try {
      final response = await _globalParameterService.updateGlobalParameterData(requestBody);
      if(response.globalParameter != null){
        logSuccess("Global Parameter API Success");
      }
    } catch (e) {
      print(e);
    }
  }

  Future<GlobalParameterResponse> getGlobalBusinessStatus() async {
    var response=GlobalParameterResponse();
    try {
       response = await _globalParameterService.getGlobalParameterData();
      if(response.globalParameter != null){
        return response;
      }
    } catch (e) {
      print(e);
    }
    return response;
  }



}