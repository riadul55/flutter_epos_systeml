import 'package:epos_system/app/data/remote_db/repositories/models/category.model.dart';
import 'package:epos_system/app/data/remote_db/services/product/product.service.dart';
import '../../../data/local_db/data_modules/bindings/product.repository.binding.dart'
    as PRBO;
import 'models/product.model.dart';

class ProductRepository {
  final ProductService _productService;
  final PRBO.ProductRepositoryBinding _binding;

  ProductRepository({required ProductService service, required PRBO.ProductRepositoryBinding binding})
      : _productService = service,
        _binding = binding;

  // return response with view model
  Future<CategoryModel> getCategories() async {
    try {
      final response = await _productService.getCategories();
      if (response.categories != null && response.categories!.isNotEmpty) {
        var categoryModel = CategoryModel.fromData(response);
        await _binding.repository.deleteAllCategories();
        await categoryModel.save(_binding);
        return Future.value(categoryModel);
      } else {
        throw Exception(response.categoryErrorsResponse?.message ?? "");
      }
    } catch (e) {
      throw Exception(e);
    }
  }


  Future<ProductModel> getProducts() async {
    try {
      final response = await _productService.getProducts();
      if (response.product != null && response.product!.isNotEmpty) {
        var productModel = ProductModel.fromData(response);
        await _binding.repository.deleteAllProduct();
        await productModel.save(_binding);
        return Future.value(productModel);
      } else {
        throw Exception(response.errorResponse?.message ?? "");
      }
    } catch (e) {
      throw Exception(e);
    }
  }

}
