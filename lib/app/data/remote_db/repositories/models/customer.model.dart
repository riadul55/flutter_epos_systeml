import 'package:epos_system/app/data/local_db/app_db.dart';
import 'package:epos_system/app/data/remote_db/bindings/customer.repository.binding.dart';
import 'package:epos_system/app/data/remote_db/services/customer/dto/customer.response.dart';
import 'package:drift/drift.dart' as drift;
import 'package:uuid/uuid.dart';

import '../../../local_db/data_modules/bindings/local.customer.repository.binding.dart';

class CustomerModel {
  final CustomerResponse data;

  const CustomerModel({required this.data});

  factory CustomerModel.fromData(CustomerResponse response) {
    return CustomerModel(data: response);
  }

  Future<void> save(LocalCustomerRepositoryBinding binding) async {
    if (data != null && data.customerInformation != null && data.customerInformation!.isNotEmpty) {
      for (CustomerInformation element in data.customerInformation ?? []) {
        var uuid = Uuid();
        var table=TbCustomerCompanion(
          id:drift.Value(uuid.v1()),
          business: drift.Value("${element.business}"),
          email: drift.Value("${element.email}"),
          firstName: drift.Value("${element.firstName}" ),
          lastName: drift.Value("${element.lastName}"),
          phone: drift.Value("${element.phone}"),
          createdAt: drift.Value("${element.createdAt}"),
          updatedAt: drift.Value("${element.updatedAt}"),
        );
        if(element.addresses != null){
          for(int i=0; i<element.addresses!.length ; i++ ){
            var addressUuid = Uuid();
            var addressTable=TbCustomerAddressCompanion(
                id: drift.Value(addressUuid.v1()),
                onetimeConsumerId: drift.Value(uuid.v1()),
                type: drift.Value("${element.addresses![i]?.type}"),
                streetName: drift.Value("${element.addresses![i]?.streetName}"),
                building: drift.Value("${element.addresses![i]?.building}"),
                city: drift.Value("${element.addresses![i]?.city}"),
                state: drift.Value("${element.addresses![i]?.state}"),
                zip: drift.Value("${element.addresses![i]?.zip}"),
            );
            binding.repository.addCustomerAddress(addressTable);
          }
        }
        binding.repository.addCustomer(table);
      }
    }
  }
}
