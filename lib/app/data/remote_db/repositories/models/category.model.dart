import 'package:epos_system/app/data/remote_db/services/product/dto/category.response.dart';
import 'package:uuid/uuid.dart';
import '../../../local_db/app_db.dart';
import '../../../local_db/data_modules/bindings/product.repository.binding.dart';
import 'package:drift/drift.dart' as drift;

class CategoryModel {
  final CategoryResponse data;

  const CategoryModel({required this.data});

  factory CategoryModel.fromData(CategoryResponse response) {
    return CategoryModel(data: response);
  }

  Future<void> save(ProductRepositoryBinding binding) async {
    if (data != null && data.categories != null && data.categories!.isNotEmpty) {
      for (Category category in data.categories ?? []) {
        for (CategoryItem item in category.items ?? []) {
          var uuid = Uuid();
          var table = TbConfigProductPropertyCompanion(
            id: drift.Value(uuid.v1()),
            platform: drift.Value("${item.platform}"),
            keyName: drift.Value("${category.keyName}"),
            value: drift.Value("${item.value}"),
            sortOrder: drift.Value(item.sortOrder ?? 0),
            displayName: drift.Value("${item.displayName}"),
            description: drift.Value("${item.description}"),
            filename: drift.Value("${item.filename}"),
            contentType: drift.Value("${item.contentType}"),
            isSubmitted: drift.Value(true),
          );
          binding.repository.addCategory(table);
        }
      }
    }
  }
}