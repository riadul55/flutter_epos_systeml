import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:uuid/uuid.dart';

import '../../../../../config/config.dart';
import '../../../local_db/app_db.dart';
import '../../../local_db/data_modules/bindings/product.repository.binding.dart';
import '../../../local_db/models/product_entry.model.dart';
import '../../services/product/dto/product.response.dart' as response;

class ProductModel {
  final response.ProductResponse data;

  const ProductModel({required this.data});

  factory ProductModel.fromData(response.ProductResponse response) {
    return ProductModel(data: response);
  }

  Future<void> save(ProductRepositoryBinding binding) async {
    if (data != null && data.product != null && data.product!.isNotEmpty) {
      for (response.Product element in data.product ?? []) {
        ProductEntry productEntry = getProductEntry(element);
        binding.repository.addProduct(productEntry);

        for (response.Product component in element.components ?? []) {
          ProductEntry componentEntry = getProductEntry(component);
          binding.repository.addProduct(componentEntry);

          for (response.Product subComponent in component.components ?? []) {
            ProductEntry componentEntry = getProductEntry(subComponent);
            binding.repository.addProduct(componentEntry);
          }
        }
      }
    }
  }

  ProductEntry getProductEntry(response.Product element) {
    final config = ConfigEnvironments.getEnvironments();
    var productUuid = Uuid().v1();

    List<PropertyEntry> propertyList = [];
    for (MapEntry entry in element.properties!.entries) {
      propertyList.add(getPropertyEntry(productUuid, entry.key, entry.value));
    }

    var productPrice = PriceEntry(
      id: Uuid().v1(),
      productUuid: productUuid,
      price: "${element.price?.price}",
      extraPrice: "${element.price?.price}",
      vat: "${element.price?.vat ?? "0.0"}",
      currency: element.price?.currency ?? "GBP",
    );

    var productEntry = ProductEntry(
      id: productUuid,
      shortName: element.shortName ?? "",
      productType: element.productType ?? "",
      description: element.description ?? "",
      status: element.status ?? "",
      visible: element.visible ?? false,
      discountable: element.discountable ?? false,
      providerId: "${config.providerID}",
      language: "${config.language}",
      property: propertyList,
      price: productPrice,
      isSubmitted: true,
    );
    return productEntry;
  }

  getPropertyEntry(productId, key, value) => PropertyEntry(
    id: Uuid().v1(),
    productUuid: productId,
    keyName: key,
    keyValue: value,
  );
}