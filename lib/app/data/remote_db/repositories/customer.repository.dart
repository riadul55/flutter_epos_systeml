import 'package:epos_system/app/data/remote_db/repositories/models/customer.model.dart';
import 'package:epos_system/app/data/remote_db/services/customer/customer.service.dart';
import 'package:epos_system/app/modules/globals/log_info.dart';

import '../../local_db/data_modules/bindings/local.customer.repository.binding.dart';

class CustomerRepository {
  final CustomerService _customerService;
  final LocalCustomerRepositoryBinding _binding;
  CustomerRepository( {required CustomerService service, required LocalCustomerRepositoryBinding binding,})
      : _customerService = service,
        _binding=binding;

  // return response with view model
  Future<CustomerModel> getCustomerList() async {
    try {
      final response = await _customerService.importCustomerInformation();
      if(response.customerInformation != null){
        logSuccess("Import Customer API Success");
        logSuccess("Total customer import ==> ${response.customerInformation!.length}");
        var customerModel = CustomerModel.fromData(response);
        try{
          await _binding.repository.deleteCustomer();
          await _binding.repository.deleteCustomerAddress();
        }catch(e){
          print(e);
        }
        await customerModel.save(_binding);
        return Future.value(customerModel);
      }else{
        throw Exception(response.errorsResponse?.message ?? "");
      }
    } catch (e) {
      throw Exception(e);
    }
  }

}