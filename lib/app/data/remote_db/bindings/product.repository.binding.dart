import 'package:epos_system/app/data/remote_db/repositories/product.repository.dart';
import 'package:epos_system/app/data/remote_db/services/product/product.service.dart';
import '../../../data/local_db/data_modules/bindings/product.repository.binding.dart' as PRBO;
import 'package:get/get.dart';

import '../../local_db/app_db.dart';
import '../../local_db/db_injection.dart';

class ProductRepositoryBinding {
  late ProductRepository _productRepository;

  ProductRepository get repository => _productRepository;

  ProductRepositoryBinding() {
    final getConnect = Get.find<GetConnect>();
    final productService = ProductService(getConnect);
    final productRepositoryBinding = PRBO.ProductRepositoryBinding();
    _productRepository = ProductRepository(service: productService, binding: productRepositoryBinding);
  }
}
