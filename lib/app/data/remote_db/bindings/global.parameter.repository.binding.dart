import 'package:epos_system/app/data/remote_db/repositories/product.repository.dart';
import 'package:epos_system/app/data/remote_db/services/global_parameter/global.parameter.service.dart';
import 'package:epos_system/app/data/remote_db/services/product/product.service.dart';
import 'package:get/get.dart';

import '../repositories/global.parameter.repository.dart';

class GlobalParameterRepositoryBinding {
  late GlobalParameterRepository _globalParameterRepository;

  GlobalParameterRepository get repository => _globalParameterRepository;

  GlobalParameterRepositoryBinding() {
    final getConnect = Get.find<GetConnect>();
    final globalService = GlobalParameterService(getConnect);
    _globalParameterRepository = GlobalParameterRepository(service: globalService);
  }
}
