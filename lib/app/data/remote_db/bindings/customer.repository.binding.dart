import 'package:epos_system/app/data/local_db/data_modules/bindings/local.customer.repository.binding.dart';
import 'package:get/get.dart';

import '../repositories/customer.repository.dart';
import '../services/customer/customer.service.dart';

class CustomerRepositoryBinding {
  late CustomerRepository _customerRepository;

  CustomerRepository get repository => _customerRepository;

  CustomerRepositoryBinding() {
    final getConnect = GetConnect();
    final customerService = CustomerService(getConnect);
    final customerRepositoryBinding=LocalCustomerRepositoryBinding();
    _customerRepository = CustomerRepository(service: customerService,binding: customerRepositoryBinding);
  }
}
