import 'package:flutter/material.dart';

class ColorsLight {
  static var primary = Color.fromRGBO(255, 165, 0, 1.0);
  static var onPrimary = Colors.white;
  static var primaryVariant = Color.fromRGBO(212, 212, 212, 1.0);

  static var secondary = Colors.black;
  static var onSecondary = Colors.white;
  static var secondaryVariant = Color.fromRGBO(142, 139, 139, 1.0);

  static var background = Color(0xFFF8F8F8);
  static var onBackground = Color.fromRGBO(25, 29, 36, 1);

  static var error = Colors.red;
  static var onError = Colors.white;

  static var surface = Colors.white;
  static var onSurface = Color(0xFFE5E5E5);

  static var primaryText = Colors.black;
  static var secondaryText = Colors.grey;
}
