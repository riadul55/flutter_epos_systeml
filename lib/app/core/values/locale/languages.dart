import 'package:get/get.dart';

import 'string_en.dart';

class Languages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
    'en_US': STRING_EN,
    // 'bn_BD': STRING_BN,
  };
}