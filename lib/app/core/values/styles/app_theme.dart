import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../colors/colors_light.dart';

class AppThemes {
  static final Color _lightFocusColor = Colors.black.withOpacity(0.12);
  // static final Color _darkFocusColor = Colors.white.withOpacity(0.12);

  static ThemeData lightThemeData = themeData(lightColorScheme, _lightFocusColor);
  // static ThemeData darkThemeData = themeData(darkColorScheme, _darkFocusColor);

  static ThemeData themeData(ColorScheme colorScheme, Color focusColor) {
    return ThemeData(
      textTheme:GoogleFonts.robotoTextTheme(),
      colorScheme: colorScheme,
      appBarTheme: AppBarTheme(
        color: colorScheme.background,
        iconTheme: IconThemeData(color: colorScheme.primaryVariant),
        elevation: 0,
        brightness: colorScheme.brightness,
      ),
      iconTheme: IconThemeData(color: colorScheme.onPrimary),
      canvasColor: colorScheme.background,
      scaffoldBackgroundColor: colorScheme.background,
      highlightColor: Colors.transparent,
      focusColor: focusColor,
      accentColor: colorScheme.primary,
    );
  }

  static final ColorScheme lightColorScheme = ColorScheme(
    primary: ColorsLight.primary,
    primaryVariant: ColorsLight.primaryVariant,
    primaryContainer: ColorsLight.primaryVariant,
    secondary: ColorsLight.secondary,
    secondaryVariant: ColorsLight.secondaryVariant,
    secondaryContainer: ColorsLight.secondaryVariant,
    background: ColorsLight.background,
    surface: ColorsLight.surface,
    onBackground: ColorsLight.onBackground,
    error: ColorsLight.error,
    onError: ColorsLight.onError,
    onPrimary: ColorsLight.onPrimary,
    onSecondary: ColorsLight.onSecondary,
    onSurface: ColorsLight.onSurface,
    brightness: Brightness.light,
  );

  // static final ColorScheme darkColorScheme = ColorScheme(
  //   primary: ColorsDark.primary,
  //   primaryVariant: ColorsDark.primaryVariant,
  //   secondary: ColorsDark.secondary,
  //   secondaryVariant: ColorsDark.secondaryVariant,
  //   background: ColorsDark.background,
  //   surface: ColorsDark.surface,
  //   onBackground: ColorsDark.onBackground,
  //   error: ColorsDark.error,
  //   onError: ColorsDark.onError,
  //   onPrimary: ColorsDark.onPrimary,
  //   onSecondary: ColorsDark.onSecondary,
  //   onSurface: ColorsDark.onSurface,
  //   brightness: Brightness.dark,
  // );
}
