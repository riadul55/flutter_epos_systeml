
import 'package:get_storage/get_storage.dart';

import 'storage_key.dart';

class MyStorage {
  static final box = GetStorage();

  final appConfig = ReadWriteValue(StorageKey.APP_CONFIGS, {}, () => box);
  // final appModeConfig = ReadWriteValue(StorageKey.CONFIG_APP_MODE, "", () => box);
  // final urlModeConfig = ReadWriteValue(StorageKey.CONFIG_URL_MODE, "", () => box);
  // final baseUrlConfig = ReadWriteValue(StorageKey.CONFIG_BASE_URL, "", () => box);

  final mainProductItemCrossCount = ReadWriteValue(StorageKey.MAIN_PRODUCT_ITEM_CROSS_COUNT, 7, () => box);
  final mainProductItemHeight = ReadWriteValue(StorageKey.MAIN_PRODUCT_ITEM_HEIGHT, 100.0, () => box);

  final subProductItemCrossCount = ReadWriteValue(StorageKey.SUB_PRODUCT_ITEM_CROSS_COUNT, 6, () => box);
  final subProductItemHeight = ReadWriteValue(StorageKey.SUB_PRODUCT_ITEM_HEIGHT, 80.0, () => box);

  var globalParameter=ReadWriteValue(StorageKey.GLOBAL_PARAMETRER,{},()=> box);
  var businessName=ReadWriteValue(StorageKey.BUSINESS_NAME,"",()=> box);
  var businessAddress=ReadWriteValue(StorageKey.BUSINESS_ADDRESS,"",()=> box);
  var businessPostCode=ReadWriteValue(StorageKey.BUSINESS_POSTCODE,"",()=> box);
  var businessEmail=ReadWriteValue(StorageKey.BUSINESS_EMAIL,"",()=> box);
  var businessPhone=ReadWriteValue(StorageKey.BUSINESS_PHONE,"",()=> box);
  var businessStatus=ReadWriteValue(StorageKey.BUSINESS_STATUS,"",()=> box);
  var businessVat=ReadWriteValue(StorageKey.BUSINESS_VAT,"",()=> box);
  var businessOpeningTime=ReadWriteValue(StorageKey.BUSINESS_TIME,"",()=> box);

  var takeOutOrderStatus=ReadWriteValue(StorageKey.TAKE_OUT_ORDER_STATUS, false, ()=> box);
  var takeOrderStatus=ReadWriteValue(StorageKey.TAKE_ORDER_STATUS, false, ()=> box);
  var waiterAppStatus=ReadWriteValue(StorageKey.WAITER_APP_STATUS, false, ()=> box);
  var collectionOrderStatus=ReadWriteValue(StorageKey.COLLECTION_ORDER_STATUS, true, ()=> box);
  var deliveryOrderStatus=ReadWriteValue(StorageKey.DELIVERY_ORDER_STATUS, true, ()=> box);
}