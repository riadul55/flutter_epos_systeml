
import 'package:flutter/material.dart';

removeLastCharacter(TextEditingController controller) {
  String result = "";
  if (controller.text.isNotEmpty) {
    result = controller.text.substring(0, controller.text.length - 1);
  }
  controller.text = result;
}