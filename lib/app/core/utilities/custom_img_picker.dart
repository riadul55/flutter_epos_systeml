import 'dart:io';

import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';


// only for Android and IOS
class CustomImgPicker {
  final ImgPickerMixin imgPickerMixin;
  CustomImgPicker(this.imgPickerMixin);

  //profile pic upload
  var selectedImagePath = "";
  var selectedImageSize = "";

  //crop code
  var cropImagePath = '';
  var cropImageSize = '';

  //compress code
  var compressImagePath = '';
  var compressImageSize = '';

  void getImage(ImageSource imageSource) async {
    final pickedFile = await ImagePicker().pickImage(source: imageSource);
    if (pickedFile != null) {
      selectedImagePath = pickedFile.path;
      selectedImageSize =
          ((File(selectedImagePath)).lengthSync() / 1024 / 1024)
              .toStringAsFixed(2) +
              " Mb";

      //Crop
      final cropImageFile = await ImageCropper.cropImage(
        sourcePath: selectedImagePath,
        maxWidth: 512,
        maxHeight: 512,
        compressFormat: ImageCompressFormat.jpg,
        // aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
      );
      cropImagePath = cropImageFile!.path;
      cropImageSize = ((File(cropImagePath)).lengthSync() / 1024 / 1024).toStringAsFixed(2) + " Mb";

      // Compress
      final dir = await Directory.systemTemp;
      final targetPath = dir.absolute.path + "/temp.jpg";
      var compressedFile = await FlutterImageCompress.compressAndGetFile(
          cropImagePath,
          targetPath, quality: 90);
      compressImagePath = compressedFile!.path;
      compressImageSize =
          ((File(compressImagePath)).lengthSync() / 1024 / 1024)
              .toStringAsFixed(2) +
              " Mb";

        imgPickerMixin.onImageSelected(
            CustomImage(compressImagePath, compressImageSize, compressedFile));
    } else {
      imgPickerMixin.onError("No image selected");
    }
  }
}

abstract class ImgPickerMixin {
  void onImageSelected(CustomImage image);
  void onError(String error);
}

class CustomImage {
  String path;
  String size;
  File file;

  CustomImage(this.path, this.size, this.file);
}