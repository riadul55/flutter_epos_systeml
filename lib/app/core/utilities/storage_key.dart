

class StorageKey {
  static final String IS_FIRST_TIME = "isFirstTime";
  /// app mode configs prefs
  static final String APP_CONFIGS = "appConfig";
  static final String CONFIG_APP_MODE = "appModeConfig";
  static final String CONFIG_URL_MODE = "urlModeConfig";
  static final String CONFIG_BASE_URL = "baseUrlConfig";

  static final String MAIN_PRODUCT_ITEM_CROSS_COUNT = "mainProductItemCrossCount";
  static final String MAIN_PRODUCT_ITEM_HEIGHT = "mainProductItemHeight";
  static final String SUB_PRODUCT_ITEM_CROSS_COUNT = "subProductItemCrossCount";
  static final String SUB_PRODUCT_ITEM_HEIGHT = "subProductItemHeight";


  static const GLOBAL_PARAMETRER = "globalParameter";
  static const BUSINESS_NAME = "businessName";
  static const BUSINESS_ADDRESS = "businessAddress";
  static const BUSINESS_POSTCODE = "businessPostCode";
  static const BUSINESS_EMAIL = "businessEmail";
  static const BUSINESS_PHONE = "businessPhone";
  static const BUSINESS_STATUS = "businessStatus";
  static const BUSINESS_VAT = "businessVat";
  static const BUSINESS_TIME = "businessTime";



  static const TAKE_OUT_ORDER_STATUS = "takeOutOrderStatus";
  static const TAKE_ORDER_STATUS = "takeOrderStatus";
  static const WAITER_APP_STATUS = "waiterAppStatus";
  static const COLLECTION_ORDER_STATUS = "collectionOrderStatus";
  static const DELIVERY_ORDER_STATUS = "deliveryOrderStatus";


}