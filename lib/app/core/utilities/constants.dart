import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Constants {

  //4dYRBg2najq5PyXNCHak

  // static final defaultPadding = ScreenUtil().setSp(20.0);
  //
  // // font sizes
  // static final xsText = ScreenUtil().setSp(12.0);
  // static final sText = ScreenUtil().setSp(14.0);
  // static final mText = ScreenUtil().setSp(16.0);
  // static final lText = ScreenUtil().setSp(18.0);
  // static final xlText = ScreenUtil().setSp(20.0);
  //
  //
  // static final formWidth = ScreenUtil().setSp(600.0);

  static final defaultPadding = 20.0;
  static final defaultElevation = 2.0;
  static final defaultBreakPoint = 1280;
  static final preferredSize = 50.0;


  // font sizes
  static final xsText = 12.0;
  static final sText = 14.0;
  static final mText = 16.0;
  static final lText = 18.0;
  static final xlText = 20.0;

  static final iconSize = 30.0;

  static final formWidth = 600.0;

  //assets images
  static final APP_LOGO = "assets/images/app-logo.png";
  //assets icons
  static final IC_PAY_NOW = "assets/icons/pay_now.png";
  static final IC_PAY_LATER = "assets/icons/pay_later.png";

  static void setToLandscape() async {
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }
}