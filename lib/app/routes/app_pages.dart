import 'package:epos_system/app/modules/products/bindings/category_binding.dart';
import 'package:epos_system/app/modules/products/bindings/order_create_binding.dart';
import 'package:epos_system/app/modules/products/views/screens/category_view.dart';
import 'package:epos_system/app/modules/products/views/screens/order_create_screen.dart';
import 'package:get/get.dart';

import 'package:epos_system/app/modules/auth/bindings/auth_binding.dart';
import 'package:epos_system/app/modules/auth/views/auth_view.dart';
import 'package:epos_system/app/modules/customer/bindings/customer_binding.dart';
import 'package:epos_system/app/modules/customer/views/customer_view.dart';
import 'package:epos_system/app/modules/home/bindings/home_binding.dart';
import 'package:epos_system/app/modules/home/views/home_view.dart';
import 'package:epos_system/app/modules/orders/bindings/checkout_binding.dart';
import 'package:epos_system/app/modules/orders/bindings/order_details_binding.dart';
import 'package:epos_system/app/modules/orders/bindings/orders_binding.dart';
import 'package:epos_system/app/modules/orders/views/orders_view.dart';
import 'package:epos_system/app/modules/orders/views/screens/checkout_view.dart';
import 'package:epos_system/app/modules/orders/views/screens/order_details_view.dart';
import 'package:epos_system/app/modules/products/bindings/products_binding.dart';
import 'package:epos_system/app/modules/products/views/products_view.dart';
import 'package:epos_system/app/modules/setting/bindings/setting_binding.dart';
import 'package:epos_system/app/modules/setting/views/setting_view.dart';

import '../modules/products/bindings/product_update_binding.dart';
import '../modules/products/views/screens/main_products_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.AUTH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.AUTH,
      page: () => AuthView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: _Paths.PRODUCTS,
      page: () => ProductsView(),
      binding: ProductsBinding(),
    ),
    GetPage(
      name: _Paths.PRODUCTS_UPDATE,
      page: () => MainProductsView(),
      binding: ProductUpdateBinding(),
    ),
    GetPage(
      name: _Paths.ORDER_CREATE,
      page: () => OrderCreateScreen(),
      binding: OrderCreateBinding(),
    ),
    GetPage(
      name: _Paths.CATEGORY,
      page: () => CategoryView(),
      binding: CategoryBinding(),
    ),
    GetPage(
      name: _Paths.CHECKOUT,
      page: () => CheckoutView(),
      binding: CheckoutBinding(),
    ),
    GetPage(
      name: _Paths.CUSTOMER,
      page: () => CustomerView(),
      binding: CustomerBinding(),
    ),
    GetPage(
      name: _Paths.ORDERS,
      page: () => OrdersView(),
      binding: OrdersBinding(),
    ),
    GetPage(
      name: _Paths.ORDER_DETAILS,
      page: () => OrderDetailsView(),
      binding: OrderDetailsBinding(),
    ),
    GetPage(
      name: _Paths.SETTING,
      page: () => SettingView(),
      binding: SettingBinding(),
    ),
  ];
}
