part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const AUTH = _Paths.AUTH;
  static const PRODUCTS = _Paths.PRODUCTS;
  static const PRODUCTS_UPDATE = _Paths.PRODUCTS_UPDATE;
  static const ORDER_CREATE = _Paths.ORDER_CREATE;
  static const CATEGORY = _Paths.CATEGORY;
  static const CUSTOMER = _Paths.CUSTOMER;
  static const ORDERS = _Paths.ORDERS;
  static const ORDER_DETAILS = _Paths.ORDER_DETAILS;
  static const CHECKOUT = _Paths.CHECKOUT;
  static const SETTING = _Paths.SETTING;
}

abstract class _Paths {
  static const HOME = '/home';
  static const AUTH = '/auth';
  static const PRODUCTS = '/products';
  static const PRODUCTS_UPDATE = '/products-update';
  static const ORDER_CREATE = '/order-create';
  static const CATEGORY = '/category';
  static const CUSTOMER = '/customer';
  static const ORDERS = '/orders';
  static const ORDER_DETAILS = '/order-details';
  static const CHECKOUT = '/checkout';
  static const SETTING = '/setting';
}
