import 'dart:async';
import 'dart:io';

import 'package:desktop_window/desktop_window.dart';
import 'package:epos_system/initializer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'app/core/utilities/constants.dart';
import 'app/core/utilities/my_storage.dart';
import 'app/core/values/locale/languages.dart';
import 'app/core/values/styles/app_theme.dart';
import 'app/data/worker/local_notification_service.dart';
import 'app/routes/app_pages.dart';
import 'main/bindings/main_binding.dart';

void main() {
  runZonedGuarded(() async {
    await Initializer.init();
    runApp(MyApp());
  }, (error, stack) {
    print(stack);
    print(error);
  });
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Epos System",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      initialBinding: MainBinding(),
      theme: AppThemes.lightThemeData,
      darkTheme: AppThemes.lightThemeData,
      translations: Languages(),
      locale: Locale("en", "US"),
      fallbackLocale: Locale("en", "US"),
    );
  }
}

Future<void> backgroundHandler(RemoteMessage message) async {
  LocalNotificationService.display(message);
}