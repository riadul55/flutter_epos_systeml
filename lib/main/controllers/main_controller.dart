import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:epos_system/app/data/worker/isolate_worker.dart';
import 'package:epos_system/app/data/worker/local_notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

class MainController extends GetxController with GetxServiceMixin {

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() async {
    if (Platform.isAndroid || Platform.isIOS) {
      // topic = <provider_uuid>, <consumer_uuid>, <admin_uuid>
      FirebaseMessaging.instance.subscribeToTopic("a7033019-3dee-457b-a9da-50df238b3c9f");

      // forground get messages
      var initialMessage = await FirebaseMessaging.instance.getInitialMessage();
      if (initialMessage != null) _handleMessage(initialMessage);
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        LocalNotificationService.display(message);
      });
      // FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
    }
    super.onReady();
  }

  void _handleMessage(RemoteMessage message) {
    print(message.data);
  }
}
