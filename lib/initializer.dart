import 'package:epos_system/app/core/utilities/my_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import './app/data/local_db/db_injection.dart' as db_injection;

import 'app/core/utilities/constants.dart';
import 'config/config.dart';

class Initializer {
  static Future<void> init() async {
    WidgetsFlutterBinding.ensureInitialized();
    db_injection.init();
    Constants.setToLandscape();
    await _initStorage();
    await _initGetConnect();
  }

  static Future<void> _initStorage() async {
    await GetStorage.init();
    Get.put(MyStorage());
  }

  static Future<void> _initGetConnect() async {
    final connect = GetConnect();
    final env = ConfigEnvironments.getEnvironments();
    connect.baseUrl = env.baseUrl;
    connect.timeout = const Duration(seconds: 20);
    connect.httpClient.maxAuthRetries = 0;
    Get.put(connect);
  }
}
