
class EnvModel {
/*
{
  "env": "Environments.LAB",
  "business": "LAB",
  "platform": "epos",
  "language": "en",
  "providerID": "a7033019-3dee-457b-a9da-50df238b3c9p",
  "providerSession": "2f471e02-b567-4dc9-b72c-7e1c3613310b",
  "adminSession": "ff4c7526-d4f8-45f6-be0a-d0221df0200a",
  "addressCreator": "03683bdc-00a1-4d6d-b344-c70d937f7168",
  "site_url": "https://food.redmango.online/",
  "base_url": "https://labapi.yuma-technology.co.uk:8443"
}
*/

  String? env;
  String? business;
  String? platform;
  String? language;
  String? providerID;
  String? providerSession;
  String? adminSession;
  String? addressCreator;
  String? siteUrl;
  String? baseUrl;

  EnvModel({
    this.env,
    this.business,
    this.platform,
    this.language,
    this.providerID,
    this.providerSession,
    this.adminSession,
    this.addressCreator,
    this.siteUrl,
    this.baseUrl,
  });
  EnvModel.fromJson(Map<String, dynamic> json) {
    env = json['env']?.toString();
    business = json['business']?.toString();
    platform = json['platform']?.toString();
    language = json['language']?.toString();
    providerID = json['providerID']?.toString();
    providerSession = json['providerSession']?.toString();
    adminSession = json['adminSession']?.toString();
    addressCreator = json['addressCreator']?.toString();
    siteUrl = json['site_url']?.toString();
    baseUrl = json['base_url']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['env'] = env;
    data['business'] = business;
    data['platform'] = platform;
    data['language'] = language;
    data['providerID'] = providerID;
    data['providerSession'] = providerSession;
    data['adminSession'] = adminSession;
    data['addressCreator'] = addressCreator;
    data['site_url'] = siteUrl;
    data['base_url'] = baseUrl;
    return data;
  }
}
