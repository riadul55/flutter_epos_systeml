import 'env.model.dart';
import 'environments.dart';

class ConfigEnvironments {
  static const String _currentEnvironments = Environments.LAB;

  static EnvModel getEnvironments() {
    return EnvModel.fromJson(availableEnvironments.firstWhere(
          (d) => d['env'] == _currentEnvironments,
    ));
  }
}
