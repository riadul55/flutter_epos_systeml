# EPOS System

EPOS System is a cross-platform pos system for restaurants.

## Packages
- [GetX CLI](https://pub.dev/packages/get_cli) (Framework)
- [Drift](https://pub.dev/packages/drift) (upgrade version of moor db)
- [Google Fonts](https://pub.dev/packages/google_fonts)
- [Get It](https://pub.dev/packages/get_it) (Dependency injection)
- [Responsive Grid](https://pub.dev/packages/responsive_grid)
- [Flutter Svg](https://pub.dev/packages/flutter_svg)
- [Flutter Staggered Grid View](https://pub.dev/packages/flutter_staggered_grid_view)
- [Get Storage](https://pub.dev/packages/get_storage)
- [Multi selection](https://pub.dev/packages/multi_select_flutter)

## Installation

- Clone the project ```git clone <url>```
- Then run this command: ```flutter packages pub run build_runner build```

## Folder Structure

```bash
lib
├───app
│   ├───core
│   │   ├───utilities
│   │   └───values
│   │       ├───colors
│   │       ├───locale
│   │       └───styles
│   ├───data
│   │   ├───local_db
│   │   │   ├───dao
│   │   │   └───tables
│   │   └───remote_db
│   ├───modules
│   │   ├───auth
│   │   │   ├───bindings
│   │   │   ├───controllers
│   │   │   └───views
│   │   │       ├───components
│   │   │       │   ├───config
│   │   │       │   └───login
│   │   │       └───screens
│   │   ├───customer
│   │   │   ├───bindings
│   │   │   ├───controllers
│   │   │   └───views
│   │   │       └───components
│   │   ├───globals
│   │   ├───home
│   │   │   ├───bindings
│   │   │   ├───controllers
│   │   │   └───views
│   │   │       ├───components
│   │   │       └───screens
│   │   ├───orders
│   │   │   ├───bindings
│   │   │   ├───controllers
│   │   │   └───views
│   │   │       ├───components
│   │   │       │   └───orders
│   │   │       └───screens
│   │   ├───products
│   │   │   ├───bindings
│   │   │   ├───controllers
│   │   │   └───views
│   │   │       ├───components
│   │   │       │   ├───category
│   │   │       │   ├───collection
│   │   │       │   └───products
│   │   │       └───screens
│   │   └───setting
│   │       ├───bindings
│   │       ├───controllers
│   │       └───views
│   │           ├───components
│   │           └───screens
│   └───routes
└───main
    ├───bindings
    └───controllers
```
## Developers
- [Md. Riadul Islam](https://github.com/riadulislam55) (Mobile Application Developer)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)